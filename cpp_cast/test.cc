#include<iostream>

using namespace std;

class A
{
private:
	int _a;
public:
	explicit A(int a)
	{
		std::cout << "explicit A(int a) " << std::endl;
	}
	A(const A& a)
	{
		std::cout << "A(const A& a) " << std::endl;
	}
};

int main()
{
	A a1(1);
	// A a2 = 2; // error 
	return 0;
}


//class A
//{
//public:
//	virtual void f()
//	{}
//};
//class B : public A
//{};
//void func(A* pa)
//{
//	B* pb1 = (B*)pa;               //不安全
//	B* pb2 = dynamic_cast<B*>(pa); //安全
//
//	cout << "pb1: " << pb1 << endl;
//	cout << "pb2: " << pb2 << endl;
//}
//int main()
//{
//	A a;
//	B b;
//	func(&a);
//	func(&b);
//	return 0;
//}


//class A
//{
//private:
//	int _a;
//public:
//	explicit A(int a)
//	{
//		std::cout << "explicit A(int a) " << std::endl;
//	}
//	A(const A& a)
//	{
//		std::cout << "A(const A& a) " << std::endl;
//	}
//};
//
//int main()
//{
//	A a1(1);
//	// A a2 = 2; // error 
//	return 0;
//}

//int main()
//{
//	const int a = 10;
//	int* p = const_cast<int*>(&a);
//	*p = 20;
//	std::cout << a << std::endl;
//	std::cout << *p << std::endl;
//	return 0;
//}

//int main()
//{
//	int i = 10;
//	int* p = &i;
//	int x = reinterpret_cast<int>(p);
//	std::cout << x << std::endl;
//	return 0;
//}

//int main()
//{
//	double d = 10;
//	int i = static_cast<int>(d);
//	std::cout << i << std::endl;
//
//	int* p = &i;
//	// int x = static_cast<int> p; // error
//	return 0;
//}

//int main()
//{
//	// 隐式类型转换
//	int i = 0;
//	double d = i;
//	std::cout << i << std::endl;
//	std::cout << d << std::endl;
//	// 显示类型转换
//	int* p = &i;
//	int address = (int)p;
//	cout << p << endl;
//	cout << address << endl;
//	return 0;
//}
