#pragma once
#include<iostream>

using namespace std;

// 定义结点颜色
enum Col
{
	RED,
	BLACK
};

// 定义红黑树结点
template<class K, class V>
struct RBTreeNode
{

	// 构造函数
	RBTreeNode(const pair<K, V> kv)
		:_left(nullptr)
		,_right(nullptr)
		,_parent(nullptr)
		,_col(RED)
		,_kv(kv)
	{}

	// 三叉链
	RBTreeNode<K, V>* _left;
	RBTreeNode<K, V>* _right;
	RBTreeNode<K, V>* _parent;

	// 存储颜色
	int _col;

	// 存储键值对
	pair<K, V> _kv;
};


template<class K, class V>
struct RBTree
{
	typedef RBTreeNode<K, V> Node;
public:
	// 插入
	pair<Node*, bool> Insert(const pair<K, V>& kv)
	{
		// 一棵空树
		if (_root == nullptr)
		{
			// 创建新结点 + 颜色初始化为黑色
			_root = new Node(kv);
			_root->_col = BLACK; // 根节点得是黑的
			return make_pair(_root, true);
		}

		// 先找到 -- 利用二叉搜索树的方法进行查找
		Node* cur = _root;
		Node* parent = nullptr;
		// 左小右大
		while (cur)
		{
			// 当前结点值大于待插入结点值，往左子树走
			if (cur->_kv.first > kv.first)
			{
				parent = cur;
				cur = cur->_left;
			}
			// 当前结点值小于待插入结点值，往右子树走
			else if (cur->_kv.first < kv.first)
			{
				parent = cur;
				cur = cur->_right;
			}
			// 待插入结点的值和当前结点的值相等，插入失败
			else
			{
				return make_pair(cur, false);
			}
		}

		// 将当前结点的值插入进去
		cur = new Node(kv); // new一个新的结点
		cur->_col = RED;
		Node* newnode = cur; // 记录新插入的结点
		// 新插入的节点值小于父节点的节点值，插入到parent的左边
		if (kv.first < parent->_kv.first)
		{
			parent->_left = cur;
			cur->_parent = parent;
		}
		// 新插入的节点值小于父节点的节点值，插入到parent的左边
		else
		{
			parent->_right = cur;
			cur->_parent = parent;
		}

		// 新插入结点的父节点是红色的，需要做出调整
		while (parent && parent->_col == RED)
		{
			Node* grandfather = parent->_parent; // parent是红色，则其父结点一定存在
			// 以grandparent左右孩子为分界线，分成if和else
			if (parent == grandfather->_left) // 左孩子
			{
				Node* uncle = grandfather->_right;
				if (uncle && uncle->_col == RED)// 情况一：uncle存在且为红色
				{
					// 颜色调整
					grandfather->_col = RED;
					uncle->_col = parent->_col = BLACK;
					
					// 继续往上处理
					cur = grandfather;
					parent = cur->_parent;
				}
				else// 情况二：uncle存在且为黑色 / 情况三：uncle不存在
				{
					// 用左右孩子分为两半，一半是if用来表示在左孩子，一半是else用来表示在右孩子
					if (cur == parent->_left)
					{
						//   g
						//  p
						// c
						// 右单旋
						RoateR(grandfather);

						// 颜色调整
						parent->_col = BLACK;
						grandfather->_col = RED;
					}
					else
					{
						//    g
						//  p
						//      c
						// 左右双旋
						RoateLR(grandfather);

						// 颜色调整
						cur->_col = BLACK;
						grandfather->_col = RED;
					}
					break;
				}
			}
			else // 右孩子
			{
				Node* uncle = grandfather->_left;
				if (uncle && uncle->_col == RED)// 情况一：uncle存在且为红色
				{
					// 颜色调整
					grandfather->_col = RED;
					uncle->_col = parent->_col = BLACK;

					// 继续往上处理
					cur = grandfather;
					parent = cur->_parent;
				}
				else// 情况二：uncle存在且为黑色 / 情况三：uncle不存在
				{
					// 用左右孩子分为两半，一半是if用来表示在左孩子，一半是else用来表示在右孩子
					if (cur == parent->_right)
					{
						//   g
						//     p
						//      c
						// 左单旋
						RoateL(grandfather);

						// 颜色调整
						parent->_col = BLACK;
						grandfather->_col = RED;
					}
					else
					{
						//    g
						//      p
						//    c
						// 右左双旋
						RoateRL(grandfather);

						// 颜色调整
						cur->_col = BLACK;
						grandfather->_col = RED;
					}
					break;
				}
			}
		}
		_root->_col = BLACK;
		return make_pair(newnode, true);
	}

	// 左单旋
	void RoateL(Node* parent)
	{
		// 三叉链
		Node* subr = parent->_right;
		Node* subrl = subr->_left;
		Node* ppnode = parent->_parent;

		// subrl与parent的关系
		parent->_right = subrl;
		if (subrl)
			subrl->_parent = parent;

		// subl和parent的关系
		subr->_left = parent;
		parent->_parent = subr;

		// ppnode和subr的关系
		if (ppnode == nullptr)
		{
			_root = subr;
			subr->_parent = nullptr;
		}
		else
		{
			if (ppnode->_left == parent)
			{
				ppnode->_left = subr;
			}
			else
			{
				ppnode->_right = subr;
			}
			subr->_parent = ppnode;
		}
	}

	// 右单旋
	void RoateR(Node* parent)
	{
		// 三叉链
		Node* subl = parent->_left;
		Node* sublr = subl->_right;
		Node* ppnode = parent->_parent;


		//sublr和parent之间的关系
		parent->_left = sublr;
		if (sublr)
			sublr->_parent = parent;

		//subl和parent的关系
		subl->_right = parent;
		parent->_parent = subl;


		//ppnode 和 subl的关系
		if (ppnode == nullptr)
		{
			_root = subl;
			subl->_parent = nullptr;
		}
		else
		{
			if (ppnode->_left == parent)
			{
				ppnode->_left = subl;
			}
			else
			{
				ppnode->_right = subl;
			}
			subl->_parent = ppnode;
		}
	}

	// 左右双旋
	void RoateLR(Node* parent)
	{
		RoateL(parent->_left);
		RoateR(parent);
	}

	// 右左双旋
	void RoateRL(Node* parent)
	{
		RoateR(parent->_right);
		RoateL(parent);
	}

	// 中序遍历
	void InOrder()
	{
		return _InOrder(_root);
	}

	void _InOrder(Node* root)
	{
		if (root == nullptr)
			return;
		// 中序
		_InOrder(root->_left);
		cout << root->_kv.first << " ";
		_InOrder(root->_right);
	}

	// 验证是否平衡
	// 先检查检查颜色
	bool CheckColour(Node* root, int blacknum, int blenchnum) // 基准值
	{
		if (root == nullptr)
		{
			// 每个路径黑色不相等
			if (blacknum != blenchnum)
			{
				return false;
			}
			return true;
		}
		// 黑色增加
		if (root->_col == BLACK)
		{
			++blacknum;
		}

		// 连续红色结点情况
		if (root->_col == RED && root->_parent && root->_parent->_col == RED)
		{
			cout << root->_kv.first << "出现连续的红色结点" << endl;
			return false;
		}

		// 递归
		return CheckColour(root->_left, blacknum, blenchnum)
			&& CheckColour(root->_right, blacknum, blenchnum);
	}

	// 再检查是否平衡
	bool IsRBTree()
	{
		return _IsRBTree(_root);
	}

	bool _IsRBTree(Node* root)
	{
		if (root == nullptr)
		{
			return true;
		}

		if (root->_col == RED)
		{
			return false;
		}

		// 找最左路径作为黑色结点数目的参考值
		int blenchnum = 0;
		Node* cur = _root;
		while (cur)
		{
			if (cur->_col == BLACK)
				++blenchnum;
			cur = cur->_left;
		}

		return CheckColour(root, 0, blenchnum);
	}

	// 计算树的高度
	int Height()
	{
		return _Height(_root);
	}

	int _Height(Node* root)
	{
		if (root == nullptr)
		{
			return 0;
		}

		int leftcount = _Height(root->_left);
		int rightcount = _Height(root->_right);
		
		return leftcount > rightcount ? leftcount + 1 : rightcount + 1;
	}

	// 红黑树的查找
	Node* Find(const pair<K, V>& kv)
	{
		Node* cur = _root;
		while (cur)
		{
			// 当前结点的值大于寻找的结点的值
			if (cur->_kv.first > kv.first)
			{
				cur = cur->_left;
			}
			else if(cur->_kv.first < kv.first)
			{
				cur = cur->_right;
			}
			else
			{
				// 找到了
				return cur;
			}
		}
	}

	// 删除
	bool Erase(const K& key)
	{
		// 用于遍历二叉树找结点
		Node* parent = nullptr;
		Node* cur = _root;
		// 用于标记实际的删除结点及其父结点
		Node* delparentpos = nullptr;
		Node* delpos = nullptr;
		// 先找到
		while (cur)
		{
			// 所给key值小于当前节点的值 -- 往左树走
			if (key < cur->_kv.first)
			{
				parent = cur;
				cur = cur->_left;
			}
			// 所给key值大于当前结点的值 -- 往右树走
			else if (key > cur->_kv.first)
			{
				parent = cur;
				cur = cur->_right;
			}
			// 找到了
			else
			{
				// 左子树为空
				if (cur->_left == nullptr)
				{
					// 待删除结点是根节点
					if (cur == _root)
					{
						// 让根节点的右子树作为新的结点
						_root = _root->_right;
						if (_root)
							_root->_parent = nullptr;
						delete cur; // 删除原节点
						return true;
					}
					else // 不是根节点
					{
						delparentpos = parent; // 标记当前待删除结点的父节点
						delpos = cur; // 标记当前待删除的结点
					}
					break; // 删除结点有祖先的结点，需要更新平衡因子
				}
				// 右子树为空
				else if (cur->_right == nullptr)
				{
					// 待删除结点是根节点
					if (cur == _root)
					{
						// 让根节点的左子树作为新的结点
						_root = _root->_left;
						if (_root)
							_root->_parent = nullptr;
						delete cur; // 删除原节点
						return true;
					}
					else // 不是根节点
					{
						delparentpos = parent; // 标记当前待删除结点的父节点
						delpos = cur; // 标记当前待删除的结点
					}
					break; // 删除结点有祖先的结点，需要更新平衡因子
				}
				// 左右子树都不为空
				else
				{
					// 替换法
					// 寻找待删除结点的右子树中的最小值
					Node* minparent = cur;
					Node* minright = cur->_right;
					while (minright->_left)
					{
						minparent = minright; // 记录一下父节点
						minright = minright->_left; // 往左子树走
					}
					cur->_kv.first = minright->_kv.first;// 将待删除结点first替换为右子树的最小值
					cur->_kv.second = minparent->_kv.second;// 将待删除结点second替换为右子树的最小值
					// 记录一下要删除的父节点
					delparentpos = minparent;
					// 记录一下实际要删除的结点
					delpos = minright;
					break; // 祖先结点的平衡因子需要改变
				}
			}
		}
		// 没有被修改过，说明没找到当前要删除的结点
		if (delparentpos == nullptr)
			return false;

		// 记录当前要删除结点和当前要删除结点的父节点
		Node* del = delpos;
		Node* delP = delparentpos;

		// 调整红黑树
		if (delpos->_col == BLACK)
		{
			if (delpos->_left && delpos->_left->_col == RED) //待删除结点有一个红色的左孩子
			{
				//     delpos
				// _left
				delpos->_left->_col = BLACK; //将这个红色的左孩子变黑
			}
			else if (delpos->_right && delpos->_right->_col == RED) //待删除结点有一个红色的右孩子
			{
				// delpos
				//     _right
				delpos->_right->_col = BLACK; //将这个红色的右孩子变黑
			}
			else // 待删除结点的左右均为空
			{
				while (delpos != _root)
				{
					// 待删除的结点是其父节点的左孩子
					if (delpos == delparentpos->_left)
					{
						//      delparentpos
						//  delpos       brother
						Node* brother = delparentpos->_right; // 兄弟结点是其父结点的右孩子
						// 情况1:brother为红色
						if (brother->_col)
						{
							// 先左旋再调颜色
							RoateL(delparentpos);
							delparentpos->_col = RED;
							brother->_col = BLACK;
							// 继续向上调整
							brother = delparentpos->_right;
						}
						// 情况2：brother为黑色，且其左右孩子都是黑色结点或为空
						if (((brother->_left == nullptr) || (brother->_left->_col == BLACK))
							&& ((brother->_right == nullptr) || (brother->_right->_col == BLACK)))
						{
							brother->_col = RED;
							// 分情况
							if (delparentpos->_col == RED)
							{
								delparentpos->_col = BLACK;
								break;
							}
							// 向上调整
							delpos = delparentpos;
							delparentpos = delpos->_parent;
						}
						else
						{
							// 情况3：brother为黑色，且其右孩子是红色结点
							// 左旋
							RoateL(delparentpos);
							brother->_col = delparentpos->_col;
							delparentpos->_col = BLACK;
							brother->_right->_col = BLACK;
							break;

							// 情况4：brother为黑色，且其左孩子是红色结点，右孩子是黑色结点或为空
							if ((brother->_right == nullptr) || (brother->_right->_col == BLACK))
							{
								brother->_left->_col = BLACK;
								brother->_col = RED;
								RoateR(brother);

								brother = delparentpos->_right; 
							}
						}
					}
					// 待删除的结点是其父节点的右孩子
					else
					{
						Node* brother = delparentpos->_right; // 兄弟结点是其父结点的右孩子
						// 情况1：brother为红色
						if (brother->_col == RED)
						{
							RoateR(delparentpos);
							delparentpos->_col = RED;
							brother->_col = BLACK;
							// 继续向上调整
							brother = delparentpos->_left;
						}
						// 情况2：brother为黑色，且其左右孩子都是黑色结点或为空
						if (((brother->_left == nullptr) || (brother->_left->_col == BLACK))
							&& ((brother->_right == nullptr) || (brother->_right->_col == BLACK)))
						{
							brother->_col = RED;
							if (delparentpos->_col == RED)
							{
								delparentpos->_col = BLACK;
								break;
							}

							delpos = delparentpos;
							delparentpos = delpos->_parent;
						}
						else
						{
							// 情况3：brother为黑色，且其右孩子是红色结点
							RoateR(delparentpos);
							brother->_col = delparentpos->_col;
							delparentpos->_col = BLACK;
							brother->_left->_col = BLACK;
							break;

							// 情况4：brother为黑色，且其左孩子是红色结点，右孩子是黑色结点或为空
							if ((brother->_left == nullptr) || (brother->_left->_col == BLACK))
							{
								brother->_right->_col = BLACK;
								brother->_col = RED;
								RoateL(brother);

								brother = delparentpos->_left;
							}
						}
					}
				}
			}
		}

		// 进行删除
		// 删除的结点的左子树是空树
		if (del->_left == nullptr)
		{
			if (del == delP->_left) //删除结点是其父结点的左孩子
			{
				delP->_left = del->_right;
				if (del->_right)
					del->_right->_parent = delP;
			}
			else //实际删除结点是其父结点的右孩子
			{
				delP->_right = del->_right;
				if (del->_right)
					del->_right->_parent = delP;
			}
		}
		else // 删除的结点的右子树是空树
			 // del->_right == nullptr 
		{
			if (del == delP->_left) //实际删除结点是其父结点的左孩子
			{
				delP->_left = del->_left;
				if (del->_left)
					del->_left->_parent = delP;
			}
			else //实际删除结点是其父结点的右孩子
			{
				delP->_right = del->_left;
				if (del->_left)
					del->_left->_parent = delP;
			}
		}
		delete del;
		return true;
	}

private:
	Node* _root = nullptr;
};
