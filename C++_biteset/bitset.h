#pragma once

#include<vector>
#include<assert.h>
#include<iostream>
using namespace std;

namespace JRH
{
	// 模拟实现位图
	template<size_t N>
	class bitset
	{
	public:
		// 构造函数
		bitset()
		{
			_bits.resize(N / 32 + 1, 0);
		}

		// set设置位
		void set(size_t pos)
		{
			assert(pos < N);
			// 除一下找到在第几个
			int i = pos / 32;
			int j = pos % 32;
			_bits[i] |= (1 << j);
		}

		// reset清空
		void reset(size_t pos)
		{
			assert(pos < N);

			// 除一下找到在第几个
			int i = pos / 32;
			int j = pos % 32;
			_bits[i] &= (~(1 << j));
		}

		//反转位
		void flip(size_t pos)
		{
			assert(pos < N);

			// 找到在哪里
			int i = pos / 32;
			int j = pos % 32;
			_bits[i] ^= (1 << j);
		}

		//获取位的状态
		bool test(size_t pos)
		{
			assert(pos < N);

			// 找到位置
			int i = pos / 32;
			int j = pos % 32;
			if (_bits[i] & (1 << j))
				return true;
			else
				return false;
		}

		//获取可以容纳的位的个数
		size_t size()
		{
			return N;
		}

		//获取被设置位的个数
		size_t count()
		{
			size_t count = 0;
			for (auto e : _bits)
			{
				int num = e;
				// 计算1的个数
				while (num)
				{
					num = num & (num - 1);
					count++;
				}
			}
			return count;
		}

		//判断位图中是否有位被设置
		bool any()
		{
			for (auto e : _bits)
			{
				if (e != 0)
				{
					return true;
				}
			}
			return false;
		}

		//判断位图中是否全部位都没有被设置
		bool none()
		{
			return !any();
		}

		//判断位图中是否全部位都被设置
		bool all()
		{
			size_t n = _bits.size();
			// 先判断前n-1个整数
			for (size_t i = 0; i < n - 1; ++i)
			{
				// 取反后不全为0则取反前不全为1
				if (~_bits[i] != 0)
				{
					return false;
				}
			}
			// 再判断第n个整数的bite位
			for (size_t j = 0; j < N % 32; j++)
			{
				if ((_bits[n - 1] & (1 << j)) == 0)
				{
					return false;
				}
			}
			return true;
		}

		//打印函数
		void Print()
		{
			int count = 0;
			size_t n = _bits.size();

			// 先打印前n-1
			for (int i = 0; i < n - 1; i++)
			{
				for (int j = 0; j < 32; j++)
				{
					if (_bits[i] & (1 << j))
					{
						cout << "1";
					}
					else
					{
						cout << "0";
					}
					count++;
				}
			}

			// 再打印第n个的比特位
			for (int j = 0; j < N % 32; j++)
			{
				if (_bits[n - 1] & (1 << j))
				{
					cout << "1";
				}
				else
				{
					cout << "0";
				}
				count++;
			}
			cout << endl;
			cout << " " << count << endl;
		}

	private:
		vector<int> _bits;
	};
}