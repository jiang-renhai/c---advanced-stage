
#include<iostream>
#include<set>

using namespace std;

//template<class T1, class T2>
//struct pair
//{
//	typedef T1 first_type;
//	typedef T2 second_type;
//	T1 first;
//	T2 second;
//	pair()
//		:first(T1())
//		,second(T2())
//	{}
//	pair(const T1& a, const T2& b)
//		:first(a)
//		,second(b)
//	{}
//};

void testset1()
{
	set<int> set1;
	int num[10] = { 1,2,4,3,5,7,6,8,9,10 };

	// 区间构造
	set<int> set2(num, num + sizeof(num) / sizeof(num[0]));

	// 拷贝构造
	set<int> set3(set2);// 拷贝构造代价太大

	// for循环打印set2
	for (auto& e : set2)
	{
		cout << e << " ";
	}
	cout << endl;

	// for循环打印set3
	for (auto& e : set3)
	{
		cout << e << " ";
	}
	cout << endl;
}

void testset2()
{
	set<int> set1 = { 1,2,1,4,5,3,7,6,5,8,9 };
	set<int>::iterator it = set1.begin();
	// 去重+排序
	while (it != set1.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;

	// 范围for
	for (auto& e : set1)
	{
		cout << e << " ";
	}
	cout << endl;
}

void testset3()
{
	int a[10] = { 1,3,1,2,4,5,7,6,5,8 };
	// 这个greater仿函数是set库函数中的函数
	// 就是进行降序的函数
	set<int, greater<int>> set1(a, a + sizeof(a) / sizeof(a[0]));

	// for循环
	for (auto& e : set1)
	{
		cout << e << " ";
	}
	cout << endl;
}

void testset4()
{
	std::set<int> myset;
	std::set<int>::iterator it;
	std::pair<std::set<int>::iterator, bool> ret;

	// for循环并单个插入
	for (int i = 1; i <= 5; ++i)
		myset.insert(i * 10);

	// 单个插入
	ret = myset.insert(20);

	if (ret.second == false)
		it = ret.first;

	// 插入迭代器所代表的值
	myset.insert(it, 25);
	myset.insert(it, 24);
	myset.insert(it, 26);

	// 插入一段区间
	int myints[] = { 5,10,15 };
	myset.insert(myints, myints + 3);

	std::cout << "myset contains:";
	for (it = myset.begin(); it != myset.end(); ++it)
		std::cout << ' ' << *it;
	std::cout << '\n';
}

void testset5()
{
	std::set<int> myset;
	std::set<int>::iterator it;

	for (int i = 1; i < 10; i++)
		myset.insert(i * 10); // 10 20 30 40 50 60 70 80 90

	it = myset.begin();
	++it; // 第二个位置

	// 删除第二个位置
	myset.erase(it);

	// 删除一个数
	myset.erase(40);

	// 删除一个迭代器区间
	it = myset.find(60);
	myset.erase(it, myset.end());

	std::cout << "myset contains:";
	for (it = myset.begin(); it != myset.end(); ++it)
		std::cout << ' ' << *it;
	std::cout << '\n';
}

void testset6()
{
	std::set<int> myset;
	std::set<int>::iterator it;

	for (int i = 1; i <= 5; i++)
		myset.insert(i * 10); // 10 20 30 40 50

	it = myset.find(20);
	myset.erase(it); // 删除20这个迭代器的值
	myset.erase(myset.find(40)); // 删除40这个迭代器的值

	std::cout << "myset contains:";
	for (it = myset.begin(); it != myset.end(); ++it)
		std::cout << ' ' << *it;
	std::cout << '\n';
}

void testset7()
{
	std::set<int> myset;
	std::set<int>::iterator it;

	for (int i = 1; i < 10; i++)
		myset.insert(i * 10); // 10 20 30 40 50 60 70 80 90

	it = myset.begin();
	++it; // 第二个位置

	// 删除第二个位置
	myset.erase(it);

	// 删除一个数
	myset.erase(40);

	// 删除一个迭代器区间
	it = myset.find(60);
	myset.erase(it, myset.end());

	// 删除一个不存在的迭代器
	set<int>::iterator pos = myset.find(70);
	if (pos != myset.end())
	{
		myset.erase(pos);
	}

	for (auto& e : myset)
	{
		cout << e << " ";
	}
	cout << endl;
}

void testset8()
{
	std::set<int> myset;

	for (int i = 1; i < 10; i++)
		myset.insert(i * 10); // 10 20 30 40 50 60 70 80 90
	cout << myset.count(10) << endl;
	cout << myset.count(0) << endl;

}

void testset9()
{
	set<int> myset;
	set<int>::iterator itup, itlow;

	for (int i = 1; i < 10; ++i)
	{
		myset.insert(i * 10);
	}

	itlow = myset.lower_bound(35);
	itup = myset.upper_bound(60);
	cout << "[" << *itlow << "," << *itup << "]" << endl;

	myset.erase(itlow, itup);

	for (auto& e : myset)
	{
		cout << e << " ";
	}
	cout << endl;
}

void testset10()
{
	// multiset
	int a[10] = { 1,3,2,4,1,2,5,6,7,10 };
	multiset<int> set1(a, a + sizeof(a) / sizeof(a[0]));
	for (auto& e : set1)
	{
		cout << e << " ";
	}
	cout << endl;
}

#include<map>

void testmap1()
{
	map<string, string> map1;
	map1.insert(make_pair("banana", "香蕉"));
	map1.insert(make_pair("apple", "苹果"));
	map1.insert(make_pair("orange", "橙子"));
	map<string, string>::iterator it = map1.begin();
	while (it != map1.end())
	{
		cout << it->first << it->second << endl;
		++it;
	}
}

void testmap2()
{
	map<int, int> map1; // 空的构造
	int a[] = { 1,2,3,4,5,6,7,8,88,9,10 };
	for (auto& e : a)
	{
		map1.insert(make_pair(e, e));
	}
	map<int, int> map2(map1.begin(), map1.end()); // 迭代器区间构造
	map<int, int> map3(map2); // 拷贝构造

	for (auto& e : map3)
	{
		cout << e.first << "->" << e.second << endl;
	}
}

void testmap3()
{
	std::map<char, int> mymap;

	// 匿名对象
	mymap.insert(std::pair<char, int>('a', 100));
	mymap.insert(std::pair<char, int>('z', 200));

	// 在map中插入一个键值对，迭代器加判断是否插入成功
	std::pair<std::map<char, int>::iterator, bool> ret;
	ret = mymap.insert(std::pair<char, int>('z', 500));
	if (ret.second == false)
	{
		std::cout << "element 'z' already existed";
		std::cout << " with a value of " << ret.first->second << '\n';
	}

	// 插入一个迭代器位置加值
	std::map<char, int>::iterator it = mymap.begin();
	mymap.insert(it, std::pair<char, int>('b', 300));
	mymap.insert(it, std::pair<char, int>('c', 400));

	// 迭代器区间插入，利用begin()到find('c')的位置
	std::map<char, int> anothermap;
	anothermap.insert(mymap.begin(), mymap.find('c'));

	std::cout << "mymap contains:\n";
	for (it = mymap.begin(); it != mymap.end(); ++it)
		std::cout << it->first << " => " << it->second << '\n';

	std::cout << "anothermap contains:\n";
	for (it = anothermap.begin(); it != anothermap.end(); ++it)
		std::cout << it->first << " => " << it->second << '\n';
}

void testmap4()
{
	std::map<char, int> mymap;
	std::map<char, int>::iterator it;

	mymap.insert(make_pair('a', 10));
	mymap.insert(make_pair('b', 20));
	mymap.insert(make_pair('c', 30));
	mymap.insert(make_pair('d', 40));
	mymap.insert(make_pair('e', 50));
	mymap.insert(make_pair('f', 60));

	// 删除b这个位置的整个迭代器
	it = mymap.find('b');
	mymap.erase(it);

	// 删除键值为c的元素
	mymap.erase('c');

	// 删除迭代器区间
	it = mymap.find('e');
	mymap.erase(it, mymap.end());

	for (it = mymap.begin(); it != mymap.end(); ++it)
		std::cout << it->first << " => " << it->second << '\n';
}

void testmap5()
{
	std::map<char, int> mymap;
	std::map<char, int>::iterator it;

	mymap.insert(pair<char, int>('a', 50));
	mymap.insert(make_pair('b', 100));
	mymap.insert(pair<char, int>('c', 150));
	mymap.insert(pair<char, int>('d', 200));

	// 找到b这个键值对
	it = mymap.find('b');
	if (it != mymap.end()) // 判断是否找到
		mymap.erase(it);

	std::cout << "elements in mymap:" << '\n';
	std::cout << "a => " << mymap.find('a')->second << '\n';
	std::cout << "c => " << mymap.find('c')->second << '\n';
	std::cout << "d => " << mymap.find('d')->second << '\n';
}

void testmap6()
{
	std::map<char, int> mymap;
	char c;

	mymap.insert(make_pair('a', 101));
	mymap.insert(make_pair('b', 202));
	mymap.insert(make_pair('c', 303));
	mymap.insert(make_pair('d', 404));

	for (c = 'a'; c < 'h'; c++)
	{
		std::cout << c;
		if (mymap.count(c) > 0) // 数值大于0则输出是个有值的map
			std::cout << " is an element of mymap.\n";
		else
			std::cout << " is not an element of mymap.\n";
	}
}

void testmap7()
{
	std::map<std::string, std::string> mymap;

	mymap.insert(make_pair("sort", "排序"));
	mymap["insert"];  // 插入
	mymap["insert"] = "插入"; // 插入+修改value返回值
	mymap["left"] = "左边";  // 插入+修改

	for (auto& e : mymap)
	{
		cout << e.first << "=>" << e.second << endl;
	}

}

//V& operator[](const K& key)
//{
//	pair<iterator, bool> ret = insert(make_pair(key, V());
//	return ret.first->second;
//}


void test_map()
{
	string arr[] = { "苹果","香蕉","橙子","苹果","香蕉","橙子","苹果","香蕉","橙子","梨","柠檬" };

	map<string, int> fruitcount;
	for (auto& e : arr)
	{
		map<string, int>::iterator it = fruitcount.find(e);
		if (it != fruitcount.end())
		{
			it->second++;
		}
		else
		{
			// 刚进入
			fruitcount.insert(make_pair(e, 1));

		}
	}

	// 遍历
	map<string, int>::iterator it = fruitcount.begin();
	while (it != fruitcount.end())
	{
		cout << it->first << "=>" << it->second << endl;
		++it;
	}
	cout << endl;
}

void test_map1()
{
	string arr[] = { "苹果","香蕉","橙子","苹果","香蕉","橙子","苹果","香蕉","橙子","梨","柠檬" };
	map<string, int> fruitcount;
	for (auto& e : arr)
	{
		//1、e不在fruitcount中，插入pair(e, int()),然后对返回次数++
		//2、e在的话，就返回value的引用次数++
		fruitcount[e]++;
	}
	// 遍历
	map<string, int>::iterator it = fruitcount.begin();
	while (it != fruitcount.end())
	{
		cout << it->first << "=>" << it->second << endl;
		++it;
	}
	cout << endl;
}

void test_map2()
{
	multimap<string, string> mdict;
	mdict.insert(make_pair("sort", "排序"));
	mdict.insert(make_pair("left", "左边"));
	mdict.insert(make_pair("left", "右边"));
	mdict.insert(make_pair("left", "不知道"));

	// 遍历
	for (auto& e : mdict)
	{
		cout << e.first << "=>" << e.second << endl;
	}
	cout << endl;
}

int main()
{
	test_map2();
	return 0;
}