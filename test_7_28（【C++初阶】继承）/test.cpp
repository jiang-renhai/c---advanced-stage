#include<iostream>
using namespace std;

//class Person
//{
//public:
//	void Print()
//	{
//		cout << "name:" << _name << endl;
//		cout << "age:" << _age << endl;
//	}
//protected:
//	string _name = "peter"; // 姓名
//	int _age = 18;  // 年龄
//};
//
//class Student : public Person
//{
//protected:
//	int _stuid; // 学号
//};
//
//class Teacher : public Person
//{
//protected:
//	int _jobid; // 工号
//};
//
//int main()
//{
//	Student s;
//	Teacher t;
//	s.Print();
//	t.Print();
//	return 0;
//}


//class Person
//{
//public:
//	string _name = "zhangsan";
//	string _sex = "nan";
//	int _age = 2;
//};
//class Student : public Person
//{
//public:
//	int _No;
//};
//
//int main()
//{
//	Person p;
//	Student s;
//
//	Person pr = s;
//	Person& ps = s;
//	ps._name = "lisi";
//
//	Person* ptr = &s;
//	ptr->_name = "wangwu";
//	return 0;
//}





//int main()
//{
//	Student s1;
//	// 1.子类对象可以赋值给父类对象/指针/引用
//	Person p1 = s1;
//	Person* pp = &s1;
//	Person& rp = s1;
//
//	//2.基类对象不能赋值给派生类对象
//	s1 = p1;
//
//	// 3.基类的指针可以通过强制类型转换赋值给派生类的指针
//	pp = &s1;
//	Student* ps1 = (Student*)pp;
//	ps1->_No = 10;
//	return 0;
//}


//class Person
//{
//public:
//	void Print() { cout << "Person::_num:" << _num << endl; }
//protected:
//	string _name = "zhangsan";
//	int _num = 1;
//};
//
//class Student :public Person
//{
//public:
//	void Print() { cout << "Student::_num:" << _num << endl; }
//protected:
//	int _num = 9;
//};
//
//int main()
//{
//	Student s;
//	s.Print();
//	s.Person::Print();
//	return 0;
//}



//class Person
//{
//public:
//	void fun()
//	{
//		cout << "func()" << endl;
//	}
//};
//class Student : public Person
//{
//public:
//	void fun(int i)
//	{
//		cout << "func(int i)->" << i << endl;
//	}
//};
//
//int main()
//{
//	Student s;
//	s.fun(10);
//	s.Person::fun();
//	return 0;
//}



//class Person
//{
//public:
//	Person(const char* name = "peter") // 默认构造
//	//Person(const char* name)
//		: _name(name)
//	{
//		cout << "Person()" << endl;
//	}
//
//	Person& operator=(const Person& p)
//	{
//		cout << "Person operator=(const Person& p)" << endl;
//		if (this != &p)
//			_name = p._name;
//
//		return *this;
//	}
//
//	Person(const Person& p)
//		: _name(p._name)
//	{
//		cout << "Person(const Person& p)" << endl;
//	}
//
//	~Person()
//	{
//		cout << "~Person()" << endl;
//	}
//protected:
//	string _name; // 姓名
//};
//
//class Student : public Person
//{
//public:
//	Student(const char* name = "zhangsan", int id = 0)
//		:Person(name)
//		, _id(id)
//	{}
//	Student& operator=(const Student& s)
//	{
//		if (this != &s)
//		{
//			Person::operator=(s); // 父类的成员
//			_id = s._id;  // 子类的成员
//		}
//		return *this;
//	}
//
//	//Student(const Student& s)
//	//	:_name(s.name)
//	//	,_id(s._id)
//	//{}
//	Student(const Student& s)
//		:Person(s)
//		,_id(s._id)
//	{}
//	~Student()
//	{
//		cout << "~Student" << endl;
//	}
//protected:
//	int _id;
//};
//
//int main()
//{
//	Student s1("lisi", 4);
//	Student s2;
//	s2 = s1;
//	return 0;
//}

//class Student;
//class Person
//{
//public:
//	friend void Display(const Person& p, const Student& s);
//protected:
//	string _name;
//};
//
//class Student : public Person
//{
//public:
//	friend void Display(const Person& p, const Student& s);
//protected:
//	int _num;
//};
//
//void Display(const Person& p, const Student& s)
//{
//	cout << p._name << endl;
//	cout << s._num << endl;
//}
//
//int main()
//{
//	Person p;
//	Student s;
//	Display(p, s);
//	return 0;
//}


//class Person
//{
//public:
//	Person() { ++_count; }
////protected:
//	string _name;
//public:
//	static int _count;
//};
//int Person::_count = 0; // 静态成员
//class Student : public Person
//{
//protected:
//	int _num;
//};
//class Graduate : public Student
//{
//protected:
//	string _Course;
//};
//
//int main()
//{
//	Person p;
//	Student s;
//	cout << &p._name << endl;
//	cout << &s._name << endl << endl;
//
//	cout << &p._count << endl;
//	cout << &s._count << endl << endl;
//
//	cout << &Person::_count << endl;
//	cout << &Student::_count << endl << endl;
//	return 0;
//}



//class Person
//{
//public:
//	string _name;
//	int _age;
//};
//class Student : virtual public Person
//{
//protected:
//	int _num;
//};
//class Teacher : virtual public Person
//{
//protected:
//	int _id;
//};
//class Assistant : public Student, public Teacher
//{
//protected:
//	string _majorCourse;
//};
//
//int main()
//{
//	Assistant as;
//	as.Student::_age = 18;
//	as.Person::_age = 33;
//	as._age = 19;
//
//	return 0;
//}



//class A
//{
//public:
//	int _a;
//};
//class B : public A{
//public:
//	int _b;
//};
//class C : public A
//{
//public:
//	int _c;
//};
//class D : public B, public C
//{
//public:
//	int _d;
//};
//int main()
//{
//	D d;
//	d.B::_a = 1;
//	d.C::_a = 2;
//	d._b = 3;
//	d._c = 4;
//	d._d = 5;
//	return 0;
//}

//class A
//{
//public:
//	int _a;
//};
//class B : virtual public A {
//public:
//	int _b;
//};
//class C : virtual public A
//{
//public:
//	int _c;
//};
//class D : public B, public C
//{
//public:
//	int _d;
//};
//int main()
//{
//	D d;
//	d.B::_a = 1;
//	d.C::_a = 2;
//	d._b = 3;
//	d._c = 4;
//	d._d = 5;
//	d._a = 6;
//	return 0;
//}




