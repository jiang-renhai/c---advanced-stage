#include<iostream>
#include"labyrinth.h"
#include<stdlib.h>
#include<windows.h>

using namespace std;

// 模拟搞个栈
// 题目给的是用链栈
// 所以还是模拟一个吧

// 初始化
void Stack::InitStack(Stack* st)
{
    // 开个头结点指针
    st->_top = new Node;
    if (st->_top == nullptr)
    {
        perror("new failed\n");
        exit(-1);
    }
    else
    {
        // 头结点和尾结点一样
        st->_bottom = st->_top;
        // next指向空
        st->_top->_next = nullptr;
    }
    return;
}

// 入栈尾插
void Stack::PushStack(Stack* st, Box bx)
{
    // 创建一个新结点
    Node* newnode = new Node;

    // 头插法
    newnode->_data = bx;
    newnode->_next = st->_top;
    st->_top = newnode;
}

// 判空
bool Stack::IsEmpty(Stack* st)
{
    // 栈顶指针和栈底指针地址一样时候，空；否则为不空
    if (st->_top == st->_bottom)
    {
        return true;
    }
    else
    {
        return false;
    }
}

// 出栈
void Stack::Pop(Stack* st, Box& bx)
{
    if (IsEmpty(st))
        return;
    else
    {
        // 记录一下栈顶元素
        Node* pTop = st->_top;
        bx = pTop->_data;
        // 栈顶元素指向下一个元素
        st->_top = pTop->_next;
        // 将该记录的栈顶之前的结点释放掉
        free(pTop);
        pTop = nullptr;
        return;
    }
}

//打印
void Stack::Print(Stack* st)
{
    Node* p = st->_top;
    while (p != st->_bottom)
    {
        cout << "(" << p->_data._x << ", " << p->_data._y << ", " << p->_data._di << ")" << endl;
        p = p->_next;
    }
    cout << endl;
    return;
}


Box Stack::GetElement(Stack& st, Box& tmp)
{
    st.Pop(&st, tmp);
    Box res = tmp;

    if (st.IsEmpty(&st))
        return res;
    else
    {
        Box last = GetElement(st, tmp);	
        st.PushStack(&st, res);
        return last;
    }
}


// 栈内链表反转
void Stack::Reverse(Stack& st, Box& temp)
{
    if (st.IsEmpty(&st)) 
        return;
    Box i = GetElement(st, temp);
    st.Reverse(st, temp);
    st.PushStack(&st, i);
}


// 核心代码（贪心算法）找路径带着回溯
bool findPath(int** maze, Stack& s, int M, int N)
{
    // 边界构建全是1，其余自己构建
    // 1 1 1 1 1
    // 1 x x x 1
    // 1 x x x 1
    // 1 x x x 1
    // 1 1 1 1 1
    // 二重循环
    for (int i = 0; i < M + 2; ++i)
    {
        for (int j = 0; j < N + 2; j++)
        {
            if (i == 0 || i == M + 1)
            {
                maze[i][j] = 1;
            }
            else if (j == 0 || j == N + 1)
            {
                maze[i][j] = 1;
            }
            else
            {
                // 自己输入
                cin >> maze[i][j];
            }
        }
    }

    // 定义四个方向
    //   incX    incY
    // 0   0      1
    // 1   1      0
    // 2   0     -1
    // 3  -1      0
    Direction direct[4]{};
    direct[0].incX = 0; direct[0].incY = 1;
    direct[1].incX = 1; direct[1].incY = 0;
    direct[2].incX = 0; direct[2].incY = -1;
    direct[3].incX = -1; direct[3].incY = 0;

    Box tmp;
    int x, y, di; // 当前所处的单元格
    int line, col; // 下一步要走的单元格
    maze[1][1] = -1;

    // 定义一下该格子
    tmp._x = 1;
    tmp._y = 1;
    tmp._di = -1; // 代表该格子已经被访问过了，正好下一个先访问的是右边

    // 先push进这个元素
    s.PushStack(&s, tmp);
    while (!s.IsEmpty(&s))
    {
        // 回溯即出度：当碰到死胡同的时候走错路的时候推栈往前走
        s.Pop(&s, tmp);
        // 定义一下当前box的值的情况
        x = tmp._x; y = tmp._y; di = tmp._di + 1; // +1有细节，因为曾经是0，加1则为1，表示走过的点
        // 走走四个方向
        while (di < 4)
        {
            // 下一步要走的坐标
            line = x + direct[di].incX;
            col = y + direct[di].incY;

            // 试走
            if (maze[line][col] == 0)
            {
                tmp._x = x; tmp._y = y; tmp._di = di; // 前面x,y.di的作用就显示出来了
                s.PushStack(&s, tmp);
                x = line; y = col; maze[line][col] = -1; // 标记为-1表示已经走过
                if (x == M && y == N)
                {
                    s.Reverse(s, tmp);
                    return true; // 迷宫有路
                }
                else
                {
                    di = 0;
                }
            }
            else
            {
                ++di;
            }
            //cout << di;
        }
    }

    return false;
}


// 销毁函数即析构函数
void Stack::Destory(Stack* s, Box bx)
{
    while (!IsEmpty(s))
    {
        Pop(s, bx);
    }
    free(s->_top);
    s->_top = nullptr;
}

// 打印迷宫
void Chess(int** maze, int M, int N)
{
    printf("\n棋盘表示:       0  1  2  3  4  5  6  7  8  9\n");
    printf("              ┌──┬──┬──┬──┬──┬──┬──┬──┬──┬──┬\n");
    printf("             0");

    int s = 0;
    for (s = 0; s < M + 1; s++)
    {
        for (int j = 0; j <= N + 1; j++)
        {
            if (maze[s][j] < N + 2)
                printf("│%-2d", maze[s][j]); 
            else
                printf("│%d", maze[s][j]);
        }
        printf("│\n");

        printf("              ");
        printf("├──┼──┼──┼──┼──┼──┼──┼──┼──┼──┼\n");
        printf("             %d", s + 1);
    }

    for (int j = 0; j <= N + 1; j++)
    {
        if (maze[s][j] < N + 2) 
            printf("│%-2d", maze[s][j]);
        else
            printf("│%-2d", maze[s][j]);
    }
    printf("│\n");

    printf("              ");
    printf("└──┴──┴──┴──┴──┴──┴──┴──┴──┴──┴\n");
}


void menu()
{
    cout << "\t\t\t这是一个迷宫小游戏\t\t\t" << endl;
    cout<<"\t\t\t我们有以下功能：\t\t\t"<<endl;
    cout << "\t\t\t1.输入迷宫并输出路径及平面图\t\t\t" << endl;
    cout << "\t\t\t0.退出\t\t\t" << endl;
}

int main()
{
    while (1)
    {
        menu();
        int choice = 0;
        cin >> choice;

        int M, N;

        if (choice == 1)
        {
            cout << "\t\t\t请输入迷宫数组行数和列数$";
            cin >> M >> N;

            // new一个动态的二维数组
            int** maze = new int* [M + 2];
            for (int i = 0; i < N + 3; i++)
            {
                maze[i] = new int[N + 2];
            }

            bool res;
            Box bx;
            bx._di = 0;

            Stack s;
            s.InitStack(&s);
            res = findPath(maze, s, M, N);

            if (res)
            {
                s.Print(&s);
                Chess(maze, M, N);
            }
            else
            {
                cout << "没有通路！" << endl;
            }
            s.Destory(&s, bx);

            // 不需要析构函数，全局结束自动析构
            //for (int i = 0; i < N; i++)
            //{
            //    delete[] maze[i];
            //}
            //delete[] maze;

            system("pause");
            system("cls");

        }
        else if (choice == 0)
        {
            break;
        }
        else
        {
            cout << "\t\t\t输入错误，请重新输入\t\t\t" << endl;
            Sleep(1000);
            system("cls");
            main();
        }
    }

    return 0;
}
