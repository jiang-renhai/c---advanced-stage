#pragma once


struct Box
{
    int _x;  // 坐标x
    int _y;  // 坐标y
    int _di; // 下一个可走方位的方向号
};

struct Direction
{
    // 表
    //  incX    incY
    // 0  0      1
    // 1  1      0
    // 2  0      -1
    // 3  -1     0
    // 根据上面的表上下左右动
    int incX;
    int incY;
};

// 定义栈结点
typedef struct Node
{
    Box _data;
    Node* _next;
}Node;

// 栈
class Stack
{
private:
    Node* _top; // 栈顶
    Node* _bottom; // 栈底=

public:
    Stack()
        //:_top(nullptr)
        //,_bottom(nullptr)
    {}


    void InitStack(Stack*);
    void PushStack(Stack*, Box);


    Box GetElement(Stack&, Box&);
    void Reverse(Stack&, Box&);

    void Print(Stack*);
    void Pop(Stack*, Box&);
    bool IsEmpty(Stack*);

    void Destory(Stack*, Box);
};


