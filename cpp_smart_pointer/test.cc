#include<iostream>
#include<thread>
#include<mutex>
using namespace std;

// auto_ptr
namespace JRH
{
	template<class T>
	class auto_ptr
	{
	public:
		// RAII构造函数
		auto_ptr(T* ptr = nullptr)
			:_ptr(ptr)
		{}
		~auto_ptr()
		{
			if (_ptr != nullptr)
			{
				std::cout << "delete:" << _ptr << std::endl;
				delete _ptr;
				_ptr = nullptr;
			}
		} 
		// 
		// 拷贝构造
		auto_ptr(auto_ptr<T>& ap)
			:_ptr(ap._ptr)
		{
			ap._ptr = nullptr;
		}
		// 拷贝赋值
		auto_ptr& operator=(auto_ptr<T>& ap)
		{
			if (this != &ap)
			{
				delete _ptr; // 释放自己管理的资源
				_ptr = ap._ptr; // 接管ap对象的资源
				ap._ptr = nullptr; // 管理权转移后ap被置空
			}
			return *this;
		}

		T& operator*()
		{
			return *_ptr;
		}
		T* operator->()
		{
			return _ptr;
		}
	private:
		T* _ptr; // 管理的资源
	};
}

// unique_ptr
namespace JRH
{
	template<class T>
	class unique_ptr
	{
	private:
		T* _ptr;
	public:
		// RAII构造函数
		unique_ptr(T* ptr = nullptr)
			:_ptr(ptr)
		{}
		// 析构函数
		~unique_ptr()
		{
			if (_ptr != nullptr)
			{
				std::cout << "delete:" << _ptr << std::endl;
				delete _ptr;
				_ptr = nullptr;
			}
		}
		// &
		T& operator*()
		{
			return *_ptr;
		}
		// *
		T* operator->()
		{
			return _ptr;
		}
		// 拷贝构造
		unique_ptr(unique_ptr<T>& up) = delete;
		// 拷贝赋值
		unique_ptr& operator=(unique_ptr<T>& up) = delete;
	};
}

//// shared_ptr
//namespace JRH
//{
//	template<class T>
//	class shared_ptr
//	{
//	private:
//		T* _ptr; // 管理资源
//		int* _pcount; // 计数器
//	public:
//		shared_ptr(T* ptr = nullptr)
//			:_ptr(ptr)
//			, _pcount(new int(1))
//		{}
//		~shared_ptr()
//		{
//			if (--(*_pcount) == 0)
//			{
//				if (_ptr == nullptr)
//				{
//					std::cout << "delete: " << _ptr << std::endl;
//					delete _ptr;
//					_ptr = nullptr;
//				}
//				delete _pcount;
//				_pcount = nullptr;
//			}
//		}
//		shared_ptr(shared_ptr<T>& sp)
//			:_ptr(sp._ptr)
//			, _pcount(sp._pcount)
//		{
//			(*_pcount)++;
//		}
//		shared_ptr& operator=(shared_ptr<T>& sp)
//		{
//			if (_ptr != sp._ptr) // 管理同一块资源的不用赋值操作
//			{
//				if (--(*_pcount) == 0) // 只有当引用计数为0的时候才进行销毁
//				{
//					std::cout << "delete: " << _ptr << std::endl;
//					delete _ptr;
//					delete _pcount;
//				}
//				// 进行赋值操作
//				_ptr = sp._ptr; // 让sp和当前对象一同管理数据
//				_pcount = sp._pcount; // 获取sp对象管理的资源对应的引用计数
//				(*_pcount)++; // 当前新增了对象进行管理相关的资源，引用计数++
//			}
//			return *this;
//		}
//		// 获取引用计数的大小
//		int use_count()
//		{
//			return *_pcount;
//		}
//		// 返回引用和指针
//		T& operator*()
//		{
//			return *_ptr;
//		}
//		T* operator->()
//		{
//			return _ptr;
//		}
//	};
//}

//// shared_ptr - lock()
//namespace JRH
//{
//	template<class T>
//	class shared_ptr
//	{
//	private:
//		T* _ptr; // 管理资源
//		int* _pcount; // 计数器
//		mutex* _pmutex; // 加锁
//	private:
//		// ++引用计数
//		void AddRef()
//		{
//			_pmutex->lock();
//			(*_pcount)++;
//			_pmutex->unlock();
//		}
//		// --引用计数
//		void ReleaseRef()
//		{
//			_pmutex->lock();
//			bool flag = false;
//			if (--(*_pcount) == 0) // 将管理资源的引用计数--
//			{
//				if (_ptr != nullptr)
//				{
//					// 销毁_ptr
//					std::cout << "delete: " << std::endl;
//					delete _ptr;
//					_ptr == nullptr;
//				}
//				delete _pcount;
//				_pcount = nullptr;
//				flag = true;
//			}
//			_pmutex->unlock(); // 解锁
//			if (flag == true)
//			{
//				delete _pmutex;
//			}
//		}
//	public:
//		shared_ptr(T* ptr = nullptr)
//			:_ptr(ptr)
//			, _pcount(new int(1))
//			, _pmutex(new mutex)
//		{}
//		~shared_ptr()
//		{
//			ReleaseRef();
//		}
//		shared_ptr(shared_ptr<T>& sp)
//			:_ptr(sp._ptr)
//			, _pcount(sp._pcount)
//			, _pmutex(sp._pmutex)
//		{
//			AddRef();
//		}
//		shared_ptr& operator=(shared_ptr<T>& sp)
//		{
//			if (_ptr != sp._ptr) // 管理同一块资源的不用赋值操作
//			{
//				ReleaseRef();         //将管理的资源对应的引用计数--
//				_ptr = sp._ptr;       //与sp对象一同管理它的资源
//				_pcount = sp._pcount; //获取sp对象管理的资源对应的引用计数
//				_pmutex = sp._pmutex; //获取sp对象管理的资源对应的互斥锁
//				AddRef();             //新增一个对象来管理该资源，引用计数++
//			}
//			return *this;
//		}
//		// 获取引用计数的大小
//		int use_count()
//		{
//			return *_pcount;
//		}
//		// 返回引用和指针
//		T& operator*()
//		{
//			return *_ptr;
//		}
//		T* operator->()
//		{
//			return _ptr;
//		}
//	};
//}

// 删除器
namespace JRH
{
	template<class T>
	struct Delete
	{
		void operator()(const T* ptr)
		{
			std::cout << "delete: " << std::endl;
			delete ptr;
		}
	};
	template<class T, class D = Delete<T>>
	class shared_ptr
	{
	private:
		T* _ptr; // 管理资源
		int* _pcount; // 计数器
		mutex* _pmutex; // 加锁
		D _del;
	public:
		shared_ptr(T* ptr, D del)
			: _ptr(ptr)
			, _pcount(new int(1))
			, _pmutex(new mutex)
			, _del(del)
		{}
	private:
		// --引用计数
		void ReleaseRef()
		{
			_pmutex->lock();
			bool flag = false;
			if (--(*_pcount) == 0) // 将管理资源的引用计数--
			{
				if (_ptr != nullptr)
				{
					// 销毁_ptr
					std::cout << "delete: " << std::endl;
					delete _ptr;
					_ptr == nullptr;
				}
				delete _pcount;
				_pcount = nullptr;
				flag = true;
			}
			_pmutex->unlock(); // 解锁
			if (flag == true)
			{
				delete _pmutex;
			}
		}
	};
}

namespace JRH
{
	template<class T>
	class weak_ptr
	{
	private:
		T* _ptr;
	public:
		// 构造函数
		weak_ptr()
			:_ptr(nullptr)
		{}
		// 拷贝构造
		weak_ptr(const std::shared_ptr<T>& sp)
			:_ptr(sp.get())
		{}
		// 拷贝赋值
		weak_ptr& operator=(const std::shared_ptr<T>* sp)
		{
			_ptr = sp.get();
			return *this;
		}
		// & *
		T& operator*()
		{
			return *_ptr;
		}
		T* operator->()
		{
			return _ptr;
		}
	};
}


struct ListNode
{
	std::weak_ptr<ListNode> _next;
	std::weak_ptr<ListNode> _prev;
	int _val;
	// 析构函数
	~ListNode()
	{
		std::cout << "~ListNode() " << std::endl;
	}
};
int main()
{
	std::shared_ptr<ListNode> node1(new ListNode);
	std::shared_ptr<ListNode> node2(new ListNode);
	std::cout << node1.use_count() << std::endl;
	std::cout << node2.use_count() << std::endl;
	node1->_next = node2;
	node2->_prev = node1;
	std::cout << node1.use_count() << std::endl;
	std::cout << node2.use_count() << std::endl;
	// ...
	return 0;
}

//struct ListNode
//{
//	std::shared_ptr<ListNode> _next;
//	std::shared_ptr<ListNode> _prev;
//	int _val;
//	// 析构函数
//	~ListNode()
//	{
//		std::cout << "~ListNode() " << std::endl;
//	}
//};
//int main()
//{
//	std::shared_ptr<ListNode> node1(new ListNode);
//	std::shared_ptr<ListNode> node2(new ListNode);
//	node1->_next = node2;
//	node2->_prev = node1;
//	// ...
//	return 0;
//}


//struct ListNode
//{
//	ListNode* _next;
//	ListNode* _prev;
//	int _val;
//	// 析构函数
//	~ListNode()
//	{
//		std::cout << "~ListNode() " << std::endl;
//	}
//};
//template<class T>
//struct Del
//{
//	void operator()(const T* ptr)
//	{
//		std::cout << "delete[]: " << std::endl;
//		delete[] ptr;
//	}
//};
//
//int main()
//{
//	std::shared_ptr<ListNode> sp1(new ListNode[10], Del<ListNode>());
//	std::shared_ptr<FILE> sp2(fopen("test.txt", "r"), [](FILE* ptr) {
//		std::cout << "fclose: " << std::endl;
//		fclose(ptr);
//	});
//	return 0;
//}

//struct ListNode
//{
//	ListNode* _next;
//	ListNode* _prev;
//	int _val;
//	// 析构函数
//	~ListNode()
//	{
//		std::cout << "~ListNode() " << std::endl;
//	}
//};
//
//int main()
//{
//	std::shared_ptr<ListNode> sp1(new ListNode[10]); // error
//	std::shared_ptr<FILE> sp2(fopen("test.txt", "r")); // error
//	return 0;
//}


//void func(JRH::shared_ptr<int>& sp, int n)
//{
//	for (int i = 0; i < n; i++)
//	{
//		JRH::shared_ptr<int> copy(sp);
//	}
//}
//
//
//int main()
//{
//	JRH::shared_ptr<int> p(new int(0));
//
//	const int n = 1000;
//	thread t1(func, std::ref(p), n);
//	thread t2(func, std::ref(p), n);
//
//	t1.join();
//	t2.join();
//
//	cout << p.use_count() << endl; //预期：1
//
//	return 0;
//}


//void func(JRH::shared_ptr<int>& sp, size_t n)
//{
//	for (size_t i = 0; i < n; i++)
//	{
//		JRH::shared_ptr<int> copy(sp);
//	}
//}
//int main()
//{
//	JRH::shared_ptr<int> p(new int(0));
//	const size_t n = 1000;
//	thread t1(func, p, n);
//	thread t2(func, p, n);
//	t1.join();
//	t2.join();
//	std::cout << p.use_count() << std::endl; // 1
//	return 0;
//}

//int main()
//{
//	shared_ptr<int> sp1(new int(1));
//	shared_ptr<int> sp2(sp1);
//	*sp1 = 10;
//	*sp2 = 20;
//	std::cout << sp1.use_count() << std::endl; // 2
//	shared_ptr<int> sp3(new int(1));
//	shared_ptr<int> sp4(new int(2));
//	sp3 = sp4;
//	std::cout << sp3.use_count() << std::endl; // 2
//	return 0;
//}

//
//int main()
//{
//	//unique_ptr<int> up1(new int(1));
//	//// unique_ptr<int> up2(up1); // error
//	return 0;
//}

//int main()
//{
//	std::auto_ptr<int> ap1(new int(1));
//	std::auto_ptr<int> ap2(ap1); // 拷贝构造
//	*ap2 = 20;
//	// *ap1 = 10; // error
//
//	std::auto_ptr<int> ap3(new int(1));
//	std::auto_ptr<int> ap4(new int(2));
//	ap3 = ap4; // 拷贝赋值
//	return 0;
//}

//template<class T>
//class SmartPointer
//{
//private:
//	T* _ptr;
//public:
//	// 构造函数
//	SmartPointer(T* ptr)
//		:_ptr(ptr)
//	{}
//	~SmartPointer()
//	{
//		std::cout << "delete: " << _ptr << std::endl;
//		delete _ptr;
//	}
//	T& operator*()
//	{
//		return *_ptr;
//	}
//	T* operator->()
//	{
//		return _ptr;
//	}
//};
//
//int div()
//{
//	int a;
//	int b;
//	std::cin >> a >> b;
//	if (b == 0)
//	{
//		throw invalid_argument("除0错误");
//	}
//	return a / b;
//}
//
//void func()
//{
//	SmartPointer<int> sp(new int);
//	// ...
//	std::cout << div() << std::endl;
//	// ...
//}
//
//int main()
//{
//	SmartPointer<int> sp1(new int);
//	SmartPointer<int> sp2(sp1); // 拷贝构造
//	SmartPointer<int> sp3(new int);
//	SmartPointer<int> sp4(new int);
//	sp3 = sp4; // 拷贝赋值
//	return 0;
//}


//int main()
//{
//	try
//	{
//		func();
//	}
//	catch (exception& e)
//	{
//		std::cout << e.what() << std::endl;
//	}
//	return 0;
//}

//int div()
//{
//	int a;
//	int b;
//	std::cin >> a >> b;
//	if (b == 0)
//	{
//		throw invalid_argument("除0错误");
//	}
//	return a / b;
//}
//
//void func()
//{
//	int* ptr = new int;
//	try
//	{
//		std::cout << div() << std::endl;
//	}
//	catch (...)
//	{
//		delete ptr;
//		throw;
//	}
//	delete ptr;
//}
//
//int main()
//{
//	try
//	{
//		func();
//	}
//	catch (exception& e)
//	{
//		std::cout << e.what() << std::endl;
//	}
//	return 0;
//}

//int div()
//{
//	int a;
//	int b;
//	std::cin >> a >> b;
//	if (b == 0)
//	{
//		throw invalid_argument("除0错误");
//	}
//	return a / b;
//}
//
//void func()
//{
//	int* ptr = new int;
//	// ...
//	std::cout << div() << std::endl;
//	// ...
//	delete ptr;
//}
//
//int main()
//{
//	try
//	{
//		func();
//	}
//	catch (exception& e)
//	{
//		std::cout << e.what() << std::endl;
//	}
//	return 0;
//}
