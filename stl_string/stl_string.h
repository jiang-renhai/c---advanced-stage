#pragma once
#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string.h>
#include<assert.h>
using namespace std;

namespace JRH
{
	class string
	{
	public:
		typedef char* iterator; // 迭代器（原生）
		typedef const char* const_iterator; // const迭代器（原生）
	public:
		// 1、默认成员函数
		string(const char* str = "")         // 构造函数(""代表着个数为0)
		{
			_size = strlen(str); // 初始化定义字符串当前的长度
			_capacity = _size; // 初始化容量就是当前长度
			_str = new char[_capacity + 1]; // 多开一个空间以便存放'\0'
			strcpy(_str, str); // 将传入的str拷贝给this对象的_str
		}
		//string(const string& s)              // 拷贝构造函数 -- 传统写法
		//	:_str(new char[s._capacity + 1]) // 多开一个空间用来存放'\0'
		//	, _size(s._size)
		//	, _capacity(s._capacity)
		//{
		//	strcpy(_str, s._str); // 将s的字符串拷贝过去
		//	_size = s._size;      // s的数量拷贝过去
		//	_capacity = s._capacity; // 容量也拷贝过去
		//}
		string(const string& s)              // 拷贝构造函数 -- 现代写法
			:_str(nullptr)
			, _size(0)
			, _capacity(0)
		{
			string tmp(s._str); // 调用构造函数，构造出一个C字符串为s._str的对象
			swap(tmp); // 交换这两个对象
		}
		//string& operator=(const string& s)   // 赋值运算符重载函数 -- 传统写法
		//{
		//	if (this != &s)
		//	{
		//		delete[] _str; // 释放掉原来的空间
		//		_str = new char[s._capacity + 1]; // 申请一块空间，+1为'\0'
		//		strcpy(_str, s._str); // 将s._str拷贝一份到_str
		//		_size = s._size; // 将s的大小给对象
		//		_capacity = s._capacity; // 同样将容量给对象
		//	}
		//	return *this; // 支持连续赋值
		//}
		string& operator=(string s)   // 赋值运算符重载函数 -- 现代写法1（编译器接收右值的时候自动调用拷贝构造函数）
		{
			swap(s); // 交换两个对象
			return *this; // 支持连续赋值
		}
		//string& operator=(const string& s)   // 赋值运算符重载函数 -- 现代写法2
		//{
		//	if (this != &s) // 避免自己给自己赋值
		//	{
		//		string tmp(s._str); // 建立一个临时对象
		//		swap(tmp); // 交换
		//	}
		//	return *this; // 支持连续赋值，返回左值
		//}
		~string()                            // 析构函数
		{
			delete[] _str; // 将对象空间释放掉
			_str = nullptr; // 将指针置为空防止野指针
			_size = 0; // 将空间大小置为0
			_capacity = 0; // 将空间容量置为0
		}

		// 2、迭代器相关函数
		iterator begin()                  // 头
		{
			return _str; // 实现首字母的地址
		}
		iterator end()                    // 尾
		{
			return _str + _size; // 实现尾字符的下一个字符的地址
		}
		const_iterator begin() const      // const头
		{
			return _str; // 实现首字母的const地址
		}
		const_iterator end() const        // const尾
		{
			return _str + _size; // 实现尾字符的下一个字符的const地址
		}

		// 3、容量和大小相关函数
		size_t size()                          // 有效个数大小
		{
			return _size; // 返回当前字符串的有效长度
		}
		size_t capacity()                      // 总的容量
		{
			return _capacity; // 返回当前字符串的容量
		}
		void reserve(size_t n)                 // 扩容，不可缩容（大小不变）
		{
			if (n > _capacity) // 只有当n大于容量的时候才进行操作
			{
				char* tmp = new char[n + 1]; // 多开一个空间用于存放'\0'
				strncpy(tmp, _str, _size + 1); // 将_str的所有都拷贝到tmp临时对象中，包括一堆\0
				delete[] _str; // 将其_str释放掉
				_str = tmp; // 新开辟的空间给_str
				_capacity = n; // 将n给对象的新的容量
			}
		}
		void resize(size_t n, char ch = '\0')  // 扩容，也可缩容（改变大小）
		{
			if (n <= _size) // n比大小小的时候
			{
				_size = n; // 将_size调成n
				_str[_size] = '\0'; // 将_str的最后一个元素直接定为'\0'
			}
			else
			{
				if (n > _capacity) // 判断是否需要增容
				{
					reserve(n);
				}
				for (size_t i = 0; i < n; i++) // 将_size扩大到n，ch为一个个字符
				{
					_str[i] = ch;
				}
				_size = n; // _size更新
				_str[_size] = '\0'; // 最后加一个\0
			}
		}
		bool empty() const                     // 判空
		{
			return strcmp(_str, "") == 0; // 比较_str是否为空
		}

		// 4、修改字符串相关函数
		void push_back(char ch)                      // 尾插一个字符
		{
			if (_size == _capacity) // 满的时候
			{
				// 扩容
				reserve(_capacity == 0 ? 4 : _capacity * 2);
			}
			_str[_size] = ch; // 最后一个字符定为ch
			_str[_size++] = '\0'; // 先实现最后一个字符的后一个字符为\0，再将_size++
		}
		void append(const char* str)                 // 尾增一个字符串
		{
			size_t len = _size + strlen(str); // 尾插str后字符串的大小（不包括'\0'）
			if (len > _capacity) // 判断是否需要增容
			{
				reserve(len); // 增容
			}
			strcpy(_str + _size, str); // 将str尾插到字符串后面
			_size = len; // 字符串大小改变
		}
		string& operator+=(char ch)                  // 尾增一个字符
		{
			push_back(ch); // 调用push_back 
			return *this;  // 支持连续+=
		}
		string& operator+=(const char* str)          // 尾增一个字符串 
		{
			append(str); // 调用append
			return *this; // 支持连续+=，且其为左值
		}
		string& insert(size_t pos, char ch)          // 随机插入一个字符
		{
			assert(pos <= _size); // 检查下标合法性
			if (_size == _capacity) // 增容
			{
				reserve(_capacity == 0 ? 4 : _capacity * 2);
			}
			// 定义end到除了末尾\0的最末位元素
			char* end = _str + _size; 
			while (end >= _str + pos) // end从后往前到pos位置时候
			{
				*(end + 1) = *end; // 后面一个字符等于前面一个字符，等于字符往后移动
				end--;
			}
			_str[pos] = ch; // 插入
			_size++; // 有效字符长度++
			return *this; // 支持连续赋值，左值
		}
		string& insert(size_t pos, const char* str)  // 随机插入一个字符串
		{
			assert(pos <= _size); // 坐标合法
			size_t len = strlen(_str); // 定义一下长度
			if (len + _size > _capacity) // 扩容
			{
				reserve(len + _size); // 扩大到len+_size的大小的容量
			}
			char* end = _str + _size; // end定义为最末尾元素（\0前一个）
			while (end >= _str + pos) // 从后往前找位置
			{
				*(end + len) = *end;
				end--;
			}
			strncpy(_str + pos, str, len); // pos位置开始放上指定字符串
			_size += len;
			return *this; // 支持连续插入
		}
		string& erase(size_t pos, size_t len)        // 删除
		{
			assert(pos < _size); // 检测下标的合法性
			size_t n = _size - pos; // 计算后面数的大小
			if (len >= n) // 此时pos后面的数全部删掉
			{
				_size = pos; // 更新此时_size的大小
				_str[_size] = '\0'; // 直接定义\0即可
			}
			else // 删一部分
			{
				strcpy(_str + pos, _str + pos + len); // 将len后面的数覆盖以pos为开头的数
				_size -= len; // 更新_str
			}
			return *this; // 支持连续删除
		}
		void clear()                                 // 清空
		{
			_size = 0; // 将_size到首元素位置
			_str[_size] = '\0'; // 首元素直接定义为\0
		}
		void swap(string& s)                         // 交换
		{
			std::swap(_str, s._str); // 交换字符串
			std::swap(_size, s._size); // 交换有效字符大小
			std::swap(_capacity, s._capacity); // 交换容量
		}
		const char* c_str() const                     // 与C字符进行交互
		{
			return _str;
		}

		// 5、访问字符串相关函数
		char& operator[](size_t i)                          // 访问
		{
			assert(i < _size); // 检测下标的合法性
			return _str[i]; // 可读可写
		}
		const char& operator[](size_t i) const              // const访问 
		{
			assert(i < _size); // 检测下标的合法性
			return _str[i]; // 只能读不能写
		}
		size_t find(char ch, size_t pos = 0)         // 查找字符
		{
			assert(pos < _size); // 检查坐标合法性
			for (size_t i = pos; i < _size; i++)
			{
				if (_str[i] == ch)
				{
					return i; // 找到了，返回下标
				}
			}
			return npos; // 没有找到对应字符，返回npos
		}
		size_t find(const char* str, size_t pos = 0)  // 查找字符串
		{
			assert(pos < _size); // 检查坐标合法性
			const char* ret = strstr(_str + pos, str); // 用strstr查找相对应的字符串
			if (ret)
			{
				return ret - _str; // 返回字符串第一个元素下标
			}
			else
			{
				return npos; // 没找到返回npos
			}
		}
		size_t rfind(char ch, size_t pos = npos)      // 整数查找
		{
			string tmp(*this); // 拷贝构造一个tmp临时对象
			std::reverse(tmp.begin(), tmp.end()); // 调用reverse逆置对象tmp
			if (pos >= _size) // 所给pos大于字符串有效长度
			{
				pos = _size - 1; // 重新设置pos为字符串最后一个字符的下标
			}
			pos = _size - 1 - pos; // 将pos改为镜像对称后的位置
			size_t ret = tmp.find(ch, pos); // 复用find函数
			if (ret != npos)
				return _size - 1 - ret; // 找到了，返回ret镜像对称后的位置
			else
				return npos; // 没找到，返回npos
		}
		size_t rfind(const char* str, size_t pos = 0) // 查找第一个匹配字符串
		{
			string tmp(*this); // 拷贝构造对象tmp
			std::reverse(tmp.begin(), tmp.end()); // 调用reverse逆置对象tmp的C字符串
			size_t len = strlen(str); // 待查找的字符串的长度
			char* arr = new char[len + 1]; // 开辟arr字符串（用于拷贝str字符串）
			strcpy(arr, str); // 拷贝str给arr
			size_t left = 0, right = len - 1; // 设置左右指针
			// 逆置字符串arr
			while (left < right)
			{
				std::swap(arr[left], arr[right]);
				left++;
				right--;
			}
			if (pos >= _size) // 所给pos大于字符串有效长度
			{
				pos = _size - 1; // 重新设置pos为字符串最后一个字符的下标
			}
			pos = _size - 1 - pos; // 将pos改为镜像对称后的位置
			size_t ret = tmp.find(arr, pos); // 复用find函数
			delete[] arr; // 销毁arr指向的空间，避免内存泄漏
			if (ret != npos)
				return _size - ret - len; // 找到了，返回ret镜像对称后再调整的位置
			else
				return npos; // 没找到，返回npos
		}

		// 6、关系运算符重载
		bool operator>(const string& s) const   // 大于
		{
			return strcmp(_str, s._str) > 0;
		}
		bool operator>=(const string& s) const  // 大于等于
		{
			return (*this == s) || (*this > s);
		}
		bool operator<(const string& s) const   // 小于
		{
			return !(*this >= s);
		}
		bool operator<=(const string& s) const  // 小于等于
		{
			return !(*this > s);
		}
		bool operator==(const string& s) const  // 等于
		{
			return strcmp(_str, s._str) == 0;
		}
		bool operator!=(const string& s) const  // 不等于
		{
			return !(*this == s);
		}
	private:
		char* _str;                // 存储字符串
		size_t _size;              // 记录字符串当前的有效长度
		size_t _capacity;          // 记录字符串当前的容量
		static const size_t npos;  // 静态成员变量，只针对整型，整型最大值
	};
	// >> 和 << 运算符重载函数
	istream& operator>>(istream& in, string& s)
	{
		s.clear(); // 清空
		char ch = in.get(); // 读取第一个字符
		while (ch != ' ' && ch != '\n') // 当读取到的字符不是空格或'\n'的时候继续读取
		{
			s += ch; // 将读取到的字符尾插到字符串后面
			ch = in.get(); // 继续读取字符串
		}
		return in; // 支持连续输入
	}
	ostream& operator<<(ostream& out, const string& s)
	{
		for (auto ch : s) // 连续打印
		{
			out << s;
		}
		return out; // 支持连续输出
	}
	istream& getline(istream& in, string& s) // 读取一行有空格的字符
	{
		s.clear(); // 清空字符串
		char ch = in.get(); // 读取一个字符
		while (ch != '\n') // 当读取到的字符不是'\n'的时候继续读取
		{
			s += ch; // 将读取到的字符尾插到字符串后面
			ch = in.get(); // 继续读取字符
		}
		return in; // 支持连续输入
	}
	void Print(string& s)
	{
		for (size_t i = 0; i < s.size(); ++i)
		{
			cout << s[i] << " ";
		}
		cout << endl;
		// 只能读不能写
		string::const_iterator it = s.begin();
		while (it != s.end())
		{
			cout << *it << " ";
			++it;
		}
		cout << endl;
		for (auto ch : s)
		{
			cout << ch << " ";
		}
		cout << endl;
	}
	void Test_String1()
	{
		string s1("xxxxxxx\n");
		string s2("hello world\n");
		s1 = s2;
		cout << s1.c_str() << endl;
		cout << s1.c_str() << endl;
		cout << s1.c_str() << endl;
		cout << s2.c_str() << endl;

		s2[0]++;
		cout << s1.c_str() << endl;
		cout << s2.c_str() << endl;

		string s3(s2);
		cout << s2.c_str();
		cout << s3.c_str();
	}

	// 遍历string
	void Test_String2()
	{
		string s1("hello world\n");
		for (size_t i = 0; i < s1.size(); ++i)
		{
			// 修改
			s1[i]++;
		}
		cout << endl;
		for (size_t i = 0; i < s1.size(); ++i)
		{
			// 读取
			cout << s1[i] << " ";
		}
		cout << endl;
		Print(s1);
	}

	// 遍历string -- 迭代器
	void Test_String3()
	{
		string s1("hello world\n");
		string::iterator it = s1.begin();
		while (it != s1.end())
		{
			cout << *it << " ";
			++it;
		}
		cout << endl;
		for (auto ch : s1)
		{
			cout << ch << " ";
		}
		cout << endl;
	}

	void Test_String4()
	{
		string s1("hello world");
		string s2("yyyyy");
		string s3("xx");
		cout << (s1 < s2) << endl;
		cout << (s2 > s3) << endl;
		cout << (s2 == s3) << endl;
	}

	void Test_String5()
	{
		string s1("hello world");
		s1.push_back('x');
		s1.append("yyyy");
		cout << s1.c_str() << endl;
	}

	void Test_String6()
	{
		string s1("hello world");
		s1.insert(5, 'x');
		cout << s1.c_str() << endl;
	}

	void Test_String7()
	{
		string s1;
		s1.resize(20, 'x');
		cout << s1.c_str() << endl;
		s1.resize(30, 'y');
		cout << s1.c_str() << endl;
		s1.resize(10);
		cout << s1.c_str() << endl;
	}

	void Test_String8()
	{
		string s1("hello world");
		s1.insert(0, "xxx");
		cout << s1.c_str() << endl;
	}

	void Test_String9()
	{
		string s1;
		cin >> s1;
		cout << s1.c_str() << endl;
	}
}
