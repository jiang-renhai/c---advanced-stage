#include<iostream>
#include<string>
using namespace std;

class Exception
{
protected:
	int _errid; // 错误编号
	string _errmsg; // 错误描述
public:
	// 构造函数
	Exception(int errid, const char* errmsg)
		:_errid(errid)
		,_errmsg(errmsg)
	{}
	// 获取错误编号
	int GetErrid() const
	{
		return _errid;
	}
	virtual string what() const
	{
		return _errmsg;
	}
};

// 派生类NerworkException
class NetworkException : public Exception
{
protected:
	string _net; // 导致异常的network
public:
	// 构造函数
	NetworkException(int errid, const char* errmsg, const char* net)
		:Exception(errid, errmsg)
		,_net(net)
	{}
	virtual string what() const
	{
		string msg = "newworkexception:";
		msg += _errmsg;
		msg += "network语句：";
		msg += _net;
		return msg;
	}
};

// 派生类CacheException
class CacheException : public Exception 
{
protected:
	string _cache;
public:
	// 构造函数
	CacheException(int errid, const char* errmsg, const char* cache)
		:Exception(errid, errmsg)
		,_cache(cache)
	{}
	virtual string what() const
	{
		string msg = "cacheexception:";
		msg += _errmsg;
		msg += "chche语句";
		msg += _cache;
		return msg;
	}
};


//void func1()
//{
//	throw string("这是一个异常");
//}
//
//void func2()
//{
//	int* array = new int[10];
//	try
//	{
//		func1();
//	}
//	catch (...)
//	{
//		delete[] array;
//		throw; // 将捕获到的异常再次抛出
//	}
//	delete[] array;
//}

int main()
{
	try
	{
		func2();
	}
	catch (const string& s)
	{
		std::cout << "错误描述：" << s << std::endl;
	}
	catch (...)
	{
		std::cout << "未知错误：" << std::endl;
	}
	return 0;
}


//void func1()
//{
//	throw string("这是一个异常");
//}
//
//void func2()
//{
//	func1();
//}
//
//void func3()
//{
//	func2();
//}

//int main()
//{
//	try
//	{
//		func3();
//	}
//	catch (const string& s)
//	{
//		std::cout << "错误描述：" << s << std::endl;
//	}
//	catch (...)
//	{
//		std::cout << "未知错误：" << std::endl;
//	}
//	return 0;
//}

//try
//{
//	// 被保护的代码
//}
//catch (ExceptionName e1)
//{
//	// catch块
//}
//catch (ExcetionName e2)
//{
//	// catch块
//}
//// ... 
//catch (ExceptionName eN)
//{
//	// catch块
//}
