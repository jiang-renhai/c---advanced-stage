#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<cassert>
#include<string>
#include<list>
#include<cstring>
using namespace std;

// string类
namespace JRH
{
	class string
	{
	private:
		char* _str;
		size_t _size;
		size_t _capacity;
	public:
		// 构造函数
		string(const char* str = "")
		{
			std::cout << "string(const char* str) -- 构造函数" << std::endl;
			_size = strlen(str); // 初始时字符串大小设置成字符字符串长度
			_capacity = _size; // 初始时字符串容量设置成字符串大小
			_str = new char[_capacity + 1]; // +1是为了后面还有个\0，为存储字符串开辟空间
			strcpy(_str, str);
		}
		// 析构函数
		~string()
		{
			delete[] _str; // 释放_str指向的空间
			_str = nullptr; // 置空
			_size = 0; // 字符串大小置0
			_capacity = 0; // 字符串容量置0
		}
		// 移动构造
		string(string&& s)
			:_str(nullptr)
			, _size(0)
			, _capacity(0)
		{
			std::cout << "string(string&& s) -- 移动构造" << std::endl;
			swap(s);
		}
		// 移动赋值
		string& operator=(string&& s)
		{
			std::cout << "string& operator=(string&& s) -- 移动赋值" << std::endl;
			swap(s);
			return *this;
		}
		// 正向迭代器
		typedef char* iterator;
		// begin
		iterator begin()
		{
			return _str; // 返回字符串中首字母元素的地址
		}
		// end
		iterator end()
		{
			return _str + _size; // 返回字符串中最后一个字母的下一个字符的地址
		}
		// 交换两个对象的数据
		void swap(string& s)
		{
			// 调用库里面的swap
			::swap(_str, s._str); // 交换两个对象的字符串
			::swap(_size, s._size); // 交换两个对象的字符串大小
			::swap(_capacity, s._capacity); // 交换两个对象的字符串容量
		}
		// 拷贝构造（现代写法）--  深拷贝
		string(const string& str)
			:_str(nullptr)
			, _size(0)
			, _capacity(0)
		{
			std::cout << "string(const string& str) -- 拷贝构造深拷贝" << std::endl;
			// 先开辟一个tmp空间
			string tmp(str._str);
			swap(tmp); // 将tmp扔到swap中进行交换
		}
		// 赋值运算符操作
		string& operator=(const string& str)
		{
			std::cout << "string& operator=(const string& str) -- 拷贝赋值深拷贝" << std::endl;
			// 开辟一个tmp临时空间
			string tmp(str);
			// 丢到swap将其进行交换
			swap(tmp);
			// 返回左值
			return *this;
		}
		// 运算符[]重载函数
		char& operator[](size_t i)
		{
			assert(i < _size);
			return _str[i]; // 返回下标相对应的字母
		}
		// 改容量，但大小不变
		void reserve(size_t n)
		{
			if (n > _capacity) // 只有当n大于容量的时候才进行修改
			{
				char* tmp = new char[n + 1]; // 开辟个临时空间，后面带上\0
				strncpy(tmp, _str, _size + 1); // 将_str的所有字符拷贝进tmp中
				delete[] _str; // 释放对象原本空间
				_str = tmp; // 将tmp的空间给_str（tmp只是临时空间，它需要给原住民_str开辟）
				_capacity = n; // 容量变成n
			}
		}
		// push_back进行尾插字符
		void push_back(char ch)
		{
			if (_size == _capacity) // 判断是否需要增容
			{
				reserve(_capacity == 0 ? 4 : _capacity * 2); // 初始为4，后面两倍两倍增加
			}
			_str[_size] = ch; // 尾插
			_str[_size++] = '\0'; // 后面一个字符为\0并将其_size++
		}
		// +=运算符重载
		string& operator+=(char ch)
		{
			push_back(ch);
			return *this; // 返回左值
		}
		// 返回c类型的字符串
		const char* c_str() const
		{
			return _str;
		}
	};
}

int main()
{
	list<pair<int, JRH::string>> mylist;
	pair<int, JRH::string> kv(1, "1234");
	mylist.push_back(kv); // 传左值
	std::cout << std::endl;
	mylist.push_back(pair<int, JRH::string>(2, "12")); // 传右值
	std::cout << std::endl;
	mylist.push_back({ 50, "1222" }); // 传初始化列表
	return 0;
}






//int main()
//{
//	list<pair<int, JRH::string>> mylist;
//	pair<int, JRH::string> kv(1, "1234");
//	mylist.emplace_back(kv); // 传左值
//	std::cout << std::endl;
//	mylist.emplace_back(pair<int, JRH::string>(2, "12")); // 传右值
//	std::cout << std::endl;
//	mylist.emplace_back(50, "12345"); // 传参数包
//	return 0;
//}















//int main()
//{
//	list<pair<int, string>> mylist;
//	pair<int, string> kv(10, "222");
//
//	mylist.push_back(kv); // 左值
//	mylist.push_back(pair<int, string>(10, "111")); // 右值
//	mylist.push_back({ 20,"123" }); // 初始化列表
//	mylist.emplace_back(kv); // 左值
//	mylist.emplace_back(pair<int, string>(30, "444")); // 右值
//	mylist.emplace_back(20, "12"); // 传参数表
//	return 0;
//}

//template<class ...Args>
//void showlist(Args... args)
//{
//	// 错误例子
//	for (int i = 0; i < sizeof...(args); i++)
//	{
//		std::cout << args[i] << " "; // 这里不能调用args[i]
//	}
//	std::endl;
//}

//// 展开函数
//template<class T, class ...Args>
//void showlist(T value, Args... args)
//{
//	std::cout << value << " "; // 打印分离出的第一个参数
//	showlist(...args); // 递归调用，将参数包继续向下传
//}
//


//// 无参的递归终止函数
//void showlist()
//{
//	std::cout << std::endl;
//}
//// 展开函数
//template<class T, class ...Args>
//void showlist(T value, Args... args)
//{
//	std::cout << value << " "; // 打印分离出的第一个参数
//	showlist(args...); // 递归调用，将参数包继续向下传
//}

//// 无参的递归终止函数
//void showlistarg()
//{
//	std::cout << std::endl;
//}
//// 供外部函数进行调用
//template<class T, class ...Args>
//void showlistarg(T value, Args... args)
//{
//	std::cout << value << " "; // 打印分离出的第一个参数
//	showlistarg(args...); // 递归调用，将参数包继续向下传
//}
//// 展开函数
//template<class ...Args>
//void showlist(Args... args)
//{
//	showlistarg(args...);
//}

//// 带参的递归终止函数
//template<class T>
//void showlistarg(const T& t)
//{
//	std::cout << t << std::endl;
//}
//// 供外部函数进行调用
//template<class T, class ...Args>
//void showlistarg(T value, Args... args)
//{
//	std::cout << value << " "; // 打印分离出的第一个参数
//	showlistarg(args...); // 递归调用，将参数包继续向下传
//}
//// 展开函数
//template<class ...Args>
//void showlist(Args... args)
//{
//	showlistarg(args...);
//}

//// 错误示范 -- 判断参数包中的参数个数
//template<class T, class ...Args>
//void showlist(T value, Args... args)
//{
//	std::cout << value << " ";
//	if (sizeof...(args) == 0)
//	{
//		return;
//	}
//	showlist(args...);
//}

//// 展开函数
//template<class ...Args>
//void showlist(Args ...args)
//{
//	int arr[] = { args... }; // 将参数包中的整数全部放进arr数组中
//	for (auto& e : arr)
//	{
//		std::cout << e << " ";
//	}
//	std::cout << std::endl;
//}


//// 无参的调用函数
//void showlist()
//{
//	std::cout << std::endl;
//}
//// 处理参数包中的每个参数
//template<class T>
//void PrintArg(const T& t)
//{
//	cout << t << " ";
//}
//// 展开函数
//template<class ...Args>
//void showlist(Args... args)
//{
//	int arr[] = { (PrintArg(args), 0)... };
//	cout << endl;
//}

//// 无参的调用函数
//void showlist()
//{
//	std::cout << std::endl;
//}
//// 处理参数包中的每个参数
//template<class T>
//int PrintArg(const T& t)
//{
//	cout << t << " ";
//	return 0;
//}
//// 展开函数
//template<class ...Args>
//void showlist(Args... args)
//{
//	int arr[] = { PrintArg(args)... };
//	cout << endl;
//}

//int main()
//{
//	showlist(1);
//	showlist(1, 2);
//	showlist(1, 2, 3);
//	return 0;
//}


