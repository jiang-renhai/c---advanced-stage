

#include"AVLTree.h"

int main(int argc, int* argv[])
{
	int a[] = { 16, 3, 7, 11, 9, 26, 18, 14, 15 };

	AVLTree<int, int> AVT;
	for (auto e : a)
	{
		AVT.Insert(make_pair(e, e));
	}

	AVT.IsBalance();
	AVT.InOrder();

	return 0;
}