#pragma once

#include"HashTable.h"

namespace JRH
{
	template<class K>
	class Unordered_Set
	{
		struct KeyOfSet // 仿函数
		{
			const K& operator()(const K& key)
			{
				return key; // 陪跑功能
			}
		};
	public:
		typedef typename HashT::HashTable<K, K, KeyOfSet>::const_iterator iterator;
		typedef typename HashT::HashTable<K, K, KeyOfSet>::const_iterator const_iterator;

		iterator begin()
		{
			return _ht.begin();
		}

		iterator end()
		{
			return _ht.end();
		}

		const_iterator begin() const
		{
			return _ht.begin();
		}

		const_iterator end() const
		{
			return _ht.end();
		}

		// 插入
		pair<iterator, bool> insert(const K& key)
		{
			return _ht.Insert(key);
		}

		// 查找
		iterator find(const K& key)
		{
			return _ht.Find(key);
		}

		// 删除
		void erase(const K& key)
		{
			_ht.Erase(key);
		}

	private:
		HashT::HashTable<K, K, KeyOfSet> _ht; // 传到hash中是key, key模型
	};
}