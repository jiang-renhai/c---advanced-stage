#pragma once

#include<vector>
#include<string>
using namespace std;

namespace HashT
{
	// 将kv.first强转成size_t
	// 仿函数
	template<class K>
	struct DefaultHashTable
	{
		size_t operator()(const K& key)
		{
			return (size_t)key;
		}
	};

	// 模版的特化（其他类型） 
	template<>
	struct DefaultHashTable<string>
	{
		size_t operator()(const string& str)
		{
			// BKDR
			size_t hash = 0;
			for (auto ch : str)
			{
				// 131是一个素数，也可以用其他一些素数代替，主要作用防止两个不同字符串计算得到的哈希值相等
				hash *= 131;
				hash += ch;
			}

			return hash;
		}
	};


	template<class T>
	struct HashNode
	{
		HashNode(const T data)
			:_data(data)
			, _next(nullptr)
		{}

		T _data;
		HashNode<T>* _next;
	};

	// 前置声明
	template<class K, class T, class KeyOfT, class HashFunc>
	class HashTable;

	// 哈希正向迭代器
	template<class K, class T, class Ptr, class Ref, class KeyOfT, class HashFunc>
	struct HTIterator
	{
		typedef HashNode<T> Node; // 哈希结点类型
		typedef HashTable<K, T, KeyOfT, HashFunc> HT; // 哈希表类型
		typedef HTIterator<K, T, Ptr, Ref, KeyOfT, HashFunc> Self; // 迭代器类型
		typedef HTIterator<K, T, T*, T&, KeyOfT, HashFunc> Iterator;  // 迭代器，用来传参的
		
		// 成员变量
		Node* _node;
		const HT* _pht;

		// 构造函数
		HTIterator(Node* node, const HT* pht)
			:_node(node)
			,_pht(pht)
		{}

		// const
		HTIterator(const Iterator& it)
			:_node(it._node)
			, _pht(it._pht)
		{}

		// Ptr
		Ptr operator->()
		{
			return &_node->_data; // 返回地址
		}

		// Ref
		Ref operator*()
		{
			return _node->_data; // 返回引用
		}

		// 判断相等
		bool operator!=(const Self& s) const
		{
			return _node != s._node;
		}

		// 判断不相等
		bool operator==(const Self& s) const
		{
			return _node == s._node;
		}

		Self& operator++()
		{
			//1. 若当前结点不是当前哈希桶中的最后一个结点，则++后走到当前哈希桶的下一个结点
			//2. 若当前结点是当前哈希桶的最后一个结点，则++后走到下一个非空哈希桶的第一个结点

			if (_node->_next) // 桶后面的结点还有
			{
				_node = _node->_next; // ++操作就是这个桶的当前结点的下一个结点
			}

			else // 正好到这个桶的最后一个结点
			{
				HashFunc hf;
				KeyOfT kot;
				size_t hashi = hf(kot(_node->_data)) % _pht->_table.size();
				hashi++;
				while (hashi < _pht->_table.size()) // 表没走完
				{
					if (_pht->_table[hashi]) // 表不为空
					{
						_node = _pht->_table[hashi]; // 刚刚好就是哈希表下一个的头结点
						return *this;
					}
					++hashi; // 为空则++
				}
				_node = nullptr;
			}
			return *this;
		}

	};

	//哈希表
	template<class K, class T, class KeyOfT, class HashFunc = DefaultHashTable<K>>
	class HashTable
	{
		typedef HashNode<T> Node;

		// 友元声明
		template<class K, class T, class Ptr, class Ref, class KeyOfT, class HashFunc>
		friend struct HTIterator;

	public:
		typedef HTIterator<K, T, T*, T&, KeyOfT, HashFunc> iterator;
		typedef HTIterator<K, T, const T*, const T&, KeyOfT, HashFunc> const_iterator;

		// 正向迭代器的begin()
		iterator begin()
		{
			// 找第一个有值的头结点桶
			for (size_t i = 0; i < _table.size(); ++i)
			{
				Node* cur = _table[i];
				if (cur)
				{
					return iterator(cur, this);
				}
			}
			return iterator(nullptr, this);
		}

		// 正向迭代器的end()
		iterator end()
		{
			// 是最后一个结点的下一个位置也就是空位置
			return iterator(nullptr, this);
		}

		// const迭代器的begin()
		const_iterator begin() const
		{
			// 找第一个有值的头结点桶
			for (size_t i = 0; i < _table.size(); ++i)
			{
				Node* cur = _table[i];
				if (cur)
				{
					return const_iterator(cur, this);
				}
			}
			return const_iterator(nullptr, this);
		}

		// const迭代器的end()
		const_iterator end() const
		{
			return const_iterator(nullptr, this);
		}



		size_t GetNextPrime(size_t prime)
		{
			const int PRIMECOUNT = 28;
			//素数序列
			const size_t primeList[PRIMECOUNT] =
			{
				53ul, 97ul, 193ul, 389ul, 769ul,
				1543ul, 3079ul, 6151ul, 12289ul, 24593ul,
				49157ul, 98317ul, 196613ul, 393241ul, 786433ul,
				1572869ul, 3145739ul, 6291469ul, 12582917ul, 25165843ul,
				50331653ul, 100663319ul, 201326611ul, 402653189ul, 805306457ul,
				1610612741ul, 3221225473ul, 4294967291ul
			};

			size_t i = 0;
			for (i = 0; i < PRIMECOUNT; i++)
			{
				if (primeList[i] > prime)
					return primeList[i];
			}
			return primeList[i];
		}

		// 构造函数
		HashTable()
		{
			_table.resize(GetNextPrime(1), nullptr);
		}

		//构造函数
		//HashTable() = default; //显示指定生成默认构造函数

		// 拷贝构造
		HashTable(const HashTable& ht)
		{
			//1. 将哈希表调整到ht._table.size()一样的大小。
			//2. 将ht._table每个桶当中的结点一个个拷贝到自己的哈希表中
			//3. 更改哈希表中有效的数据的量
			_table.resize(ht._table.size());

			for (size_t i = 0; i < ht._table.size(); ++i)
			{
				if (ht._table[i]) // 桶不为空的时候
				{
					Node* cur = ht._table[i];
					while (cur) // 这个桶的往后结点遍历完
					{
						Node* copy = new Node(cur->_data); // 拷贝结点的创建
						// 头插到桶中
						copy->_next = _table[i];
						_table[i] = cur;
						cur = cur->_next;
					}
				}
			}
			_n = ht._n;
		}

		// 运算符重载
		HashTable& operator=(HashTable ht)
		{
			// 通过间接调用拷贝构造进行运算符赋值重载
			_table.swap(ht._table);
			swap(_n, ht._n);
			return *this;
		}

		// 析构函数
		~HashTable()
		{
			for (size_t i = 0; i < _table.size(); ++i)
			{
				if (_table[i]) // 桶结点不为空
				{
					Node* cur = _table[i]; // 定义当前桶的头结点
					while (cur)
					{
						Node* next = cur->_next;
						delete cur;
						cur = next;
					}
					_table[i] = nullptr;
				}
			}
		}

		// 插入
		pair<iterator, bool> Insert(const T& data)
		{
			// 1查看哈希表中是否存在该键值的键值对，若已存在则插入失败
			// 2判断是否需要调整负载因子，若负载因子过大都需要对哈希表的大小进行调整
			// 3将键值对插入哈希表
			// 4哈希表中的有效元素个数加一

			KeyOfT kot;
			HashFunc hf;
			// 1、查看键值对，调用Find函数
			iterator key_ = Find(kot(data));
			if (key_ != end())
			{
				return make_pair(key_, false);
			}

			// 2、判断负载因子，负载因子是1的时候进行增容
			if (_n == _table.size()) // 整个哈希表都已经满了
			{
				// 增容
				// a.创建一个新表，将原本容量扩展到原始表的两倍
				size_t HashiNewSize = GetNextPrime(_table.size());
				vector<Node*> NewHashiTable;
				NewHashiTable.resize(HashiNewSize, nullptr);

				// b.遍历旧表，顺手牵羊，将原始表数据逐个头插到新表中
				for (size_t i = 0; i < _table.size(); ++i)
				{
					if (_table[i]) // 这个桶中的有数据/链表存在
					{
						Node* cur = _table[i]; // 记录头结点
						while (cur)
						{
							Node* next = cur->_next; // 记录下一个结点
							size_t hashi = hf(kot(data)) % _table.size(); // 记录一下新表的位置
							// 头插到新表中
							cur->_next = _table[hashi];
							_table[hashi] = cur;

							cur = next; // 哈希这个桶的下一个结点
						}
						_table[i] = nullptr;
					}
				}
				// c.交换两个表
				_table.swap(NewHashiTable);
			}

			// 3、将键值对插入到哈希表中
			size_t hashii = hf(kot(data)) % _table.size();
			Node* newnode = new Node(data);
			// 头插法
			newnode->_next = _table[hashii];
			_table[hashii] = newnode;

			// 4、将_n++
			++_n;
			return make_pair(iterator(newnode, this), true);
		}

		// 查找
		iterator Find(const K& key)
		{
			//1通过哈希函数计算出对应的哈希地址
			//2通过哈希地址找到对应的哈希桶中的单链表，遍历单链表进行查找即可
			HashFunc hf;
			KeyOfT kot;
			size_t hashi = hf(key) % _table.size();

			Node* cur = _table[hashi]; // 刚好到哈希桶的位置
			while (cur)
			{
				// 找到匹配的了
				if (kot(cur->_data) == key)
				{
					return iterator(cur, this);
				}
				cur = cur->_next;
			}
			return end();
		}

		// 删除
		bool Erase(const K& key)
		{
			//1通过哈希函数计算出对应的哈希桶编号
			//2遍历对应的哈希桶，寻找待删除结点
			//3若找到了待删除结点，则将该结点从单链表中移除并释放
			//4删除结点后，将哈希表中的有效元素个数减一
			KeyOfT kot;
			HashFunc hf;
			size_t hashi = hf(key) % _table.size();
			// prev用来记录前面一个结点，有可能是删除的是哈希桶第一个结点
			// cur记录的是当前结点，当然是要删除的结点
			Node* prev = nullptr;
			Node* cur = _table[hashi];

			while (cur) // 遍历到结尾
			{
				if (kot(cur->_data) == key) // 刚好找到这个值
				{
					// 第一种情况：这个要删除的值刚好是哈希桶的头结点
					if (prev == nullptr)
					{
						_table[hashi] = cur->_next;
					}
					// 第二种情况：这个要删除的值不是哈希桶的头结点，而是下面挂的值
					else // (prev != nullptr)
					{
						prev->_next = cur->_next;
					}
					delete cur; // 删除cur结点
					_n--;
					return true;
				}
				prev = cur;
				cur = cur->_next;
			}
			return false;
		}

	public:
		// 打印一下
		void Print()
		{
			// 大循环套小循环
			for (size_t i = 0; i < _table.size(); ++i)
			{
				printf("[%d]->", i);
				Node* cur = _table[i];
				// 小循环
				while (cur)
				{
					//cout << cur->_data;
					cur = cur->_next;
				}
				printf("NULL\n");
			}
		}

	private:
		vector<Node*> _table; //哈希表
		size_t _n = 0; //哈希表中的有效元素个数
	};
}