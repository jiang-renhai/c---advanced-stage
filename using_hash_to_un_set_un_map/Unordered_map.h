#pragma once


#include"HashTable.h"

namespace JRH1
{
	template<class K, class V>
	class Unordered_Map
	{
		struct KeyOfMap // 仿函数，取key
		{
			const K& operator()(const pair<K, V>& kv)
			{
				return kv.first;  // 取key
			}
		};
	public:
		//现在没有实例化，没办法到HashTable里面找iterator，所以typename就是告诉编译器这里是一个类型，实例化以后再去取
		typedef typename HashT::HashTable<K, pair<K, V>, KeyOfMap>::iterator iterator;
		typedef typename HashT::HashTable<K, pair<K, V>, KeyOfMap>::iterator const_iterator;

		iterator begin()
		{
			return _ht.begin();
		}

		iterator end()
		{
			return _ht.end();
		}

		const_iterator begin() const
		{
			return _ht.begin();
		}

		const_iterator end() const
		{
			return _ht.end();
		}

		// 插入
		pair<iterator, bool> insert(const pair<K, V>& kv)
		{
			return _ht.Insert(kv);
		}

		// []重载
		V& operator[](const K& key)
		{
			pair<iterator, bool> ret = _ht.Insert(make_pair(key, V()));
			return ret.first->second;
		}

		// 查找
		iterator find(const K& key)
		{
			return _ht.Find(key);
		}

		// 删除
		void erase(const K& key)
		{
			_ht.Erase(key);
		}

	private:
		HashT::HashTable<K, pair<K, V>, KeyOfMap> _ht; // 传到hash中是pair<K, V>模型的键值对
	};
}