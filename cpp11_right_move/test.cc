#define CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<list>
#include<cstring>
#include<algorithm>
#include<cassert>
using namespace std;

// list
namespace JRH
{
	template<class T>
	struct ListNode
	{
		T _data; // 数据域
		ListNode* _prev = nullptr; // 前置指针
		ListNode* _next = nullptr; // 后置指针
	};
	template<class T>
	class list
	{
		typedef ListNode<T> node;
	private:
		node* _head; // 头指针
	public:
		// 构造函数
		list()
		{
			// 没有数据域
			_head = new node;
			_head->_prev = _head;
			_head->_next = _head;
		}
		// 析构函数
		~list()
		{
			delete _head;
		}
		// 左值引用的insert版本
		void insert(node* pos, const T& t)
		{
			// prev newnode pos
			node* prev = pos->_prev;
			node* newnode = new node;
			newnode->_data = t;

			newnode->_prev = prev;
			prev->_next = newnode;
			newnode->_next = pos;
			pos->_prev = newnode;
		}
		// 右值引用的insert版本
		void insert(int pos, T&& t)
		{
			// prev newnode pos
			node* prev = pos->_prev;
			node* newnode = new node;
			newnode->_data = std::forward<T>(t); // 完美转发

			newnode->_prev = prev;
			prev->_next = newnode;
			newnode->_next = pos;
			pos->_prev = newnode;
		}
		// 左值引用的push_back版本
		void push_back(const T& t)
		{
			insert(_head, t);
		}
		// 右值引用的push_back版本
		void push_back(T&& t)
		{
			insert(_head, std::forward<T>(t));
		}
	};
}

// string类
namespace JRH
{
	class string
	{
	private:
		char* _str;
		size_t _size;
		size_t _capacity;
	public:
		// 构造函数
		string(const char* str = "")
		{
			_size = strlen(str); // 初始时字符串大小设置成字符字符串长度
			_capacity = _size; // 初始时字符串容量设置成字符串大小
			_str = new char[_capacity + 1]; // +1是为了后面还有个\0，为存储字符串开辟空间
			strcpy(_str, str);
		}
		// 析构函数
		~string()
		{
			delete[] _str; // 释放_str指向的空间
			_str = nullptr; // 置空
			_size = 0; // 字符串大小置0
			_capacity = 0; // 字符串容量置0
		}
		// 移动构造
		string(string&& s)
			:_str(nullptr)
			,_size(0)
			,_capacity(0)
		{
			std::cout << "string(string&& s)" << std::endl;
			swap(s);
		}
		// 移动赋值
		string& operator=(string&& s)
		{
			std::cout << "string& operator=(string&& s) -- 移动赋值" << std::endl;
			swap(s);
			return *this;
		}
		// 正向迭代器
		typedef char* iterator;
		// begin
		iterator begin()
		{
			return _str; // 返回字符串中首字母元素的地址
		}
		// end
		iterator end()
		{
			return _str + _size; // 返回字符串中最后一个字母的下一个字符的地址
		}
		// 交换两个对象的数据
		void swap(string& s)
		{
			// 调用库里面的swap
			::swap(_str, s._str); // 交换两个对象的字符串
			::swap(_size, s._size); // 交换两个对象的字符串大小
			::swap(_capacity, s._capacity); // 交换两个对象的字符串容量
		}
		// 拷贝构造（现代写法）--  深拷贝
		string(const string& str)
			:_str(nullptr)
			,_size(0)
			,_capacity(0)
		{
			std::cout << "string(const string& str) -- 深拷贝" << std::endl;
			// 先开辟一个tmp空间
			string tmp(str);
			swap(tmp); // 将tmp扔到swap中进行交换
		}
		// 赋值运算符操作
		string& operator=(const string& str)
		{
			std::cout << "string& operator=(const string& str) -- 深拷贝" << std::endl;
			// 开辟一个tmp临时空间
			string tmp(str);
			// 丢到swap将其进行交换
			swap(tmp);
			// 返回左值
			return *this;
		}
		// 运算符[]重载函数
		char& operator[](size_t i)
		{
			assert(i < _size);
			return _str[i]; // 返回下标相对应的字母
		}
		// 改容量，但大小不变
		void reserve(size_t n)
		{
			if (n > _capacity) // 只有当n大于容量的时候才进行修改
			{
				char* tmp = new char[n + 1]; // 开辟个临时空间，后面带上\0
				strncpy(tmp, _str, _size + 1); // 将_str的所有字符拷贝进tmp中
				delete[] _str; // 释放对象原本空间
				_str = tmp; // 将tmp的空间给_str（tmp只是临时空间，它需要给原住民_str开辟）
				_capacity = n; // 容量变成n
			}
		}
		// push_back进行尾插字符
		void push_back(char ch)
		{
			if (_size == _capacity) // 判断是否需要增容
			{
				reserve(_capacity == 0 ? 4 : _capacity * 2); // 初始为4，后面两倍两倍增加
			}
			_str[_size] = ch; // 尾插
			_str[_size++] = '\0'; // 后面一个字符为\0并将其_size++
		}
		// +=运算符重载
		string& operator+=(char ch)
		{
			push_back(ch);
			return *this; // 返回左值
		}
		// 返回c类型的字符串
		const char* c_str() const
		{
			return _str;
		}
	};
	JRH::string to_string(int value)
	{
		bool flag = true;
		// 将负数的value转化成正数
		if (value < 0)
		{
			flag = true;
			value = 0 - value;
		}
		JRH::string str; // 返回的string
		while (value > 0) // 将其转换成字符串进入str中
		{
			int x = value % 10;
			value /= 10;
			str += (x + '0');
		}
		// 负数情况
		if (flag == false)
		{
			str += '-';
		}
		// 进行翻转
		std::reverse(str.begin(), str.end());
		return str;
	}
}

int main()
{
	JRH::list<JRH::string> lt;
	JRH::string s("1234");
	lt.push_back(s); // 左值引用版本
	lt.push_back("1111"); // 右值引用版本
	return 0;
}



//void Func(int& t)
//{
//	std::cout << "void Func(int& t) -- 左值引用" << std::endl;
//}
//
//void Func(const int& t)
//{
//	std::cout << "void Func(const int& t) -- const左值引用" << std::endl;
//}
//
//void Func(int&& t)
//{
//	std::cout << "void Func(int&& t) -- 右值引用" << std::endl;
//}
//
//void Func(const int&& t)
//{
//	std::cout << "void Func(const int&& t) -- const右值引用" << std::endl;
//}
//
//template<class T>
//void perfectforward(T&& t)
//{
//	Func(t);
//}
//int main()
//{
//	int x = 10;
//	perfectforward(x); // 左值引用
//	perfectforward(move(x)); // 右值引用
//
//	const int y = 20;
//	perfectforward(y); // const左值引用
//	perfectforward(move(y)); // const右值引用
//	return 0;
//}


//template<class T>
//void perfectforward(T&& t)
//{
//	// ...
//}
//
//int main()
//{
//	list<JRH::string> lt;
//	JRH::string s("1234");
//	lt.push_back(s); // 调用string的拷贝构造
//	lt.push_back("1111"); // 调用string的移动构造
//	lt.push_back(std::move(s)); // 调用string的移动构造
//	lt.push_back(JRH::string("2222")); // 调用string的移动构造
//	return 0;
//}

//int main()
//{
//	JRH::string s = JRH::to_string(111);
//	JRH::string ss = JRH::to_string(-222);
//	return 0;
//}


//void func1(JRH::string s)
//{}
//void func2(const JRH::string& s)
//{}
//
//int main()
//{
//	JRH::string s("hello world");
//	func1(s); // 值传参
//	func2(s); // 左值引用传参
//	s += 's';
//	return 0;
//}

//int main()
//{
//	int a = 10; // a为左值
//	//const int b = 20;
//	//int&& x = a; // 右值不能引用左值
//	//int&& y = b; // 右值不能引用const左值
//	//const int&& z = b; // const右值不能引用const左值
//	//const int&& q = a; // const右值不能引用左值
//	int&& p = move(a); // 右值引用可以引用move以后的左值
//	return 0;
//}


//template<class T>
//void func(const T& val) // const左值引用
//{
//	cout << val << endl;
//}
//int main()
//{
//	string s("hello world\n");
//	func(s); // s为左值，其为左值上去
//	func("hello "); // hello 为右值，其为右值上去
//	return 0;
//}

//int main()
//{
//	// 已经是x和y赋值的常量了
//	double x = 1.1, y = 2.2;
//	int&& rr1 = 10;
//	const double&& rr2 = x + y;
//
//	rr1 = 12;
//	rr2 = 10.2; // error
//	return 0;
//}


//int main()
//{
//	// 已经是x和y赋值的常量了
//	double x = 1.1, y = 2.2;
//
//	//以下几个都是常见的右值引用
//	int&& rx = 10;
//	double&& xy = x + y;
//	double&& fxy = fmin(x, y);
//
//	return 0;
//}

//int main()
//{
//	//以下的p、b、c、*p都是左值
//	int* p = new int(1);
//	int b = 1;
//	const int c = 2;
//
//	// 以下左值引用
//	int*& rp = p;
//	int& bb = b;
//	const int& cc = c;
//	int& rrp = *p;
//	return 0;
//}


//namespace cl
//{
//	//模拟实现string类
//	class string
//	{
//	public:
//		//[]运算符重载（可读可写）
//		char& operator[](size_t i)
//		{
//			return _str[i]; //返回对应字符
//		}
//		//...
//	private:
//		char* _str;       //存储字符串
//		size_t _size;     //记录字符串当前的有效长度
//		//...
//	};
//}
//int main()
//{
//	cl::string s("hello");
//	s[3] = 'x';    //引用返回，支持外部修改
//	return 0;
//}



//int main()
//{
//	// 已经是x和y赋值的常量了
//	double x = 1.1, y = 2.2;
//
//	//以下几个都是常见的右值
//	10;
//	x + y;
//	fmin(x, y);
//
//	//错误示例（右值不能出现在赋值符号的左边）
//	//10 = 1;
//	//x + y = 1;
//	//fmin(x, y) = 1;
//	return 0;
//}



//int main()
//{
//	//以下的p、b、c、*p都是左值
//	int* p = new int(1);
//	int b = 1;
//	const int c = 2;
//	return 0;
//}



