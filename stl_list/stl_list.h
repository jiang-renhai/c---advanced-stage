#pragma once
#include<iostream>
#include<cassert>

namespace JRH
{
	// list当中的结点
	template<class T>
	struct _list_node
	{
		// 成员变量
		T _val;    // 数据域
		_list_node<T>* _prev;  // 前驱指针
		_list_node<T>* _next;  // 后继指针
		// 成员函数
		_list_node(const T& val = T()) // 构造函数
			:_val(val)
			,_prev(nullptr)
			,_next(nullptr)
		{}
	};

	// list迭代器
	template<class T, class Ref, class Ptr>
	struct _list_iterator
	{
		typedef _list_node<T> node;
		typedef _list_iterator<T, Ref, Ptr> self;
		// 成员变量
		node* _pnode; // 指向结点的指针
		// 成员函数
		_list_iterator(node* pnode) // 构造函数
			:_pnode(pnode)
		{}
		// 运算符重载的各类函数
		self operator++() // 前置
		{
			_pnode = _pnode->_next; // 先让结点指针指向后一个节点
			return *this; // 返回自增后的结点指针
		}
		self operator--()
		{
			_pnode = _pnode->_prev; // 先让结点指针指向前一个节点
			return *this; // 返回自减后的结点指针
		}
		self operator++(int) // 后置
		{
			self tmp(*this); // 记录当前指针的指向
			_pnode = _pnode->_next; // 让结点指针指向后一个节点
			return tmp; // 返回自增前的结点指针
		}
		self operator--(int)
		{
			self tmp(*this); // 记录当前指针指向
			_pnode = _pnode->_prev; // 让结点指针指向前一个节点
			return tmp; // 返回自减前的结点指针
		}
		bool operator==(const self& s) const
		{
			return _pnode == s._pnode; // 判断两个结点指针指向是否相同
		}
		bool operator!=(const self& s) const
		{
			return _pnode != s._pnode; // 判断两个结点指针指向是否不同
		}
		Ref operator*()
		{
			return _pnode->_val; // 返回结点指针所指结点的数据
		}
		Ptr operator->()
		{
			return &_pnode->_val; // 返回结点指针所指结点的数据的地址
		}
	};

	// list
	template<class T>
	class list
	{
	public:
		// 重命名
		typedef _list_node<T> node; // 结点
		typedef _list_iterator<T, T&, T*> iterator; // 迭代器
		typedef _list_iterator<T, const T&, const T*> const_iterator; // const迭代器
	public:
		// 成员函数
		// 1、默认成员函数
		list() // 构造
		{
			_phead = new node; // 申请一个头结点
			_phead->_next = _phead; // 后继指向自己
			_phead->_prev = _phead; // 前驱指向自己
		}
		list(const list<T>& ltnode) // 拷贝构造lt2(lt1)
		{
			_phead = new node; // 创建一个新的头结点
			_phead->_next = _phead; // head后继结点指向本身
			_phead->_prev = _phead; // head前驱结点指向本身
			for (const auto& e : ltnode)
			{
				push_back(e); // 将容器ltnode中的值一个个push_back到_head结点后
			}
		}
		// 1、老式写法
		list<T>& operator=(const list<T>& ltnode) // 拷贝赋值
		{
			if (this != &ltnode) // 防止自己给自己拷贝
			{
				clear(); // 先清空容器
				for (auto& e : ltnode)
				{
					push_back(e); // 将ltnode中数据全部尾插到清空的容器中
				}
				return *this; // 支持连续赋值
			}
		}
		//// 2、现代写法
		//list<T>& operator=(const list<T>& ltnode)
		//{
		//	swap(ltnode); // 交换这两个对象
		//	return *this; // 支持连续赋值
		//}
		~list() // 析构
		{
			clear(); // 清空容器
			delete _phead; // 删除申请的节点
			_phead = nullptr; // 置空
		}

		// 2、迭代器相对应函数
		iterator begin() // 开始
		{
			return iterator(_phead->_next); // 头结点的下一个结点
		}
		iterator end() // 结尾
		{
			return iterator(_phead); // 头结点
		}
		const_iterator begin() const // const开始
		{
			return const_iterator(_phead->_next); // 头结点的下一个结点
		}
		const_iterator end() const // const结尾
		{
			return const_iterator(_phead); // 头结点
		}

		// 3、访问容器
		T& front() // 访问头
		{
			return *begin(); // 返回第一个有效数据的引用
		}
		T& back() // 访问尾
		{
			return *(--end()); // 返回头结点的引用
		}
		const T& front() const // const访问头
		{
			return *begin(); // 返回第一个有效数据的const引用
		}
		const T& back() const // const访问尾
		{
			return *(--end()); // 返回头结点的const引用
		}

		// 4、插入、删除函数
		void insert(iterator pos, const T& x) // 插入
		{
			assert(pos._pnode); // 确保合法性
			node* cur = pos._pnode; // cur结点为当前所处的结点位置
			node* prev = cur->_prev; // prev结点为当前结点的前一个结点
			node* newnode = new node(x);
			// 链接
			newnode->_next = cur;
			cur->_prev = newnode;

			prev->_next = newnode;
			newnode->_prev = prev;
		}
		iterator erase(iterator pos) // 删除
		{
			assert(pos._pnode); // 检测合法性
			assert(pos != end()); // 不能删除头结点
			node* cur = pos._pnode; // cur结点为当前结点
			node* prev = cur->_prev; // prev结点为前一个结点
			node* next = cur->_next; // next结点为后一个结点

			delete cur; // 删除当前节点

			// 建立关系
			prev->_next = next;
			next->_prev = prev;
			return iterator(next); // 返回所给迭代器pos的下一个迭代器
		}
		void push_back(const T& x) // 尾插
		{
			insert(end(), x); // 头结点前插入结点
		}
		void pop_back() // 尾删
		{
			erase(--end()); // 删除头结点的前一个节点
		}
		void push_front(const T& x)  // 头插
		{
			insert(begin(), x); // 在第一个有效结点前插入结点
		}
		void pop_front() // 头删
		{
			erase(begin()); // 删除第一个有效节点
		}

		// 5、其他函数
		size_t size() const // 容量大小
		{
			int sz = 0; // 统计的有效容量大小个数
			const_iterator it = begin(); // 开头元素
			while (it != end()) // 通过遍历累加
			{
				++sz;
				++it;
			}
			return sz; // 返回有效大小的个数
		}
		void resize(size_t n, const T& val = T()) // 扩容
		{
			iterator it = begin(); // 获取第一个有效数据的迭代器
			size_t len = 0; // 记录当前所遍历的数据个数
			while (len < n && it != end())
			{
				len++;
				it++;
			}
			if (len == n) // 说明容器当中的有效数据个数大于或是等于n
			{
				while (it != end()) // 只保留前n个数据
				{
					it = erase(it); // 每次删除后接收下一个数据的迭代器
				}
			}
			else // 说明容器当中的有效数据个数小于n
			{
				while (len < n) // 尾插数据为val的结点，直到容器当中的有效数据个数为n
				{
					push_back(val);
					len++;
				}
			}
		}
		void clear() // 清空
		{
			iterator it = begin();
			while (it != end()) // 逐个删除
			{
				it = erase(it);
			}
		}
		bool empty() const  // 判空 
		{
			return end() == begin();
		}
		void swap(list<T>& lt) // 交换
		{
			std::swap(_phead, lt._phead);
		}
	private:
		// 成员变量
		node* _phead; // 指向头结点的指针
	};

	void Print(const list<int>& lt)
	{
		list<int>::const_iterator it = lt.begin();
		while (it != lt.end())
		{
			std::cout << *it << " ";
			++it;
		}
		std::cout << std::endl;
	}
}


