
#include<iostream>
#include<set>

using namespace std;

//template<class T1, class T2>
//struct pair
//{
//	typedef T1 first_type;
//	typedef T2 second_type;
//	T1 first;
//	T2 second;
//	pair()
//		:first(T1())
//		,second(T2())
//	{}
//	pair(const T1& a, const T2& b)
//		:first(a)
//		,second(b)
//	{}
//};

void testset1()
{
	set<int> set1;
	int num[10] = { 1,2,4,3,5,7,6,8,9,10 };

	// 区间构造
	set<int> set2(num, num + sizeof(num) / sizeof(num[0]));

	// 拷贝构造
	set<int> set3(set2);// 拷贝构造代价太大

	// for循环打印set2
	for (auto& e : set2)
	{
		cout << e << " ";
	}
	cout << endl;

	// for循环打印set3
	for (auto& e : set3)
	{
		cout << e << " ";
	}
	cout << endl;
}

void testset2()
{
	set<int> set1 = { 1,2,1,4,5,3,7,6,5,8,9 };
	set<int>::iterator it = set1.begin();
	// 去重+排序
	while (it != set1.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;

	// 范围for
	for (auto& e : set1)
	{
		cout << e << " ";
	}
	cout << endl;
}

void testset3()
{
	int a[10] = { 1,3,1,2,4,5,7,6,5,8 };
	// 这个greater仿函数是set库函数中的函数
	// 就是进行降序的函数
	set<int, greater<int>> set1(a, a + sizeof(a) / sizeof(a[0]));

	// for循环
	for (auto& e : set1)
	{
		cout << e << " ";
	}
	cout << endl;
}

void testset4()
{
	std::set<int> myset;
	std::set<int>::iterator it;
	std::pair<std::set<int>::iterator, bool> ret;

	// for循环并单个插入
	for (int i = 1; i <= 5; ++i)
		myset.insert(i * 10);

	// 单个插入
	ret = myset.insert(20);

	if (ret.second == false) 
		it = ret.first;

	// 插入迭代器所代表的值
	myset.insert(it, 25);
	myset.insert(it, 24);
	myset.insert(it, 26);

	// 插入一段区间
	int myints[] = { 5,10,15 };
	myset.insert(myints, myints + 3);

	std::cout << "myset contains:";
	for (it = myset.begin(); it != myset.end(); ++it)
		std::cout << ' ' << *it;
	std::cout << '\n';
}

void testset5()
{
	std::set<int> myset;
	std::set<int>::iterator it;

	for (int i = 1; i < 10; i++) 
		myset.insert(i * 10); // 10 20 30 40 50 60 70 80 90

	it = myset.begin();
	++it; // 第二个位置

	// 删除第二个位置
	myset.erase(it);

	// 删除一个数
	myset.erase(40);

	// 删除一个迭代器区间
	it = myset.find(60);
	myset.erase(it, myset.end());

	std::cout << "myset contains:";
	for (it = myset.begin(); it != myset.end(); ++it)
		std::cout << ' ' << *it;
	std::cout << '\n';
}

void testset6()
{
	std::set<int> myset;
	std::set<int>::iterator it;

	for (int i = 1; i <= 5; i++) 
		myset.insert(i * 10); // 10 20 30 40 50

	it = myset.find(20);
	myset.erase(it); // 删除20这个迭代器的值
	myset.erase(myset.find(40)); // 删除40这个迭代器的值

	std::cout << "myset contains:";
	for (it = myset.begin(); it != myset.end(); ++it)
		std::cout << ' ' << *it;
	std::cout << '\n';
}

void testset7()
{
	std::set<int> myset;
	std::set<int>::iterator it;

	for (int i = 1; i < 10; i++)
		myset.insert(i * 10); // 10 20 30 40 50 60 70 80 90

	it = myset.begin();
	++it; // 第二个位置

	// 删除第二个位置
	myset.erase(it);

	// 删除一个数
	myset.erase(40);

	// 删除一个迭代器区间
	it = myset.find(60);
	myset.erase(it, myset.end());

	// 删除一个不存在的迭代器
	set<int>::iterator pos = myset.find(70);
	if (pos != myset.end())
	{
		myset.erase(pos);
	}

	for (auto& e : myset)
	{
		cout << e << " ";
	}
	cout << endl;
}

void testset8()
{
	std::set<int> myset;

	for (int i = 1; i < 10; i++)
		myset.insert(i * 10); // 10 20 30 40 50 60 70 80 90
	cout << myset.count(10) << endl;
	cout << myset.count(0) << endl;

}

void testset9()
{
	set<int> myset;
	set<int>::iterator itup, itlow;
}


int main()
{
	testset9();
	return 0;
}