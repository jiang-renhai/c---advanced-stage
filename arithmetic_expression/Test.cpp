#include <iostream>
#include <stack>
#include <string>
#include<windows.h>
using namespace std;

// 加减乘除的运算符优先级选择
int priority(const char ch)
{
    // 我们加入+和-的优先级为1
    // *和/的优先级为2
    // 其余不管为0优先级最低
    switch (ch)
    {
    case '+':
    case '-':
        return 1;
    case '*':
    case '/':
        return 2;
    default:
        return 0;
    }
}

// 加减乘除运算
int calculate(const int a, const int b, const char ch)
{
    switch (ch)
    {
    case '+':
        return a + b;
    case '-':
        return a - b;
    case '*':
        return a * b;
    case '/':
        return a / b;
    default:
        return 0;
    }
}

// 判断是否是加减乘除
bool isoperator(const char ch)
{
    return ch == '+' || ch == '-' || ch == '*' || ch == '/';
}

// 用string接收一下传过来的字符串
int evaluate(string& exp)
{
    // 数字栈
    stack<int> numstack;
    // 符号栈
    stack<char> symbolstack;

    for (int i = 0; i < exp.length(); ++i)
    {
        // 先取第0个位置的string类的值
        char currentCh = exp[i];

        // 检测是否为'1'等的阿拉伯数字
        // 不是这类数字就往else if走
        if (isdigit(currentCh))
        {
            // 转化成为数字
            int num = exp[i] - '0';

            // 没超过表达式并且后面的同样是阿拉伯数字
            while (i + 1 < exp.length() && isdigit(exp[i + 1]))
            {
                num *= 10;
                num += (exp[++i] - '0');
            }

            numstack.push(num);
        }
        // 遇到左括号就入栈
        else if (currentCh == '(')
        {
            symbolstack.push('(');
        }
        // 遇到右括号就出栈
        else if (currentCh == ')')
        {
            // 先取一下栈顶元素
            char cl = symbolstack.top();
            // 出栈
            symbolstack.pop();

            while (!symbolstack.empty() && cl != '(')
            {
                // 数栈顶元素拿出来
                int a = numstack.top();
                // 出栈
                numstack.pop();
                // 需要两个栈顶元素
                int b = numstack.top();
                // pop出
                numstack.pop();

                // 拿进去计算
                numstack.push(calculate(b, a, cl));

                cl = symbolstack.top();
                symbolstack.pop();
            }
        }
        // 如果是加减乘除运算符
        else if (isoperator(currentCh))
        {
            // 符号栈为空或者是当前的符号的优先级大于栈顶的符号的优先级
            if (symbolstack.empty() || priority(currentCh) > priority(symbolstack.top()))
            {
                // 符号栈push这个加减乘除运算符
                symbolstack.push(currentCh);
            }
            else
            {
                // 符号栈的栈顶元素优先级高
                // 取栈顶元素
                char cal = symbolstack.top();
                symbolstack.pop();
                symbolstack.push(currentCh);

                // 再拿出两个数字进行运算
                int a = numstack.top();
                numstack.pop();
                int b = numstack.top();
                numstack.pop();
                numstack.push(calculate(b, a, cal));
            }
        }
    }

    // 最后数字栈中还有数字
    while (numstack.size() > 1)
    {
        char cal = symbolstack.top();
        symbolstack.pop();

        // 再从数字栈中取俩数据，放到计算器内运算
        int a = numstack.top();
        numstack.pop();
        int b = numstack.top();
        numstack.pop();

        // 总数再放入到数字栈中
        numstack.push(calculate(b, a, cal));
    }

    return numstack.top();
}


int main()
{
    while (1)
    {
        cout << "\t\t\t这是一个计算器\t\t\t" << endl;
        int flag = 0;

        cout << "\t\t请选择按钮操作（1、计算 2、清屏并退出）$";
        cin >> flag;

        if (flag == 1)
        {
            cout << "\t\t请输入要计算的表达式（注意，中间不能有空格出现）$";
            string s1;
            cin >> s1;
            cout << "\t\t输出结果为$" << evaluate(s1) << endl;
            Sleep(4000);
            system("cls");
        }
        else if (flag == 2)
        {
            //system("cls");
            break;
        }
        else
        {
            cout << "输入错误，请重新输入$" << endl;
            Sleep(1000);
            system("cls");
            //continue;
        }

    }
    return 0;
}
