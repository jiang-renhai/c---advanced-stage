#include<iostream>
#include<mutex>
using namespace std;

class Singleton
{
private:
	// 1、将构造函数私有化，防拷贝
	Singleton()
	{}
	Singleton(const Singleton&) = delete;
	Singleton& operator=(const Singleton&) = delete;
public:
	//2、提供一个全局访问点获取单例对象
	static Singleton* GetInstance()
	{
		static Singleton inst;
		return &inst;
	}
};

//class Singleton
//{
//public:
//	// 3、提供一个全局访问点获取单例对象
//	static Singleton* Getinstance()
//	{
//		//双检查
//		if (_inst == nullptr)
//		{
//			_mtx.lock();
//			if (_inst == nullptr)
//			{
//				_inst = new Singleton;
//			}
//			_mtx.unlock();
//		}
//		return _inst;
//	}
//private:
//	// 1、将构造函数设置为私有防拷贝
//	Singleton()
//	{}
//	Singleton(const Singleton&) = delete;
//	Singleton& operator=(const Singleton&) = delete;
//	// 2、提供一个指向单例对象的static指针
//	static Singleton* _inst;
//	static mutex _mtx; //互斥锁
//};
//
////在程序入口之前先将static指针初始化为空
//Singleton* Singleton::_inst = nullptr;
//mutex Singleton::_mtx; //初始化互斥锁


//class Singleton
//{
//public:
//	// 3、提供一个全局访问点获取单例对象
//	static Singleton* GetInstance()
//	{
//		return _inirt;
//	}
//private:
//	// 1、将构造函数设为私有，防止拷贝
//	Singleton()
//	{}
//	Singleton(const Singleton&) = delete;
//	Singleton operator=(const Singleton&) = delete;
//	// 2、提供一个指向单例对象的static指针
//	static Singleton* _inirt;
//};
//
//// 4、在程序入口之前完成单例对象的初始化
//Singleton* Singleton::_inirt = new Singleton;




//class NonInherit final
//{
//	// ...
//};

//class NonInherit
//{
//public:
//	static NonInherit CreateOBJ()
//	{
//		return NonInherit();
//	}
//private:
//	// 构造函数私有化
//	NonInherit()
//	{}
//};

//class CopyBan
//{
//public:
//	CopyBan()
//	{}
//private:
//	// C++98
//	CopyBan(const CopyBan&);
//	CopyBan& operator=(const CopyBan&);
//	// C++11
//	//CopyBan(const CopyBan&) = delete;
//	//CopyBan& operator=(const CopyBan&) = delete;
//};

//class StackOnly
//{
//public:
//	StackOnly()
//	{}
//private:
//	// C++98
//	void* operator new(size_t size);
//	void operator delete(void* size);
//	// C++11
//	//void* operator new(size_t size) = delete;
//	//void operator delete(void* size) = delete;
//};

//class StackOnly
//{
//public:
//	// 2、提供一个获取对象的接口，并且该接口必须设置为静态成员函数
//	static StackOnly CreateOBJ()
//	{
//		return StackOnly();
//	}
//private:
//	// 1、将构造函数私有化
//	StackOnly()
//	{}
//};



//class HeapOnly
//{
//public:
//	// 2、提供一个获取对象的接口，并且该接口必须设置为静态成员函数
//	static HeapOnly* CreateOBJ()
//	{
//		return new HeapOnly;
//	}
//private:
//	// 1、将构造函数私有化
//	HeapOnly()
//	{}
//	// 3、将拷贝构造私有化并且只声明而不定义
//	// C++98
//	HeapOnly(const HeapOnly&);
//	// C++11
//	//HeapOnly(const HeapOnly&) = delete;
//};
//
//int main()
//{
//	
//	return 0;
//}
