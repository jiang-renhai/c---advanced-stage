

#include<iostream>
using namespace std;

//class Person {
//public:
//	virtual void BuyTicket() const { cout << "买票-全价" << endl; }
//};
//class Student : public Person {
//public:
//	void BuyTicket() const { cout << "买票-半价" << endl; }
//};

//class A {};
//class B : public A {};
//
//class Person {
//public:
//	virtual A* BuyTicket() const 
//	{ 
//		cout << "买票-全价" << endl; 
//		return 0;
//	}
//};
//class Student : public Person {
//public:
//	virtual B* BuyTicket() const 
//	{ 
//		cout << "买票-半价" << endl;
//		return 0;
//	}
//};
//
//// 多态条件
//// 1. 必须通过基类的指针或者引用调用虚函数
//// 2. 被调用的函数必须是虚函数，且派生类必须对基类的虚函数进行重写
//
//// 多态，不同对象传递过去，调用不同的函数
//// 多态调用看的指向的对象
//// 普通对象 看当前类型
//void func(const Person& p)
//{
//	p.BuyTicket();
//}
//
//int main()
//{
//	Person ps;
//	Student st;
//
//	func(ps);
//	func(st);
//
//	return 0;
//}


//class Person {
//public:
//	 virtual ~Person() { cout << "~Person()" << endl; }
//};
//class Student : public Person {
//public:
//	virtual ~Student() 
//	{ 
//		cout << "~Student()" << endl;
//		delete[] ptr;
//	}
//protected:
//	int* ptr = new int[10];
//};
//
//int main()
//{
//	Person* p = new Person;
//	delete p;
//
//	p = new Student;
//	delete p;  // {满足指针}p->desturctor() + operator delete(p)
//
//	return 0;
//}


//class Car
//{
//public:
//	virtual void Drive() final {}
//};
//class Benz :public Car
//{
//public:
//	virtual void Drive() { cout << "Benz-舒适" << endl; }
//};

//
//class Car {
//public:
//	virtual void Drive() {}
//};
//class Benz :public Car {
//public:
//	virtual void Drive() override { cout << "Benz-舒适" << endl; }
//};
//
//int main()
//{
//
//	return 0;
//}


//class A
//{
//public:
//	A Aobj()
//	{
//		return A();
//	}
//private:
//	A()
//	{}
//};
//
//class B : public A
//{};
//
//int main()
//{
//
//	return 0;
//}


//class A final
//{
//public:
//};
//
//class B : public A 
//{};


//class A
//{
//public:
//	virtual void func(int val = 1) { std::cout << "A->" << val << std::endl; }
//	virtual void test() { func(); }
//};
//
//class B : public A
//{
//public:
//	void func(int val = 0) { std::cout << "B->" << val << std::endl; }
//};
//
//int main(int argc, char* argv[])
//{
//	B* p = new B;
//	p->test();
//	return 0;
//}


//class Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Func1()" << endl;
//	}
//private:
//	int _b = 1;
//};
//int main()
//{
//	Base b;
//	cout << sizeof(Base) << endl;
//	return 0;
//}



//class Person
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Base::Func1()" << endl;
//	}
//	virtual void Func2()
//	{
//		cout << "Base::Func2()" << endl;
//	}
//	void Func3()
//	{
//		cout << "Base::Func3()" << endl;
//	}
//private:
//	int _b = 1;
//};
//class Student : public Person
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Derive::Func1()" << endl;
//	}
//
//private:
//	int _d = 2;
//};
//int main()
//{
//	Person b;
//	Student d;
//	return 0;
//}


class Person {
public:
	virtual void BuyTicket() { cout << "买票-全价" << endl; }
	virtual void Func1() {}
	virtual void Func2() {}
protected:
	int _a = 0;
};
class Student : public Person {
public:
	virtual void BuyTicket() { cout << "买票-半价" << endl; }
protected:
	int _b = 1;
};
void Func(Person& p)
{
	p.BuyTicket();
}

int main()
{
	Person ps;
	Student st;

	ps = st;
	Person* ptr = &st;
	Person& ref = st;

	return 0;
}
