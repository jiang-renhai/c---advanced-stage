#pragma once

#include<vector>
using namespace std;

namespace HashT
{
	// 将kv.first强转成size_t
	// 仿函数
	template<class K>
	struct DefaultHashTable
	{
		size_t operator()(const K& key)
		{
			return (size_t)key;
		}
	};

	// 模版的特化（其他类型） 
	template<>
	struct DefaultHashTable<string>
	{
		size_t operator()(const string& str)
		{
			// BKDR
			size_t hash = 0;
			for (auto ch : str)
			{
				// 131是一个素数，也可以用其他一些素数代替，主要作用防止两个不同字符串计算得到的哈希值相等
				hash *= 131;
				hash += ch;
			}

			return hash;
		}
	};


	template<class K, class V>
	struct HashNode
	{
		HashNode(const pair<K, V>& kv)
			:_kv(kv)
			, _next(nullptr)
		{}

		pair<K, V> _kv;
		HashNode<K, V>* _next;
	};

	//哈希表
	template<class K, class V, class HashFunc = DefaultHashTable<K>>
	class HashTable
	{
		typedef HashNode<K, V> Node;
	public:
		HashTable()
		{
			_table.resize(10, nullptr);
		}

		// 插入
		bool Insert(const pair<K, V>& kv)
		{
			// 1查看哈希表中是否存在该键值的键值对，若已存在则插入失败
			// 2判断是否需要调整负载因子，若负载因子过大都需要对哈希表的大小进行调整
			// 3将键值对插入哈希表
			// 4哈希表中的有效元素个数加一


			HashFunc hf;
			// 1、查看键值对，调用Find函数
			Node* key_ = Find(kv.first);
			if (key_) // 如果key_是存在的则插入不了
			{
				return false; // 插入不了
			}

			// 2、判断负载因子，负载因子是1的时候进行增容
			if (_n == _table.size()) // 整个哈希表都已经满了
			{
				// 增容
				// a.创建一个新表，将原本容量扩展到原始表的两倍
				int HashiNewSize = _table.size() * 2;
				vector<Node*> NewHashiTable;
				NewHashiTable.resize(HashiNewSize, nullptr);

				// b.遍历旧表，顺手牵羊，将原始表数据逐个头插到新表中
				for (size_t i = 0; i < _table.size(); ++i)
				{
					if (_table[i]) // 这个桶中的有数据/链表存在
					{
						Node* cur = _table[i]; // 记录头结点
						while (cur)
						{
							Node* next = cur->_next; // 记录下一个结点
							size_t hashi = hf(kv.first) % _table.size(); // 记录一下新表的位置
							// 头插到新表中
							cur->_next = _table[hashi];
							_table[hashi] = cur;

							cur = next; // 哈希这个桶的下一个结点
						}
						_table[i] = nullptr;
					}
				}
				// c.交换两个表
				_table.swap(NewHashiTable);
			}

			// 3、将键值对插入到哈希表中
			size_t hashii = hf(kv.first) % _table.size();
			Node* newnode = new Node(kv);
			// 头插法
			newnode->_next = _table[hashii];
			_table[hashii] = newnode;

			// 4、将_n++
			++_n;
			return true;
		}

		// 查找
		HashNode<K, V>* Find(const K& key)
		{
			//1通过哈希函数计算出对应的哈希地址
			//2通过哈希地址找到对应的哈希桶中的单链表，遍历单链表进行查找即可
			HashFunc hf;
			size_t hashi = hf(key) % _table.size();

			Node* cur = _table[hashi]; // 刚好到哈希桶的位置
			while (cur)
			{
				// 找到匹配的了
				if (cur->_kv.first == key)
				{
					return (HashNode<K, V>*)cur;
				}
				cur = cur->_next;
			}
			return nullptr;
		}

		// 删除
		bool Erase(const K& key)
		{
			//1通过哈希函数计算出对应的哈希桶编号
			//2遍历对应的哈希桶，寻找待删除结点
			//3若找到了待删除结点，则将该结点从单链表中移除并释放
			//4删除结点后，将哈希表中的有效元素个数减一
			HashFunc hf;
			size_t hashi = hf(key) % _table.size();
			// prev用来记录前面一个结点，有可能是删除的是哈希桶第一个结点
			// cur记录的是当前结点，当然是要删除的结点
			Node* prev = nullptr;
			Node* cur = _table[hashi];

			while (cur) // 遍历到结尾
			{
				if (cur->_kv.first == key) // 刚好找到这个值
				{
					// 第一种情况：这个要删除的值刚好是哈希桶的头结点
					if (prev == nullptr)
					{
						_table[hashi] = cur->_next;
					}
					// 第二种情况：这个要删除的值不是哈希桶的头结点，而是下面挂的值
					else // (prev != nullptr)
					{
						prev->_next = cur->_next;
					}
					delete cur; // 删除cur结点
					_n--;
					return true;
				}
				prev = cur;
				cur = cur->_next;
			}
			return false;
		}

	public:
		// 打印一下
		void Print()
		{
			// 大循环套小循环
			for (size_t i = 0; i < _table.size(); ++i)
			{
				printf("[%d]->", i);
				Node* cur = _table[i];
				// 小循环
				while (cur)
				{
					cout << cur->_kv.first << ":" << cur->_kv.second << "->";
					cur = cur->_next;
				}
				printf("NULL\n");
			}
		}

	private:
		vector<Node*> _table; //哈希表
		size_t _n = 0; //哈希表中的有效元素个数
	};
}