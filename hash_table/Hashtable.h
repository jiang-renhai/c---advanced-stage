#pragma once

#include<vector>
#include<iostream>
using namespace std;

// 标记状态
enum State
{
	EMPTY, // 空
	EXITS, // 存在
	DELETE // 删除
};

// HashData数据
template<class K, class V>
struct HashData
{
public:
	pair<K, V> _kv;
	State _state = EMPTY;
};

// 将kv.first强转成size_t
// 仿函数
template<class K>
struct DefaultHashTable
{
	size_t operator()(const K& key)
	{
		return (size_t)key;
	}
};

namespace open_hash
{
	// HashTable
	template<class K, class V, class HashFunc = DefaultHashTable<K>>
	class HashTable
	{
	public:

		HashTable()
		{
			// 构造10个空间
			_table.resize(10);
		}

		bool Insert(const pair<K, V>& kv)
		{
			// 查看哈希表中是否存在该键值的键值对，若已存在则插入失败
			// 负载因子过大都需要对哈希表的大小进行调整
			// 将键值对插入哈希表
			// 哈希表中的有效元素个数加1

			// 1、判断哈希表中是否存在该键值的键值对
			HashData<K, V>* key_ = Find(kv.first);
			// 利用Find函数的bool值来判断是否存在
			if (key_)
			{
				return false; // 键值已经存在了，返回false
			}

			// 2、负载因子过大需要调整 -- 即大于0.7
			if (_n * 10 / _table.size() >= 7)
			{
				// 增容
				// a.创建新的哈希表，并将新表开辟到原始表的两倍
				size_t newSize = _table.size() * 2;
				HashTable<K, V, HashFunc> NewHashTable;
				NewHashTable._table.resize(newSize);

				// b.将原始表数据迁移到新表
				for (size_t i = 0; i < _table.size(); i++)
				{
					if (_table[i]._state == EXITS)
					{
						NewHashTable.Insert(_table[i]._kv);
					}
				}
				//for (auto& e : _table)
				//{
				//	if (e._state == EXITS)
				//	{
				//		NewHashTable.Insert(e._kv);
				//	}
				//}
				// c.交换原始表和新表
				_table.swap(NewHashTable._table);
			}

			// 3、将键值对插入到哈希表
			// 线性探索方式
			// a.取余计算原始哈希地址
			HashFunc hf;
			size_t hashi = hf(kv.first) % _table.size();
			// b.当是DELETE或者是EMPTY即插入，EXIST即跳过
			while (_table[hashi]._state == EXITS)
			{
				++hashi;
				hashi %= _table.size();// 防止超出
			}
			// c.插入进去并将状态进行改变
			_table[hashi]._kv = kv;
			_table[hashi]._state = EXITS;
			// 3、哈希表中的有效元素个数加1
			++_n;

			return true;
		}

		// 查找函数
		HashData<K, V>* Find(const K& key)
		{
			// 1通过哈希函数计算出对应的哈希地址。
			// 2从哈希地址处开始，采用线性探测向后进行数据的查找，
			// 直到找到待查找的元素判定为查找成功，或找到一个状态为EMPTY的位置判定为查找失败。
			HashFunc hf;
			size_t hashi = hf(key) % _table.size();
			while (_table[hashi]._state != EMPTY)
			{
				if (_table[hashi]._state == EXITS
					&& _table[hashi]._kv.first == key)
				{
					return (HashData<K, V>*) & _table[hashi];
				}
				++hashi;
				hashi %= _table.size();
			}

			// 没找到
			return nullptr;
		}

		// 删除
		bool Erase(const K& key)
		{
			// 1查看哈希表中是否存在该键值的键值对，若不存在则删除失败
			// 2若存在，则将该键值对所在位置的状态改为DELETE即可
			// 3哈希表中的有效元素个数减一
			HashData<K, V>* key_ = Find(key);
			if (key_)
			{
				key_->_state = DELETE;
				--_n;
				return true;
			}
			return false;
		}

	private:
		vector<HashData<K, V>> _table; //哈希表
		size_t _n = 0; //哈希表中的有效元素个数(用来控制负载因子)
	};

}

