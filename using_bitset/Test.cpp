#include<iostream>
#include<string>
using namespace std;

#include<bitset>

int main()
{
	bitset<8> bs1(string("10101010"));
	cout << bs1[0] << endl; // 0

	bs1[0] = 1;
	cout << bs1 << endl; // 10101011

	return 0;
}



//int main()
//{
//	bitset<8> bs1(string("10101010"));
//	bitset<8> bs2(string("01010101"));
//
//	cout << (bs1 & bs2) << endl; // 00000000
//	cout << (bs1 | bs2) << endl; // 11111111
//	cout << (bs1 ^ bs2) << endl; // 11111111
//
//	return 0;
//}



//int main()
//{
//	bitset<8> bs1(string("10101010"));
//	bitset<8> bs2(string("10101010"));
//
//	bs1 >>= 1;
//	cout << bs1 << endl; // 01010101
//
//	bs2 |= bs1;
//	cout << bs2 << endl; // 11111111
//
//	return 0;
//}



//int main()
//{
//	bitset<8> bs;
//	cin >> bs;           // 10011
//	cout << bs << endl;  // 00010011
//
//	return 0;
//}


//int main()
//{
//	bitset<8> bs;
//	bs.set(3); //设置第3位
//	bs.set(4); //设置第4位
//	cout << bs << endl; //00011000
//
//	bs.flip(); //反转所有位
//	cout << bs << endl; //11100111
//	// 计算位图里面的1的个数
//	cout << bs.count() << endl; //6
//
//	// 获取指定位的状态
//	cout << bs.test(2) << endl; //1
//
//	bs.reset(0); //清空第0位
//	cout << bs << endl; //11100110
//
//	bs.flip(7); //反转第7位
//	cout << bs << endl; //01100110
//
//	// 计算位图里面的1和0的总个数
//	cout << bs.size() << endl; //8
//
//	cout << bs.any() << endl; //1
//
//	bs.reset(); //清空所有位
//	cout << bs.none() << endl; //1
//
//	bs.set(); //设置所有位
//	cout << bs.all() << endl; //1
//	return 0;
//}





//int main()
//{
//	bitset<16> bs1; // 0000 0000 0000 0000
//	
//	bitset<16> bs2(0xfa5); // 0000 1111 1010 0101
//
//	bitset<16> bs3(string("10010011")); // 0000 0000 1001 0011
//
//	return 0;
//}