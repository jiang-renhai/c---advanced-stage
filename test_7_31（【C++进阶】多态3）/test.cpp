
#include<iostream>
using namespace std;

//class Person {
//public:
//	virtual void BuyTicket() const { cout << "买票-全价" << endl; }
//};
//class Student : public Person {
//public:
//	void BuyTicket() const { cout << "买票-半价" << endl; }
//};

//class A {};
//class B : public A {};
//
//class Person {
//public:
//	virtual A* BuyTicket() const 
//	{ 
//		cout << "买票-全价" << endl; 
//		return 0;
//	}
//};
//class Student : public Person {
//public:
//	virtual B* BuyTicket() const 
//	{ 
//		cout << "买票-半价" << endl;
//		return 0;
//	}
//};
//
//// 多态条件
//// 1. 必须通过基类的指针或者引用调用虚函数
//// 2. 被调用的函数必须是虚函数，且派生类必须对基类的虚函数进行重写
//
//// 多态，不同对象传递过去，调用不同的函数
//// 多态调用看的指向的对象
//// 普通对象 看当前类型
//void func(const Person& p)
//{
//	p.BuyTicket();
//}
//
//int main()
//{
//	Person ps;
//	Student st;
//
//	func(ps);
//	func(st);
//
//	return 0;
//}


//class Person {
//public:
//	 virtual ~Person() { cout << "~Person()" << endl; }
//};
//class Student : public Person {
//public:
//	virtual ~Student() 
//	{ 
//		cout << "~Student()" << endl;
//		delete[] ptr;
//	}
//protected:
//	int* ptr = new int[10];
//};
//
//int main()
//{
//	Person* p = new Person;
//	delete p;
//
//	p = new Student;
//	delete p;  // {满足指针}p->desturctor() + operator delete(p)
//
//	return 0;
//}


//class Car
//{
//public:
//	virtual void Drive() final {}
//};
//class Benz :public Car
//{
//public:
//	virtual void Drive() { cout << "Benz-舒适" << endl; }
//};

//
//class Car {
//public:
//	virtual void Drive() {}
//};
//class Benz :public Car {
//public:
//	virtual void Drive() override { cout << "Benz-舒适" << endl; }
//};
//
//int main()
//{
//
//	return 0;
//}


//class A
//{
//public:
//	A Aobj()
//	{
//		return A();
//	}
//private:
//	A()
//	{}
//};
//
//class B : public A
//{};
//
//int main()
//{
//
//	return 0;
//}


//class A final
//{
//public:
//};
//
//class B : public A 
//{};


//class A
//{
//public:
//	virtual void func(int val = 1) { std::cout << "A->" << val << std::endl; }
//	virtual void test() { func(); }
//};
//
//class B : public A
//{
//public:
//	void func(int val = 0) { std::cout << "B->" << val << std::endl; }
//};
//
//int main(int argc, char* argv[])
//{
//	B* p = new B;
//	p->test();
//	return 0;
//}


//class Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Func1()" << endl;
//	}
//private:
//	int _b = 1;
//};
//int main()
//{
//	Base b;
//	cout << sizeof(Base) << endl;
//	return 0;
//}



//class Person
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Base::Func1()" << endl;
//	}
//	virtual void Func2()
//	{
//		cout << "Base::Func2()" << endl;
//	}
//	void Func3()
//	{
//		cout << "Base::Func3()" << endl;
//	}
//private:
//	int _b = 1;
//};
//class Student : public Person
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Derive::Func1()" << endl;
//	}
//
//private:
//	int _d = 2;
//};
//int main()
//{
//	Person b;
//	Student d;
//	return 0;
//}





//class Person {
//public:
//	virtual void BuyTicket() { cout << "买票-全价" << endl; }
//	virtual void Func1() 
//	{
//		cout << "Person::Func1" << endl;
//	}
//	virtual void Func2() 
//	{
//		cout << "Person::Func2" << endl;
//	}
//protected:
//	int _a = 0;
//};
//
//class Student : public Person {
//public:
//	virtual void BuyTicket() { cout << "买票-半价" << endl; }
//	virtual void Func3() 
//	{
//		cout << "Student::Func3" << endl;
//	}
//protected:
//	int _b = 1;
//};
//
//typedef void(*FUNC_PTR) ();
//
//// 打印函数指针数组
//void PrintVFT(FUNC_PTR* table)
//{
//	for (size_t i = 0; table[i] != nullptr; i++)
//	{
//		printf("[%d]:%p\n", i, table[i]);
//	}
//	printf("\n");
//}
//
//int main()
//{
//	Person ps;
//	Student st;
//
//	int vft1 = *((int*)&ps);
//	PrintVFT((FUNC_PTR*)vft1);
//
//	int vft2 = *((int*)&st);
//	PrintVFT((FUNC_PTR*)vft2);
//	return 0;
//}


//int main()
//{
//	Person ps;
//	Student st;
//
//	int a = 0;
//	printf("栈：%p\n", &a);
//
//	static int b = 0;
//	printf("静态区：%p\n", &b);
//
//	int* p = new int;
//	printf("堆：%p\n", &p);
//
//	const char* str = "hello world";
//	printf("常量区：%p\n", str);
//
//	printf("虚表1：%p\n", *((int*)&ps));
//	printf("虚表2：%p\n", *((int*)&st));
//	return 0;
//}


//int main()
//{
//	Person ps;
//	Student st;
//
//	return 0;
//}


//class Person {
//public:
//	virtual void BuyTicket() { cout << "买票-全价" << endl; }
//};
//
//class Student : public Person {
//public:
//	virtual void BuyTicket() { cout << "买票-半价" << endl; }
//};
//
//void Func(Person& p)
//{
//	p.BuyTicket();
//}
//
//int main()
//{
//	Person ps;
//	Person* ptr = &ps;
//
//
//	return 0;
//}


//class Person 
//{
//public:
//	virtual void func1() { cout << "Person::func1" << endl; }
//	virtual void func2() { cout << "Person::func2" << endl; }
//private:
//	int _a;
//};
//
//class Student :public Person 
//{
//public:
//	virtual void func1() { cout << "Student::func1" << endl; }
//	virtual void func3() { cout << "Student::func3" << endl; }
//	virtual void func4() { cout << "Student::func4" << endl; }
//private:
//	int b;
//};
//
//int main()
//{
//	Person ps;
//	Student st;
//
//	return 0;
//}


//class Person 
//{
//public:
//	virtual void func1() { cout << "Person::func1" << endl; }
//	virtual void func2() { cout << "Person::func2" << endl; }
//private:
//	int _a;
//};
//
//class Student :public Person 
//{
//public:
//	virtual void func1() { cout << "Student::func1" << endl; }
//	virtual void func3() { cout << "Student::func3" << endl; }
//	virtual void func4() { cout << "Student::func4" << endl; }
//private:
//	int b;
//};
//
//typedef void(*VFPTR) ();
//void PrintVTable(VFPTR vTable[])
//{
//	cout << " 虚表地址>" << vTable << endl;
//	for (int i = 0; vTable[i] != nullptr; ++i)
//	{
//		printf(" 第%d个虚函数地址 :0X%x,->", i, vTable[i]);
//		VFPTR f = vTable[i];
//		f();
//	}
//	cout << endl;
//}
//int main()
//{
//	Person ps;
//	Student st;
//	VFPTR* vTableb = (VFPTR*)(*(int*)&ps);
//	PrintVTable(vTableb);
//	VFPTR* vTabled = (VFPTR*)(*(int*)&st);
//	PrintVTable(vTabled);
//	return 0;
//}


//class Base1 {
//public:
//	virtual void func1() 
//	{ cout << "Base1::func1" << endl; }
//	virtual void func2()
//	{ cout << "Base1::func2" << endl; }
//private:
//	int b1;
//};
//class Base2 {
//public:
//	virtual void func1()
//	{ cout << "Base2::func1" << endl; }
//	virtual void func2()
//	{ cout << "Base2::func2" << endl; }
//private:
//	int b2;
//};
//class Derive : public Base1, public Base2 {
//public:
//	virtual void func1()
//	{ cout << "Derive::func1" << endl; }
//	virtual void func3() 
//	{ cout << "Derive::func3" << endl; }
//private:
//	int d1;
//};
//
//int main()
//{
//	Derive d;
//	Base1* bs1 = &d;
//	bs1->func1();
//
//	Base2* bs2 = &d;
//	bs2->func1();
//
//	Derive* dr = &d;
//	dr->func1();
//	return 0;
//}

//class Base1 {
//public:
//	virtual void func1()
//	{
//		cout << "Base1::func1" << endl;
//	}
//	virtual void func2()
//	{
//		cout << "Base1::func2" << endl;
//	}
//private:
//	int b1;
//};
//class Base2 {
//public:
//	virtual void func1()
//	{
//		cout << "Base2::func1" << endl;
//	}
//	virtual void func2()
//	{
//		cout << "Base2::func2" << endl;
//	}
//private:
//	int b2;
//};
//class Derive : public Base1, public Base2 {
//public:
//	virtual void func1()
//	{
//		cout << "Derive::func1" << endl;
//	}
//	virtual void func3()
//	{
//		cout << "Derive::func3" << endl;
//	}
//private:
//	int d1;
//};
//typedef void(*VFPTR) ();
//void PrintVTable(VFPTR vTable[])
//{
//	cout << " 虚表地址>" << vTable << endl;
//	for (int i = 0; vTable[i] != nullptr; ++i)
//	{
//		printf(" 第%d个虚函数地址 :0X%x,->", i, vTable[i]);
//		VFPTR f = vTable[i];
//		f();
//	}
//	cout << endl;
//}
//int main()
//{
//	Derive d;
//	VFPTR* vTableb1 = (VFPTR*)(*(int*)&d);
//	PrintVTable(vTableb1);
//	VFPTR* vTableb2 = (VFPTR*)(*(int*)((char*)&d + sizeof(Base1)));
//	PrintVTable(vTableb2);
//	return 0;
//}
