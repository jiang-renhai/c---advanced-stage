#include"RBTree.h"


int main()
{
	//int a[] = { 16, 3, 7, 11, 9, 26, 18, 14, 15 };
	int a[] = { 4, 2, 6, 1, 3, 5, 15, 7, 16, 14 };
	RBTree<int, int> t;
	for (auto e : a)
	{
		t.Insert(make_pair(e, e));
		//cout << "Insert:" << e << "->" << t.IsBalance() << endl;
	}

	t.InOrder();
	cout << endl;

	int flag = t.IsRBTree();
	cout << flag << endl;

	cout << t.Height() << endl;

	t.Erase(99);
	t.InOrder();
	cout << endl;

	t.Erase(5);
	t.InOrder();
	cout << endl;

	t.Erase(2);
	t.InOrder();
	cout << endl;
	return 0;
}
