#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<cstring>
#include<string>
#include<algorithm>
#include<vector>
#include<cassert>
using namespace std;

// string类
namespace JRH
{
	class string
	{
	private:
		char* _str;
		size_t _size;
		size_t _capacity;
	public:
		// 构造函数
		string(const char* str = "")
		{
			_size = strlen(str); // 初始时字符串大小设置成字符字符串长度
			_capacity = _size; // 初始时字符串容量设置成字符串大小
			_str = new char[_capacity + 1]; // +1是为了后面还有个\0，为存储字符串开辟空间
			strcpy(_str, str);
		}
		// 析构函数
		~string()
		{
			delete[] _str; // 释放_str指向的空间
			_str = nullptr; // 置空
			_size = 0; // 字符串大小置0
			_capacity = 0; // 字符串容量置0
		}
		// 移动构造
		string(string&& s)
			:_str(nullptr)
			, _size(0)
			, _capacity(0)
		{
			std::cout << "string(string&& s)" << std::endl;
			swap(s);
		}
		// 移动赋值
		string& operator=(string&& s)
		{
			std::cout << "string& operator=(string&& s) -- 移动赋值" << std::endl;
			swap(s);
			return *this;
		}
		// 正向迭代器
		typedef char* iterator;
		// begin
		iterator begin()
		{
			return _str; // 返回字符串中首字母元素的地址
		}
		// end
		iterator end()
		{
			return _str + _size; // 返回字符串中最后一个字母的下一个字符的地址
		}
		// 交换两个对象的数据
		void swap(string& s)
		{
			// 调用库里面的swap
			::swap(_str, s._str); // 交换两个对象的字符串
			::swap(_size, s._size); // 交换两个对象的字符串大小
			::swap(_capacity, s._capacity); // 交换两个对象的字符串容量
		}
		// 拷贝构造（现代写法）--  深拷贝
		string(const string& str)
			:_str(nullptr)
			, _size(0)
			, _capacity(0)
		{
			std::cout << "string(const string& str) -- 深拷贝" << std::endl;
			// 先开辟一个tmp空间
			string tmp(str._str);
			swap(tmp); // 将tmp扔到swap中进行交换
		}
		// 赋值运算符操作
		string& operator=(const string& str)
		{
			std::cout << "string& operator=(const string& str) -- 深拷贝" << std::endl;
			// 开辟一个tmp临时空间
			string tmp(str);
			// 丢到swap将其进行交换
			swap(tmp);
			// 返回左值
			return *this;
		}
		// 运算符[]重载函数
		char& operator[](size_t i)
		{
			assert(i < _size);
			return _str[i]; // 返回下标相对应的字母
		}
		// 改容量，但大小不变
		void reserve(size_t n)
		{
			if (n > _capacity) // 只有当n大于容量的时候才进行修改
			{
				char* tmp = new char[n + 1]; // 开辟个临时空间，后面带上\0
				strncpy(tmp, _str, _size + 1); // 将_str的所有字符拷贝进tmp中
				delete[] _str; // 释放对象原本空间
				_str = tmp; // 将tmp的空间给_str（tmp只是临时空间，它需要给原住民_str开辟）
				_capacity = n; // 容量变成n
			}
		}
		// push_back进行尾插字符
		void push_back(char ch)
		{
			if (_size == _capacity) // 判断是否需要增容
			{
				reserve(_capacity == 0 ? 4 : _capacity * 2); // 初始为4，后面两倍两倍增加
			}
			_str[_size] = ch; // 尾插
			_str[_size++] = '\0'; // 后面一个字符为\0并将其_size++
		}
		// +=运算符重载
		string& operator+=(char ch)
		{
			push_back(ch);
			return *this; // 返回左值
		}
		// 返回c类型的字符串
		const char* c_str() const
		{
			return _str;
		}
	};
}

class Father
{
public:
	virtual void Print()
	{
		std::cout << "hello virtual void Print()" << std::endl;
	}
};
class Son : public Father
{
public:
	virtual void Print() override // 检查子类有没有将父类重写
	{
		std::cout << "hello virtual void Print()" << std::endl;
	}
};

//class Father
//{
//public:
//	virtual void Print() final // final修饰虚函数表示不能被重写
//	{
//		std::cout << "hello virtual void Print()" << std::endl;
//	}
//};
//class Son : public Father
//{
//public:
//	virtual void Print() // 重写,error
//	{
//		std::cout << "hello virtual void Print()" << std::endl;
//	}
//};

//class Person final // 被final修饰的最终类，不可被继承
//{
//	// ...
//};

//// Person类
//class Person
//{
//private:
//	JRH::string _name = "张三";
//	int _age = 21;
//public:
//	// 构造函数
//	Person()
//	{}
//private:
//	// 拷贝构造
//	Person(const Person& ps) = delete;
//	// 拷贝赋值
//	Person& operator=(const Person& ps) = delete;
//};

//// Person类
//class Person
//{
//private:
//	JRH::string _name = "张三";
//	int _age = 21;
//public:
//	Person() = default;
//	// 拷贝构造函数
//	Person(const Person& ps)
//		:_name(ps._name)
//		,_age(ps._age)
//	{}
//};
//
//int main()
//{
//	Person s;
//
//	return 0;
//}

//int main()
//{
//	Person s; // 不可(没有默认构造函数)
//
//	return 0;
//}

//// Person类
//class Person
//{
//private:
//	JRH::string _name = "张三"; // 姓名
//	int _age = 21;              // 年龄
//	static int _n;              // 静态成员变量不能给缺省值
//public:
//	// 构造函数
//	Person(const char* name = "", int age = 0)
//		:_name(name)
//		,_age(age)
//	{}
//	// 析构函数
//	~Person()
//	{}
//	// 拷贝构造函数
//	Person(const Person& ps)
//		:_name(ps._name)
//		,_age(ps._age)
//	{}
//	// 拷贝赋值函数
//	Person& operator=(const Person& ps)
//	{
//		if (this != &ps)
//		{
//			_name = ps._name;
//			_age = ps._age;
//		}
//		return *this;
//	}
//};

//int main()
//{
//	Person s1("张三", 21);
//	Person s2;
//	s2 = std::move(s1); // 想要调用Person默认生成的移动赋值
//	return 0;
//}


//int main()
//{
//	Person s1("张三", 21);
//	Person s2;
//	s2 = std::move(s1); //想要调用Person默认生成的移动赋值
//
//	return 0;
//}


//int main()
//{
//	Person s1("张三", 21);
//	Person s2(move(s1)); // 想要调用s1的默认生成的移动构造
//	return 0;
//}


//int main()
//{
//	int a = 10;
//	int b = 20;
//	auto Add1 = [](int& x, int& y)
//	{
//		int tmp = x;
//		x = y;
//		y = tmp;
//	};
//	auto Add2 = [](int& x, int& y)
//	{
//		int tmp = x;
//		x = y;
//		y = tmp;
//	};
//	std::cout << typeid(Add1).name() << std::endl; // class `int __cdecl main(void)'::`2'::<lambda_1>
//	std::cout << typeid(Add2).name() << std::endl; // class `int __cdecl main(void)'::`2'::<lambda_2>
//	return 0;
//}


//class Add
//{
//private:
//	int _base;
//public:
//	// 构造函数
//	Add(int base)
//		:_base(base)
//	{}
//	// operator重载
//	int operator()(int num)
//	{
//		return _base + num;
//	}
//};
//int main()
//{
//	int base = 1;
//	// 函数对象 -- 调用重载的operator函数
//	Add add1(base);
//	add1(100);
//
//	// lambda表达式
//	auto add2 = [base](int num)->int
//	{
//		return base + num;
//	};
//	add2(100);
//	return 0;
//}


//struct Goods
//{
//	string _name;  // 名字
//	double _price; // 价格
//	int _num;      // 数量
//};
//
//struct GoodsPriceLess // 按照价格升序排
//{
//	bool operator()(const Goods& g1, const Goods& g2)
//	{
//		return g1._price < g2._price;
//	}
//};
//
//struct GoodsPriceGreater // 按照价格降序排
//{
//	bool operator()(const Goods& g1, const Goods& g2)
//	{
//		return g1._price > g2._price;
//	}
//};
//
//struct GoodsNumLess // 按照数量升序排
//{
//	bool operator()(const Goods& g1, const Goods& g2)
//	{
//		return g1._num < g2._num;
//	}
//};
//
//struct GoodsNumGreater // 按照数量降序排
//{
//	bool operator()(const Goods& g1, const Goods& g2)
//	{
//		return g1._num > g2._num;
//	}
//};

//int main()
//{
//	int x = 10;
//	int y = 20;
//	auto Swap = [x, y]()mutable
//	{
//		int tmp = x;
//		x = y;
//		y = tmp;
//	};
//	Swap(); // 没有进行交换！
//	return 0;
//}
//
//int main()
//{
//	int x = 10;
//	int y = 20;
//	auto Swap = [&x, &y]
//	{
//		int tmp = x;
//		x = y;
//		y = tmp;
//	};
//	Swap();
//	return 0;
//}

//int main()
//{
//	int x = 10;
//	int y = 20;
//	auto Swap = [&]
//	{
//		int tmp = x;
//		x = y;
//		y = tmp;
//	};
//	Swap();
//	return 0;
//}

//int main()
//{
//	int x = 10; 
//	int y = 20;
//	auto Swap = [](int& x, int& y)->void
//	{
//		int tmp = x;
//		x = y;
//		y = tmp;
//	};
//	Swap(x, y);
//	return 0;
//}

//int main()
//{
//	[] {};// 最简单的lambda表达式
//	return 0;
//}

//int main()
//{
//	vector<Goods> v = { {"apple", 2.2, 200},{"banana", 2.3, 100},{"orange", 5.5, 50},{"pineapple", 7.8, 80} };
//	sort(v.begin(), v.end(), [](const Goods& g1, const Goods& g2)
//		{
//			return g1._price < g2._price;
//		});// 按价格升序
//	sort(v.begin(), v.end(), [](const Goods& g1, const Goods& g2)
//		{
//			return g1._price > g2._price;
//		});// 按价格降序
//	sort(v.begin(), v.end(), [](const Goods& g1, const Goods& g2)
//		{
//			return g1._num < g2._num;
//		});// 按数量升序
//	sort(v.begin(), v.end(), [](const Goods& g1, const Goods& g2)
//		{
//			return g1._num > g2._num;
//		});// 按数量降序
//	return 0;
//}


//int main()
//{
//	vector<Goods> v = { {"apple", 2.2, 200},{"banana", 2.3, 100},{"orange", 5.5, 50},{"pineapple", 7.8, 80} };
//	sort(v.begin(), v.end(), GoodsPriceLess()); // 按价格升序
//	sort(v.begin(), v.end(), GoodsPriceGreater()); // 按价格降序
//	sort(v.begin(), v.end(), GoodsNumLess()); // 按数量升序
//	sort(v.begin(), v.end(), GoodsNumGreater()); // 按数量降序
//	return 0;
//}
