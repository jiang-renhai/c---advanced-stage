#pragma once

#include<iostream>
#include<assert.h>
using namespace std;

//Key-Value模型
template<class K, class V>
struct AVLTreeNode
{
	//构造函数
	AVLTreeNode(const pair<K,V>& kv)
		:_left(nullptr)
		, _right(nullptr)
		, _parent(nullptr)
		, _kv(kv)
		, _bf(0)
	{}

	// 定义三叉链
	AVLTreeNode<K, V>* _left;
	AVLTreeNode<K, V>* _right;
	AVLTreeNode<K, V>* _parent;

	//存储键位值
	pair<K, V> _kv;

	//平衡因子
	int _bf;
};

template<class K,class V>
class AVLTree
{
	typedef AVLTreeNode<K, V> Node;
public:
	// 插入
	bool Insert(const pair<K, V>& kv)
	{
		// 找
		// 1、待插入结点key比当前结点小就往左子树跑
		// 2、待插入结点key比当前结点大就往右子树跑
		// 3、待插入结点key和当前结点的值相等就显示插入失败
		// 刚开始进去为空树
		if (_root == nullptr)
		{
			_root = new Node(kv);
			return true;
		}

		Node* parent = nullptr;
		Node* cur = _root;
		// 不是空树
		while (cur)
		{
			// 待插入结点比当前结点小，往右子树跑
			if (cur->_kv.first < kv.first)
			{
				parent = cur; // 保存一下cur原本的结点
				cur = cur->_right; // 往右跑
			}
			else if (cur->_kv.first > kv.first)
			{
				parent = cur; // 保存一下cur原本的结点
				cur = cur->_left;
			}
			else
			{
				// 发现待插入的结点的值和当前节点相同
				return false;
			}
		}

		// 插入数值
		cur = new Node(kv);
		// 父亲结点小于待插入结点
		if (parent->_kv.first < kv.first)
		{
			// 往右子树插入
			parent->_right = cur;
		}
		// 父亲结点小于待插入结点
		else
		{
			// 往左子树插入
			parent->_left = cur;
		}
		// 当前节点的父节点为parent结点，更新一下防止关系乱
		cur->_parent = parent;

		// 控制平衡
		// 平衡因子的平衡
		while (parent)
		{
			if (cur == parent->_right)
			{
				parent->_bf++;
			}
			else
			{
				parent->_bf--;
			}

			if (parent->_bf == 1 || parent->_bf == -1)
			{
				//继续往上更新
				cur = parent;
				parent = parent->_parent;
			}
			else if (parent->_bf == 0)
			{
				//over
				break;
			}
			else if (parent->_bf == 2 || parent->_bf == -2)
			{
				//旋转
				if (parent->_bf == 2 && cur->_bf == 1)
				{
					// 左单旋
					RoateL(parent);
				}
				else if (parent->_bf == -2 && cur->_bf == -1)
				{
					// 右单旋
					RoateR(parent);
				}
				else if (parent->_bf == -2 && cur->_bf == 1)
				{
					// 左右单旋
					RoateLR(parent);
				}
				else if (parent->_bf == 2 && cur->_bf == -1)
				{
					// 右左单旋
					RoateRL(parent);
				}
				break;
			}
			else
			{
				assert(false);
			}
		}
		return true;
	}

	// 左单旋
	void RoateL(Node* parent)
	{
		// 三叉链
		Node* subr = parent->_right;
		Node* subrl = subr->_left;
		Node* ppnode = parent->_parent;

		// subrl与parent的关系
		parent->_right = subrl;
		if (subrl)
			subrl->_parent = parent;

		// subl和parent的关系
		subr->_left = parent;
		parent->_parent = subr;

		// ppnode和subr的关系
		if (ppnode == nullptr)
		{
			_root = subr;
			subr->_parent = nullptr;
		}
		else
		{
			if (ppnode->_left == parent)
			{
				ppnode->_left = subr;
			}
			else
			{
				ppnode->_right = subr;
			}
			subr->_parent = ppnode;
		}

		// 更新平衡因子
		subr->_bf = parent->_bf = 0;
	}

	// 右单旋
	void RoateR(Node* parent)
	{
		// 三叉链
		Node* subl = parent->_left;
		Node* sublr = subl->_right;
		Node* ppnode = parent->_parent;


		//sublr和parent之间的关系
		parent->_left = sublr;
		if (sublr)
			sublr->_parent = parent;

		//subl和parent的关系
		subl->_right = parent;
		parent->_parent = subl;


		//ppnode 和 subl的关系
		if (ppnode == nullptr)
		{
			_root = subl;
			subl->_parent = nullptr;
		}
		else
		{
			if (ppnode->_left == parent)
			{
				ppnode->_left = subl;
			}
			else
			{
				ppnode->_right = subl;
			}
			subl->_parent = ppnode;
		}

		//更新平衡因子
		subl->_bf = parent->_bf = 0;
	}

	// 左右双旋
	void RoateLR(Node* parent)
	{
		Node* subl = parent->_left;
		Node* sublr = subl->_right;
		int bf = sublr->_bf;

		//subl节点左单旋
		RoateL(subl);

		//parent节点进行右单旋
		RoateR(parent);

		//更新平衡因子
		if (bf == 1)
		{
			sublr->_bf = 0;
			subl->_bf = -1;
			parent->_bf = 0;
		}
		else if (bf == -1)
		{
			sublr->_bf = 0;
			subl->_bf = 0;
			parent->_bf = 1;
		}
		else if (bf == 0)
		{
			sublr->_bf = 0;
			subl->_bf = 0;
			parent->_bf = 0;
		}
		else
		{
			assert(false);
		}
	}

	// 右左双旋
	void RoateRL(Node* parent)
	{
		Node* subr = parent->_right;
		Node* subrl = subr->_left;
		int bf = subrl->_bf;

		//subR右单旋
		RoateR(subr);

		//parent左单旋
		RoateL(parent);

		// 更新平衡因子
		if (bf == 1)
		{
			subrl->_bf = 0;
			subr->_bf = 0;
			parent->_bf = -1;
		}
		else if (bf == -1)
		{
			subrl->_bf = 0;
			subr->_bf = 1;
			parent->_bf = 0;
		}
		else if (bf == 0)
		{
			subrl->_bf = 0;
			subr->_bf = 0;
			parent->_bf = 0;
		}
		else
		{
			assert(false);
		}
	}
	 

	// 中序遍历验证是个搜索树
	void _InOrder(Node* root)
	{
		if (root == nullptr)
		{
			return;
		}

		_InOrder(root->_left);
		_InOrder(root->_right);
		cout << root->_kv.first << ":" << root->_kv.second << endl;

	}

	void InOrder()
	{
		_InOrder(_root);
	}

	// 计算子树高度
	int Height()
	{
		return Height(_root);
	}

	int Height(Node* root)
	{
		if (root == nullptr)
			return 0;

		int leftHeight = Height(root->_left);
		int rightHeight = Height(root->_right);

		return leftHeight > rightHeight ? leftHeight + 1 : rightHeight + 1;
	}

	// 判断是不是平衡树
	bool IsBalance()
	{
		return IsBalance(_root);
	}

	bool IsBalance(Node* root)
	{
		if (root == nullptr)
			return true;
		// 后序遍历
		int leftHight = Height(root->_left);
		int rightHight = Height(root->_right);
		// 计算两个树的高度之差即可，只要是高度之差不等于根节点的平衡因子的值
		if (rightHight - leftHight != root->_bf)
		{
			cout << "平衡因子异常:" << root->_kv.first << "->" << root->_bf << endl;
			return false;
		}
		else
		{
			cout << "没问题" << endl;
		}
		// 递归判断条件
		return abs(rightHight - leftHight) < 2
			&& IsBalance(root->_left)
			&& IsBalance(root->_right);
	}


private:
	Node* _root = nullptr;
};

