#include<vector>
#include<list>
#include<map>
#include"C++11.h"
#include<iostream>
#include<array>
using namespace std;

int main()
{
	array<int, 10> a1;   //定义一个可存储10个int类型元素的array容器
	array<double, 5> a2; //定义一个可存储5个double类型元素的array容器
	return 0;
}


//int main()
//{
//	int arr[10] = { 0,1,2,3,4,5,6,7,8,9 };
//	// 将数组中元素都*2
//	for (auto& e : arr)
//	{
//		e *= 2;
//	}
//	// 打印数组中所有元素
//	for (auto& e : arr)
//	{
//		cout << e << " ";
//	}
//	cout << endl;
//	return 0;
//}

//int main()
//{
//	int arr[10] = { 0,1,2,3,4,5,6,7,8,9 };
//	// 将数组中元素都*2
//	for (int i = 0; i < sizeof(arr) / sizeof(arr[0]); i++)
//	{
//		arr[i] *= 2;
//	}
//	// 打印数组中所有元素
//	for (int i = 0; i < sizeof(arr) / sizeof(arr[0]); i++)
//	{
//		cout << arr[i] << " ";
//	}
//	cout << endl;
//	return 0;
//}


//void f(int arc)
//{
//	cout << "void f(int arc)" << endl;
//}
//void f(int* arc)
//{
//	cout << "void f(int* arc)" << endl;
//}
//int main()
//{
//	f(NULL);    //void f(int arc)
//	f(nullptr); //void f(int* arc)
//	return 0;
//}



//template<class T1, class T2>
//auto Add(T1 t1, T2 t2) -> decltype(t1 + t2)
//{
//	decltype(t1 + t2) ret;
//	ret = t1 + t2;
//	cout << typeid(ret).name() << endl;
//	return ret;
//}
//
//int main()
//{
//	cout << Add(1, 2) << endl;;   //int
//	cout << Add(1, 2.2) << endl;; //double
//	return 0;
//}


//void* GetMemory(size_t size)
//{
//	return malloc(size);
//}
//int main()
//{
//	//如果没有带参数，推导函数的类型
//	cout << typeid(decltype(GetMemory)).name() << endl;
//	//如果带参数列表，推导的是函数返回值的类型，注意：此处只是推演，不会执行函数
//	cout << typeid(decltype(GetMemory(0))).name() << endl;
//	return 0;
//}



//template<class T1, class T2>
//void Func(T1 t1, T2 t2)
//{
//	decltype(t1 * t2) ret;
//	cout << typeid(ret).name() << endl;
//}
//
//int main()
//{
//	const int x = 1;
//	double y = 2.2;
//	decltype(x * y) xy; // double
//	decltype(&x) xx; // int const*
//	Func(1, 'a'); // int
//	Func(1, 2.2); // double
//	return 0;
//}



//int main()
//{
//	int i = 20;
//	auto p = &i;
//	auto pf = strcpy;
//	cout << typeid(p).name() << endl;
//	cout << typeid(pf).name() << endl;
//	map<string, string> dict = { {"sort", "排序"}, {"insert", "插入"} };
//	//map<string, string>::iterator it = dict.begin();
//	auto it = dict.begin();
//	return 0;
//}





//class Date
//{
//public:
//	Date(int year, int month, int day)
//		: _year(year)
//		, _month(month)
//		, _day(day)
//	{
//		cout << "Date(int year, int month, int day)" << endl;
//	}
//
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	//用大括号括起来的列表对容器进行初始化
//	vector<int> v = { 1, 2, 3, 4, 5 };
//	list<int> l = { 10, 20, 30, 40, 50 };
//	vector<Date> vd = { Date(2022, 8, 29), Date{ 2022, 8, 30 }, { 2022, 8, 31 } };
//	map<string, string> m{ make_pair("sort", "排序"), { "insert", "插入" } };
//
//	//用大括号括起来的列表对容器赋值
//	v = { 5, 4, 3, 2, 1 };
//
//	return 0;
//}


//int main()
//{
//	auto il = { 1,2,3,4,5 };
//	cout << typeid(il).name() << endl; //class std::initializer_list<int>
//	return 0;
//}


//class Date
//{
//public:
//	Date(int year, int month, int day)
//		: _year(year)
//		, _month(month)
//		, _day(day)
//	{
//		cout << "Date(int year, int month, int day)" << endl;
//	}
//
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	// 一般调用逻辑
//	Date d1(2023, 10, 22);
//
//	// C++11用花括号初始化
//	Date d2 = { 2023,10,22 }; // 可加等号
//	Date d3{ 2023,10,22 }; // 可不加等号
//
//	return 0;
//}

//struct Point
//{
//	int _x;
//	int _y;
//};
//
//int main()
//{
//	// 使用大括号对内置类型进行初始化
//	int x = { 0 }; // 可使用等号
//	int y{ 0 }; // 可不使用等号
//
//	// 使用大括号对数组元素进行初始化
//	int arr1[]{ 1,2,3,4,5 }; // 可不用等号
//	int arr2[5]{ 0 }; // 可不用等号
//
//	// 使用大括号对自定义类型进行初始化
//	Point p{ 1,2 };
//
//	// 用于对new的表达式
//	int* p1 = new int[4] {0}; // 不可加等号
//	int* p2 = new int[4] {1, 2, 3, 4}; // 不可加等号
//
//	return 0;
//}


//int main()
//{
//	// 使用大括号对数组元素进行初始化
//	int arr1[] = { 0 };
//	int arr2[5] = { 1,2,3,4,5 };
//
//	// 使用大括号对自定义类型进行初始化
//	Point p = { 1,2 };
//	return 0;
//}
