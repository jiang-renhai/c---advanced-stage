#pragma once


#pragma once

#include<iostream>
#include<assert.h>
using namespace std;

//Key-Value模型
template<class K, class V>
struct AVLTreeNode
{
	//构造函数
	AVLTreeNode(const pair<K, V>& kv)
		:_left(nullptr)
		, _right(nullptr)
		, _parent(nullptr)
		, _kv(kv)
		, _bf(0)
	{}

	// 定义三叉链
	AVLTreeNode<K, V>* _left;
	AVLTreeNode<K, V>* _right;
	AVLTreeNode<K, V>* _parent;

	//存储键位值
	pair<K, V> _kv;

	//平衡因子
	int _bf;
};

template<class K, class V>
class AVLTree
{
	typedef AVLTreeNode<K, V> Node;
public:
	// 插入
	bool Insert(const pair<K, V>& kv)
	{
		// 找
		// 1、待插入结点key比当前结点小就往左子树跑
		// 2、待插入结点key比当前结点大就往右子树跑
		// 3、待插入结点key和当前结点的值相等就显示插入失败
		// 刚开始进去为空树
		if (_root == nullptr)
		{
			_root = new Node(kv);
			return true;
		}

		Node* parent = nullptr;
		Node* cur = _root;
		// 不是空树
		while (cur)
		{
			// 待插入结点比当前结点小，往右子树跑
			if (cur->_kv.first < kv.first)
			{
				parent = cur; // 保存一下cur原本的结点
				cur = cur->_right; // 往右跑
			}
			else if (cur->_kv.first > kv.first)
			{
				parent = cur; // 保存一下cur原本的结点
				cur = cur->_left;
			}
			else
			{
				// 发现待插入的结点的值和当前节点相同
				return false;
			}
		}

		// 插入数值
		cur = new Node(kv);
		// 父亲结点小于待插入结点
		if (parent->_kv.first < kv.first)
		{
			// 往右子树插入
			parent->_right = cur;
		}
		// 父亲结点小于待插入结点
		else
		{
			// 往左子树插入
			parent->_left = cur;
		}
		// 当前节点的父节点为parent结点，更新一下防止关系乱
		cur->_parent = parent;

		// 控制平衡
		// 平衡因子的平衡
		while (parent)
		{
			if (cur == parent->_right)
			{
				parent->_bf++;
			}
			else
			{
				parent->_bf--;
			}

			if (parent->_bf == 1 || parent->_bf == -1)
			{
				//继续往上更新
				cur = parent;
				parent = parent->_parent;
			}
			else if (parent->_bf == 0)
			{
				//over
				break;
			}
			else if (parent->_bf == 2 || parent->_bf == -2)
			{
				//旋转
				if (parent->_bf == 2 && cur->_bf == 1)
				{
					// 左单旋
					RoateL(parent);
				}
				else if (parent->_bf == -2 && cur->_bf == -1)
				{
					// 右单旋
					RoateR(parent);
				}
				else if (parent->_bf == -2 && cur->_bf == 1)
				{
					// 左右单旋
					RoateLR(parent);
				}
				else if (parent->_bf == 2 && cur->_bf == -1)
				{
					// 右左单旋
					RoateRL(parent);
				}
				break;
			}
			else
			{
				assert(false);
			}
		}
		return true;
	}

	// 左单旋
	void RoateL(Node* parent)
	{
		// 三叉链
		Node* subr = parent->_right;
		Node* subrl = subr->_left;
		Node* ppnode = parent->_parent;

		// subrl与parent的关系
		parent->_right = subrl;
		if (subrl)
			subrl->_parent = parent;

		// subl和parent的关系
		subr->_left = parent;
		parent->_parent = subr;

		// ppnode和subr的关系
		if (ppnode == nullptr)
		{
			_root = subr;
			subr->_parent = nullptr;
		}
		else
		{
			if (ppnode->_left == parent)
			{
				ppnode->_left = subr;
			}
			else
			{
				ppnode->_right = subr;
			}
			subr->_parent = ppnode;
		}

		// 更新平衡因子
		subr->_bf = parent->_bf = 0;
	}

	// 右单旋
	void RoateR(Node* parent)
	{
		// 三叉链
		Node* subl = parent->_left;
		Node* sublr = subl->_right;
		Node* ppnode = parent->_parent;


		//sublr和parent之间的关系
		parent->_left = sublr;
		if (sublr)
			sublr->_parent = parent;

		//subl和parent的关系
		subl->_right = parent;
		parent->_parent = subl;


		//ppnode 和 subl的关系
		if (ppnode == nullptr)
		{
			_root = subl;
			subl->_parent = nullptr;
		}
		else
		{
			if (ppnode->_left == parent)
			{
				ppnode->_left = subl;
			}
			else
			{
				ppnode->_right = subl;
			}
			subl->_parent = ppnode;
		}

		//更新平衡因子
		subl->_bf = parent->_bf = 0;
	}

	// 左右双旋
	void RoateLR(Node* parent)
	{
		Node* subl = parent->_left;
		Node* sublr = subl->_right;
		int bf = sublr->_bf;

		//subl节点左单旋
		RoateL(subl);

		//parent节点进行右单旋
		RoateR(parent);

		//更新平衡因子
		if (bf == 1)
		{
			sublr->_bf = 0;
			subl->_bf = -1;
			parent->_bf = 0;
		}
		else if (bf == -1)
		{
			sublr->_bf = 0;
			subl->_bf = 0;
			parent->_bf = 1;
		}
		else if (bf == 0)
		{
			sublr->_bf = 0;
			subl->_bf = 0;
			parent->_bf = 0;
		}
		else
		{
			assert(false);
		}
	}

	// 右左双旋
	void RoateRL(Node* parent)
	{
		Node* subr = parent->_right;
		Node* subrl = subr->_left;
		int bf = subrl->_bf;

		//subR右单旋
		RoateR(subr);

		//parent左单旋
		RoateL(parent);

		// 更新平衡因子
		if (bf == 1)
		{
			subrl->_bf = 0;
			subr->_bf = 0;
			parent->_bf = -1;
		}
		else if (bf == -1)
		{
			subrl->_bf = 0;
			subr->_bf = 1;
			parent->_bf = 0;
		}
		else if (bf == 0)
		{
			subrl->_bf = 0;
			subr->_bf = 0;
			parent->_bf = 0;
		}
		else
		{
			assert(false);
		}
	}

	// AVL树的查找
	Node* Find(const K& key)
	{
		Node* cur = _root;
		while (cur)
		{
			// 待查找的结点小于当前结点，往左子树走
			if (key < cur->_kv.first)
			{
				cur = cur->_left;
			}

			// 待查找的结点大于当前结点，往右子树走
			else if (key > cur->_kv.first)
			{
				cur = cur->_right;
			}
			// 结点值相等，找到了
			else
			{
				return cur;
			}
		}
		// 没这个结点，找不到
		return nullptr;
	}

	// AVL树的修改
	bool Modify(const K& key, const V& value)
	{
		// 1、先找到利用Find函数
		Node* ret = Find(key);
		if (ret == nullptr)
			return false;
		// 2、修改value值
		ret->_kv.second = value;
		return true;
	}

	// AVL树的修改2
	pair<Node*, bool> Modify(const pair<K, V>& kv)
	{
		// 找
		// 1、待插入结点key比当前结点小就往左子树跑
		// 2、待插入结点key比当前结点大就往右子树跑
		// 3、待插入结点key和当前结点的值相等就显示插入失败
		// 刚开始进去为空树
		if (_root == nullptr)
		{
			_root = new Node(kv);
			return make_pair(_root, true);
		}

		Node* parent = nullptr;
		Node* cur = _root;
		// 不是空树
		while (cur)
		{
			// 待插入结点比当前结点小，往右子树跑
			if (cur->_kv.first < kv.first)
			{
				parent = cur; // 保存一下cur原本的结点
				cur = cur->_right; // 往右跑
			}
			else if (cur->_kv.first > kv.first)
			{
				parent = cur; // 保存一下cur原本的结点
				cur = cur->_left;
			}
			else
			{
				// 发现待插入的结点的值和当前节点相同
				return make_pair(cur, false);
			}
		}

		// 插入数值
		cur = new Node(kv);
		Node* newnode = cur;
		// 父亲结点小于待插入结点
		if (parent->_kv.first < kv.first)
		{
			// 往右子树插入
			parent->_right = cur;
		}
		// 父亲结点小于待插入结点
		else
		{
			// 往左子树插入
			parent->_left = cur;
		}
		// 当前节点的父节点为parent结点，更新一下防止关系乱
		cur->_parent = parent;

		// 控制平衡
		// 平衡因子的平衡
		while (parent)
		{
			if (cur == parent->_right)
			{
				parent->_bf++;
			}
			else
			{
				parent->_bf--;
			}

			if (parent->_bf == 1 || parent->_bf == -1)
			{
				//继续往上更新
				cur = parent;
				parent = parent->_parent;
			}
			else if (parent->_bf == 0)
			{
				//over
				break;
			}
			else if (parent->_bf == 2 || parent->_bf == -2)
			{
				//旋转
				if (parent->_bf == 2 && cur->_bf == 1)
				{
					// 左单旋
					RoateL(parent);
				}
				else if (parent->_bf == -2 && cur->_bf == -1)
				{
					// 右单旋
					RoateR(parent);
				}
				else if (parent->_bf == -2 && cur->_bf == 1)
				{
					// 左右单旋
					RoateLR(parent);
				}
				else if (parent->_bf == 2 && cur->_bf == -1)
				{
					// 右左单旋
					RoateRL(parent);
				}
				break;
			}
			else
			{
				assert(false);
			}
		}
		return make_pair(newnode, true);
	}

	// AVL[]重载
	V& operator[](const K& key)
	{
		// 键对值
		pair<Node*, bool> ret = Insert(make_pair(key, V()));
		// 拿出结点的node
		Node* node = ret.first;
		// 返回该结点的value引用值
		return node->_kv.second;
	}

	// 中序遍历验证是个搜索树
	void _InOrder(Node* root)
	{
		if (root == nullptr)
		{
			return;
		}

		_InOrder(root->_left);
		_InOrder(root->_right);
		cout << root->_kv.first << ":" << root->_kv.second << endl;

	}

	void InOrder()
	{
		_InOrder(_root);
	}

	// AVL树的删除
	bool Erase(const K& key)
	{
		// 用于遍历二叉树找结点
		Node* parent = nullptr;
		Node* cur = _root;
		// 用于标记实际的删除结点及其父结点
		Node* delparentpos = nullptr;
		Node* delpos = nullptr;
		// 先找到
		while (cur)
		{
			// 所给key值小于当前节点的值 -- 往左树走
			if (key < cur->_kv.first)
			{
				parent = cur;
				cur = cur->_left;
			}
			// 所给key值大于当前结点的值 -- 往右树走
			else if (key > cur->_kv.first)
			{
				parent = cur;
				cur = cur->_right;
			}
			// 找到了
			else
			{
				// 左子树为空
				if (cur->_left == nullptr)
				{
					// 待删除结点是根节点
					if (cur == _root)
					{
						// 让根节点的右子树作为新的结点
						_root = _root->_right;
						if (_root)
							_root->_parent = nullptr;
						delete cur; // 删除原节点
						return true;
					}
					else // 不是根节点
					{
						delparentpos = parent; // 标记当前待删除结点的父节点
						delpos = cur; // 标记当前待删除的结点
					}
					break; // 删除结点有祖先的结点，需要更新平衡因子
				}
				// 右子树为空
				else if (cur->_right == nullptr)
				{
					// 待删除结点是根节点
					if (cur == _root)
					{
						// 让根节点的左子树作为新的结点
						_root = _root->_left;
						if (_root)
							_root->_parent = nullptr;
						delete cur; // 删除原节点
						return true;
					}
					else // 不是根节点
					{
						delparentpos = parent; // 标记当前待删除结点的父节点
						delpos = cur; // 标记当前待删除的结点
					}
					break; // 删除结点有祖先的结点，需要更新平衡因子
				}
				// 左右子树都不为空
				else
				{
					// 替换法
					// 寻找待删除结点的右子树中的最小值
					Node* minparent = cur;
					Node* minright = cur->_right;
					while (minright->_left)
					{
						minparent = minright; // 记录一下父节点
						minright = minright->_left; // 往左子树走
					}
					cur->_kv.first = minright->_kv.first;// 将待删除结点first替换为右子树的最小值
					cur->_kv.second = minparent->_kv.second;// 将待删除结点second替换为右子树的最小值
					// 记录一下要删除的父节点
					delparentpos = minparent;
					// 记录一下实际要删除的结点
					delpos = minright;
					break; // 祖先结点的平衡因子需要改变
				}
			}
		}
		// 没有被修改过，说明没找到当前要删除的结点
		if (delparentpos == nullptr)
			return false;

		// 记录当前要删除结点和当前要删除结点的父节点
		Node* del = delpos;
		Node* delP = delparentpos;

		// 更新平衡因子
		while (delpos != _root) // 最坏情况是一路更新到根节点
		{
			// 删除结点在右子树
			if (delpos == delparentpos->_right)
				delparentpos->_bf--;
			// 删除结点在左子树
			else if (delpos == delparentpos->_left)
				delparentpos->_bf++;
			// 判断是否需要旋转
			if (delparentpos->_bf == 0)
			{
				// 向上更新
				delpos = delparentpos;
				delpos = delpos->_parent;
			}
			else if (delparentpos->_bf == -1 || delparentpos->_bf == 1)
			{
				// 要删除的父节点的平衡因子为1/-1，无需向上更新平衡因子
				break;
			}
			else if (delparentpos->_bf == 2 || delparentpos->_bf == -2)
			{
				// 旋转
				// 1、右旋
				if (delparentpos->_bf == -2 && delparentpos->_left->_bf == -1)
				{
					// 记录一下右旋后的根节点
					Node* right_root = delparentpos->_left;
					// 右旋
					RoateR(delparentpos);
					// 更新根节点
					delparentpos = right_root;
				}
				// 2、左右双旋
				else if (delparentpos->_bf == -2 && delparentpos->_left->_bf == 1)
				{
					// 记录一下左右双旋后的根节点
					Node* right_left_root = delparentpos->_left->_right;
					// 左右双旋
					RoateLR(delparentpos);
					// 更新根节点
					delparentpos = right_left_root;
				}
				// 3、右单旋
				else if (delparentpos->_bf == -2 && delparentpos->_left->_bf == 0)
				{
					// 不需要往上更新节点
					Node* right_root = delparentpos->_left;
					RoateR(delparentpos);
					delparentpos = right_root;
					// 更新当前平衡因子
					delparentpos->_bf = 1;
					delparentpos->_right->_bf = -1;
					break;
				}
				// 4、右左单旋
				else if (delparentpos->_bf == -1 && delparentpos->_right->_bf == -1)
				{
					// 记录一下当前要删除的结点
					Node* right_left_root = delparentpos->_right->_left;
					RoateRL(delparentpos);
					delparentpos = right_left_root;
				}
				// 5、左单旋
				else if (delparentpos->_bf == 2 && delparentpos->_right->_bf == 1)
				{
					Node* left_root = delparentpos->_right;
					RoateL(delparentpos);
					delparentpos = left_root;
				}
				// 6、左单旋
				else if (delparentpos->_bf == 2 && delparentpos->_parent == 0)
				{
					// 不需要向上更新节点
					Node* left_root = delparentpos->_right;
					RoateL(delparentpos);
					delparentpos = left_root;
					// 更新当前平衡因子
					delparentpos->_bf = -1;
					delparentpos->_left->_bf = 1;
					break;
				}
			}
			else
			{
				assert(false);
			}

			// 继续向上更新
			delpos = delparentpos;
			delparentpos = delparentpos->_parent;
		}

		// 实际删除操作
		// 左子树为空
		if (del->_left == nullptr)
		{
			// 实际删除的结点刚好是其父节点的左孩子
			if (del == delP->_left)
			{
				delP->_left = del->_right;
				if (del->_right)
					del->_right->_parent = parent;
			}
			// 实际删除的结点刚好是其父节点的右孩子
			else
			{
				delP->_right = del->_right;
				if (del->_right)
					del->_right->_parent = parent;
			}
		}
		// 右子树为空
		else
		{
			// 实际删除的结点刚好是其父节点的左孩子
			if (del == delP->_left)
			{
				delP->_left = del->_left;
				if (del->_left)
					del->_left->_parent = parent;
			}
			// 实际删除的结点刚好是其父节点的右孩子
			else
			{
				delP->_right = del->_left;
				if (del->_left)
					del->_left->_parent = parent;
			}
		}
		// 删除实际结点
		delete del;
		return true;
	}

	// 计算子树高度
	int Height()
	{
		return Height(_root);
	}

	int Height(Node* root)
	{
		if (root == nullptr)
			return 0;

		int leftHeight = Height(root->_left);
		int rightHeight = Height(root->_right);

		return leftHeight > rightHeight ? leftHeight + 1 : rightHeight + 1;
	}

	// 判断是不是平衡树
	bool IsBalance()
	{
		return IsBalance(_root);
	}

	bool IsBalance(Node* root)
	{
		if (root == nullptr)
			return true;
		// 后序遍历
		int leftHight = Height(root->_left);
		int rightHight = Height(root->_right);
		// 计算两个树的高度之差即可，只要是高度之差不等于根节点的平衡因子的值
		if (rightHight - leftHight != root->_bf)
		{
			cout << "平衡因子异常:" << root->_kv.first << "->" << root->_bf << endl;
			return false;
		}
		else
		{
			cout << "没问题" << endl;
		}
		// 递归判断条件
		return abs(rightHight - leftHight) < 2
			&& IsBalance(root->_left)
			&& IsBalance(root->_right);
	}


private:
	Node* _root = nullptr;
};

