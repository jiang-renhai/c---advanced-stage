#include<iostream>
#include<thread>
#include<mutex>
using namespace std;

void func(atomic<int>& n, int times)
{
	for (int i = 0; i < times; i++)
	{
		n++;
	}
}
int main()
{
	atomic<int> n = { 0 };
	int times = 10000;
	thread t1(func, ref(n), times);
	thread t2(func, ref(n), times);
	t1.join();
	t2.join();
	std::cout << n << std::endl;
	return 0;
}


//void func(atomic_int& n, int times)
//{
//	for (int i = 0; i < times; i++)
//	{
//		n++;
//	}
//}
//int main()
//{
//	atomic_int n = { 0 };
//	int times = 10000;
//	thread t1(func, ref(n), times);
//	thread t2(func, ref(n), times);
//	t1.join();
//	t2.join();
//	std::cout << n << std::endl;
//	return 0;
//}

//void func(int& n, int times, mutex& mtx)
//{
//	mtx.lock();
//	for (int i = 0; i < times; i++)
//	{
//		n++;
//	}
//	mtx.unlock();
//}
//
//int main()
//{
//	int n = 0;
//	int times = 10000;
//	mutex mtx;
//	thread t1(func, ref(n), times, ref(mtx));
//	thread t2(func, ref(n), times, ref(mtx));
//	t1.join();
//	t2.join();
//	std::cout << n << std::endl;
//	return 0;
//}


//void func(int& n, int times)
//{
//	for (int i = 0; i < times; i++)
//	{
//		n++;
//	}
//}
//
//int main()
//{
//	int n = 0;
//	int times = 10000;
//	thread t1(func, ref(n), times);
//	thread t2(func, ref(n), times);
//	t1.join();
//	t2.join();
//	std::cout << n << std::endl;
//	return 0;
//}


//mutex mtx;
//void func2()
//{}
//
//void func1()
//{
//	unique_lock<mutex> u1(mtx); // 调用构造函数加锁
//	u1.unlock(); // 解锁
//	func2();
//	u1.lock(); // 加锁
//} // 调用析构函数解锁
//
//int main()
//{
//	func1();
//	return 0;
//}


//namespace JRH
//{
//	template<class Mutex>
//	class lock_guard
//	{
//	public:
//		lock_guard(Mutex& mtx)
//			:_mtx(mtx)
//		{
//			_mtx.lock(); // 加锁
//		}
//		~lock_guard()
//		{
//			_mtx.unlock();
//		}
//		// 防拷贝
//		lock_guard(const Mutex&) = delete;
//		lock_guard& operator=(const Mutex&) = delete;
//	private:
//		Mutex& _mtx;
//	};
//}

//mutex mtx;
//bool Stop()
//{
//	return false;
//}
//
//void func()
//{
//	// 保护一段匿名的代码区间
//	{
//		lock_guard<mutex> lg(mtx); // 调用构造函数构造
//		if (!Stop())
//			return; // 调用析构函数析构
//	}
//
//}// 生命周期结束后调用析构函数析构
//int main()
//{
//	func();
//	return 0;
//}



//bool Stop()
//{
//	return false;
//}
//
//void func(int n, mutex& mtx)
//{
//	mtx.lock();
//	if (!Stop())
//		return;// 这里提前返回了，unlock是走不到的
//
//	mtx.unlock();
//}
//int main()
//{
//	mutex mtx; // 锁
//	thread t1(func, 10, ref(mtx));
//	thread t2(func, 10, ref(mtx));
//
//	t1.join();
//	t2.join();
//	return 0;
//}

//void func(int n, mutex& mtx)
//{
//	mtx.lock();
//	for (int i = 0; i < n; i++)
//	{
//		std::cout << i << " ";
//	}
//	mtx.unlock();
//	std::cout << std::endl;
//}
//int main()
//{
//	mutex mtx; // 锁
//	thread t1(func, 10, ref(mtx));
//	thread t2(func, 10, ref(mtx));
//
//	t1.join();
//	t2.join();
//	return 0;
//}

//void func(int n)
//{
//	for (int i = 0; i < n; i++)
//	{
//		std::cout << i << " ";
//	}
//	std::cout << std::endl;
//}
//
//int main()
//{
//	thread t1(func, 10);
//	thread t2(func, 10);
//
//	t1.join();
//	t2.join();
//	return 0;
//}

//class mythread
//{
//public:
//	// 构造函数
//	mythread(thread& t)
//		:_t(t)
//	{}
//	// 析构函数
//	~mythread()
//	{
//		if (_t.joinable())
//			_t.join();
//	}
//	// 防拷贝
//	mythread(const thread&) = delete;
//	mythread& operator=(const thread&) = delete;
//private:
//	thread& _t;
//};
//
//void func(int n)
//{
//	for (int i = 0; i < n; i++)
//	{
//		std::cout << i << std::endl;
//	}
//}
//bool Stop()
//{
//	return false;
//}
//int main()
//{
//	thread t1(func, 10);
//	mythread mt(t1); // 生命周期结束的时候自动调用析构函数
//
//	if (!Stop())
//		return -1;
//
//	t1.join(); // 不会被执行
//	return 0;
//}



//void func(int n)
//{
//	for (int i = 0; i < n; i++)
//	{
//		std::cout << i << std::endl;
//	}
//}
//bool Stop()
//{
//	return false;
//}
//int main()
//{
//	thread t1(func, 10);
//
//	if (!Stop())
//		return -1;
//
//	t1.join(); // 不会被执行
//	return 0;
//}


//void func(int n)
//{
//	for (int i = 0; i < n; i++)
//	{
//		std::cout << i << std::endl;
//	}
//}
//int main()
//{
//	thread t1(func, 10);
//	t1.join();
//
//	t1 = thread(func, 10);
//	t1.join(); // nice
//	return 0;
//}







//void func(int* num)
//{
//	(*num)++;
//}
//int main()
//{
//	int num = 0;
//	thread t1([&num] {num++; });
//	t1.join();
//	t1.join(); // 崩溃
//	return 0;
//}

//void func(int* num)
//{
//	(*num)++;
//}
//int main()
//{
//	int num = 0;
//	thread t1([&num] {num++; });
//	t1.join();
//	t1.join(); // 崩溃
//	return 0;
//}

//void func(int* num)
//{
//	(*num)++;
//}
//int main()
//{
//	int num = 0;
//	thread t1([&num] {num++; });
//	t1.join();
//	std::cout << num << std::endl; // 1
//	return 0;
//}

//void func(int* num)
//{
//	(*num)++;
//}
//int main()
//{
//	int num = 0;
//	thread t1(func, &num);
//	t1.join();
//	std::cout << num << std::endl; // 1
//	return 0;
//}

//void func(int& num)
//{
//	num++;
//}
//int main()
//{
//	int num = 0;
//	thread t1(func, std::ref(num));
//	t1.join();
//	std::cout << num << std::endl; // 1
//	return 0;
//}

//void func(int& num)
//{
//	num++;
//}
//int main()
//{
//	int num = 0;
//	thread t1(func, num);
//	std::cout << num << std::endl; // 0
//	return 0;
//}


//void func(int n)
//{
//	std::cout << this_thread::get_id() << std::endl;
//}
//int main()
//{
//	thread t1(func);
//	t1.join();
//	return 0;
//}

//
//void func(int n)
//{
//	for (int i = 0; i < n; i++)
//	{
//		std::cout << i << std::endl;
//	}
//}
//int main()
//{
//	thread t1 = thread(func, 10);
//	t1.join();
//	return 0;
//}







//void func(int n)
//{
//	for (int i = 0; i < n; i++)
//	{
//		std::cout << i << std::endl;
//	}
//}
//int main()
//{
//	thread t1(func, 10);
//	t1.join();
//	return 0;
//}







//void func(int n)
//{
//	for (int i = 0; i < n; i++)
//	{
//		std::cout << i << std::endl;
//	}
//}
//int main()
//{
//	thread t1;
//	t1 = thread(func, 10);
//	t1.join();
//	return 0;
//}
