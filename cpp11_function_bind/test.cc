#define _CRT_CURTUE_WARNINGS 1
#include<iostream>
#include<functional>
#include<vector>
#include<stack>
#include<cstring>
#include<string>
#include<unordered_map>
using namespace std;

class Sub
{
public:
	int sub(int a, int b)
	{
		return a - b;
	}
};

int main()
{
	function<int(int, int)> func = bind(&Sub::sub, Sub(), placeholders::_2, placeholders::_1);
	std::cout << func(1, 2) << std::endl;
	return 0;
}


//int main()
//{
//	function<int(int, int)> func = bind(&Sub::sub, Sub(), placeholders::_1, placeholders::_2);
//	std::cout << func(1, 2) << std::endl;
//	return 0;
//}


//int Plus(int a, int b)
//{
//	return a + b;
//}
//
//int main()
//{
//	function<int(int)> func = bind(Plus, placeholders::_1, 10);
//	std::cout << func(2) << std::endl;
//	return 0;
//}

//class Solution
//{
//public:
//	int evalRPN(vector<string>& tokens)
//	{
//		// 定义一个栈
//		stack<int> t;
//		unordered_map<string, function<int(int, int)>> op =
//		{
//			{"+", [](int a, int b) {return a + b; }},
//			{"-", [](int a, int b) {return a - b; }},
//			{"*", [](int a, int b) {return a * b; }},
//			{"/", [](int a, int b) {return a / b; }}
//		};
//		for (const auto& str : tokens)
//		{
//			int left;
//			int right;
//			if (str == "+" || str == "-" || str == "*" || str == "/")
//			{
//				left = t.top();
//				t.pop();
//				right = t.top();
//				t.pop();
//				t.push(op[str](left, right));
//			}
//			else
//			{
//				t.push(stoi(str));
//			}
//		}
//		return t.top();
//	}
//};


//class Solution
//{
//public:
//	int evalRPN(vector<string>& tokens)
//	{
//		// 定义一个栈
//		stack<int> t;
//		// 从数组中选择
//		for (const auto& str : tokens)
//		{
//			int left;
//			int right;
//			// 如果是+-*/操作符则进入switch
//			if (str == "+" || str == "-" && str == "*" && str == "/")
//			{
//				// 取出栈顶两个元素
//				left = t.top();
//				t.pop();
//				right = t.top();
//				t.pop();
//				// switch
//				switch (str[0])
//				{
//				case '+':
//					t.push(left + right);
//					break;
//				case '-':
//					t.push(left - right);
//					break;
//				case '*':
//					t.push(left * right);
//					break;
//				case '/':
//					if (right == 0)
//					{
//						return;
//					}
//					else
//					{
//						t.push(left / right);
//						break;
//					}
//				default:
//					break;
//				}
//			}
//			else
//			{
//				t.push(stoi(str));
//			}
//		}
//		return t.top();
//	}
//};


//template<class F, class T>
//T useF(F f, T t)
//{
//	static int count = 0;
//	std::cout << "count:" << ++count << std::endl;
//	std::cout << "count:" << &count << std::endl;
//	return f(t);
//}
//
//// 函数
//double f(double a)
//{
//	return a / 2;
//}
//// 仿函数
//struct Function
//{
//public:
//	double operator()(double a)
//	{
//		return a / 4;
//	}
//};

//int main()
//{
//	// 1、函数指针
//	function<double(double)> func1 = f;
//	std::cout << useF(func1, 11.11) << std::endl;
//
//	// 2、仿函数
//	function<double(double)> func2 = Function();
//	std::cout << useF(func2, 11.11) << std::endl;
//
//	// 3、lambda表达式
//	function<double(double)> func3 = [](double x)->double {return x / 4; };
//	std::cout << useF(func3, 11.11) << std::endl;
//	return 0;
//}
















//int main()
//{
//	// 1、函数指针
//	std::cout << useF(f, 11.11) << std::endl;
//
//	// 2、仿函数
//	std::cout << useF(Function(), 11.11) << std::endl;
//
//	// 3、lambda表达式
//	std::cout << useF([](double d)->double {return d / 4; }, 11.11) << std::endl;
//	return 0;
//}










//// 函数
//int f(int a, int b)
//{
//	return a + b;
//}
//// 仿函数
//struct Function
//{
//	int operator()(int a, int b)
//	{
//		return a + b;
//	}
//};
//// plus类
//class Plus
//{
//public:
//	static int plusi(int a, int b)
//	{
//		return a + b;
//	}
//	double plusd(double a, double b)
//	{
//		return a + b;
//	}
//};
//
//int main()
//{
//	// 1、包装函数指针（函数名）
//	function<int(int, int)> func1 = f;
//	std::cout << func1(1, 2) << std::endl;
//
//	// 2、包装仿函数（函数对象）
//	function<int(int, int)> func2 = Function();
//	std::cout << func2(2, 4) << std::endl;
//
//	// 3、包装lambda表达式
//	function<int(int, int)> func3 = [](int a, int b) {return a + b; };
//	std::cout << func3(6, 8) << std::endl;
//
//	// 4、包装类内静态成员函数
//	function<int(int, int)> func4 = &Plus::plusi; // &可以省略
//	std::cout << func4(7, 9) << std::endl;
//
//	// 5、包装类内非静态成员函数
//	function<double(Plus, double, double)> func5 = &Plus::plusd; // &不可省略
//	std::cout << func5(Plus(), 1.1, 2.2) << std::endl;
//
//	return 0;
//}
