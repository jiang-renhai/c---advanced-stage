#pragma once

#include<iostream>
#include<string>
#include<bitset>
using namespace std;

namespace JRH 
{
	struct BKDRHash
	{
		size_t operator()(const string& s)
		{
			// BKDR
			size_t value = 0;
			for (auto ch : s)
			{
				value *= 31;
				value += ch;
			}
			return value;
		}
	};

	struct APHash
	{
		size_t operator()(const string& s)
		{
			size_t hash = 0;
			for (long i = 0; i < s.size(); i++)
			{
				if ((i & 1) == 0)
				{
					hash ^= ((hash << 7) ^ s[i] ^ (hash >> 3));
				}
				else
				{
					hash ^= (~((hash << 11) ^ s[i] ^ (hash >> 5)));
				}
			}
			return hash;
		}
	};

	struct DJBHash
	{
		size_t operator()(const string& s)
		{
			size_t hash = 5381;
			for (auto ch : s)
			{
				hash += (hash << 5) + ch;
			}
			return hash;
		}
	};


	//布隆过滤器
	template<size_t N, class K = string, class Hash1 = BKDRHash, class Hash2 = APHash, class Hash3 = DJBHash>
	class bloomfilter
	{
	public:
		void Set(const K& key)
		{
			// 计算出key对应的三个位
			size_t i1 = Hash1()(key) % N;
			size_t i2 = Hash2()(key) % N;
			size_t i3 = Hash3()(key) % N;

			// 置1
			_bs.set(il);
			_bs.set(i2);
			_bs.set(i3);
		}

		bool Test(const K& key)
		{
			size_t i1 = Hash1()(key) % N;
			if (_bs.test(il) == false)
			{
				return false;
			}

			size_t i2 = Hash2()(key) % N;
			if (_bs.test(i2) == false)
			{
				return false;
			}

			size_t i3 = Hash3()(key) % N;
			if (_bs.test(i3) == false)
			{
				return false;
			}
			// 三个都存在，可能导致误判
			return true;
		}
	private:
		bitset<N> _bs;
	};

}