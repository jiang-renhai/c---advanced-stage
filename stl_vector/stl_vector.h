#include<iostream>
#include<cassert>

namespace JRH
{
	// vector
	template<class T>
	class vector
	{
	public:
		typedef T* iterator;
		typedef const T* const_iterator;
	public:
		// 1、默认的函数
		vector() // 构造函数1
			:_start(nullptr)
			, _finish(nullptr)
			, _endofstorage(nullptr)
		{}
		vector(size_t n, const T& val) // 构造函数2
			:_start(nullptr)
			,_finish(nullptr)
			,_endofstorage(nullptr)
		{
			reserve(n); // 调用下面写的reverse函数将其空间先定义为n个
			// 尾插n个val值进入到vector中
			for (size_t i = 0; i < n; i++)
			{
				push_back(val);
			}
		}
		vector(int n, const T& val) // 构造函数2 -- 重载int
			:_start(nullptr)
			, _finish(nullptr)
			, _endofstorage(nullptr)
		{
			reserve(n); // 调用下面写的reverse函数将其空间先定义为n个
			// 尾插n个val值进入到vector中
			for (int i = 0; i < n; i++)
			{
				push_back(val);
			}
		}
		vector(long n, const T& val) // 构造函数2 -- 重载long
			:_start(nullptr)
			, _finish(nullptr)
			, _endofstorage(nullptr)
		{
			reserve(n); // 调用下面写的reverse函数将其空间先定义为n个
			// 尾插n个val值进入到vector中
			for (long i = 0; i < n; i++)
			{
				push_back(val);
			}
		}
		template<class InputIterator>
		vector(InputIterator first, InputIterator last) // 构造函数3
			:_start(nullptr)
			, _finish(nullptr)
			, _endofstorage(nullptr)
		{
			// 将迭代器中的所有区间的值全部都放进vector中
			while (first != last)
			{
				// this->push_back(*first);
				push_back(*first);
				first++;
			}
		}
		vector(const vector<T>& vt) // 拷贝构造 -- 传统写法
			:_start(nullptr)
			, _finish(nullptr)
			, _endofstorage(nullptr)
		{
			// 先开一块相同大小的空间
			_start = new T[vt.capacity()];
			for (size_t i = 0; i < vt.size(); i++) // 一个个拷贝过去
			{
				_start[i] = vt[i];
			}
			_finish = _start + vt.size(); // 有效数据的尾
			_endofstorage = _start + vt.capacity(); // 整个数组的尾
		}
		//vector(const vector<T>& vt) // 拷贝构造 -- 现代写法
		//	:_start(nullptr)
		//	, _finish(nullptr)
		//	, _endofstorage(nullptr)
		//{
		//	reserve(vt.capacity()); // 开辟一段vt等大的空间
		//	for (auto e : vt)
		//	{
		//		push_back(e); // 将容器vt中的数据一个个尾插进来
		//	}
		//}
		vector<T>& operator=(const vector<T>& vt) // 运算符赋值重载函数 -- 传统写法
		{
			if (this != &vt) // 不能自己给自己赋值
			{
				delete[] _start; // 释放原来的空间
				_start = new T[vt.capacity()]; // 开辟一块与vt等大的空间
				for (size_t i = 0; i < vt.size(); i++)
				{
					_start[i] = vt[i]; // 将vt里面的数据一个个拷贝过来
				}
				_finish = _start + vt.size(); // 有效数据的大小
				_endofstorage = _start + vt.capacity(); // 整个数组的大小
			}
			return *this; // 支持连续赋值
		}
		//vector<T>& operator=(const vector<T> vt) // 运算符赋值重载函数 -- 现代写法
		//{
		//	std::swap(vt); // 交换这两个值
		//	return *this; // 支持连续赋值
		//}
		~vector() // 析构函数
		{
			if (_start != nullptr) // 避免对空指针进行曹组
			{
				delete[] _start; // 释放空间
				_start = nullptr; 
				_finish = nullptr;
				_endofstorage = nullptr;
			}
		}

		// 2、迭代器相关函数
		iterator begin() // 头
		{
			return _start; // 返回头元素地址
		}
		iterator end() // 尾
		{
			return _finish; // 返回有效数据的下一个元素地址
		}
		const_iterator begin() const // const头
		{
			return _start; // 返回头元素地址
		}
		const_iterator end() const // const尾
		{
			return _finish; // 返回有效数据的下一个元素地址
		}

		// 3、容器相关的函数
		size_t size() const // 大小
		{
			return _finish - _start;
		}
		size_t capacity() const // 容量
		{
			return _endofstorage - _start;
		}
		void reserve(size_t n) // 保持容量
		{
			if (n > capacity()) // 判断是否需要进行操作
			{
				size_t sz = size(); // 保存一下有效数据的个数
				T* tmp = new T[n]; // 开辟一个可以容纳n个空间的tmp
				if (_start)
				{
					for (size_t i = 0; i < sz; i++)
					{
						tmp[i] = _start[i]; // 将原本容器中的所有数据都拷贝到tmp临时空间中
					}
					delete[] _start; // 释放原空间
				}
				_start = tmp; // 将tmp临时空间中的数据全都放到对象容器当中
				_finish = _start + sz; // 有效数据个数
				_endofstorage = _start + n; // 总容量大小
			}
		}
		void resize(size_t n, const T& val = T()) // 扩容
		{
			if (n < size()) // 当n小于当前的size时
			{
				_finish = _start + n; // 将size缩小到n
			}
			else // 当n大于当前的size时
			{
				if (n > capacity()) // 判断是否需要增容
				{
					reserve(n);
				}
				while (_finish < _start + n) // 将size扩大到n
				{
					*_finish = val;
					_finish++;
				}
			}
		}
		bool empty() const // 判空
		{
			return _start == _finish;
		}

		// 4、修改容器内容相关函数
		void push_back(const T& x)
		{
			if (_finish == _endofstorage) // 到容量底了
			{
				reserve(capacity() == 0 ? 4 : 2 * capacity());
			}
			*_finish = x;
			_finish++;
		}
		void pop_back()
		{
			assert(!empty());
			--_finish;
		}
		void insert(iterator pos, const T& x)
		{
			assert(pos >= _start);
			assert(pos <= _finish);
			if (_finish == _endofstorage)
			{
				size_t len = pos - _start; // 记录一下相差大小
				// 扩容
				reserve(capacity() == 0 ? 4 : capacity() * 2);
				pos = _start + len;
			}

			iterator end = _finish - 1; // 将pos位置及其之后的数据统一向后挪动一位，以留出pos位置进行插入
			while (end >= pos)
			{
				*(end + 1) = *end;
				--end;
			}
			*pos = x; // 插入 
			++_finish;  // 往后移一位
		}
		iterator erase(iterator pos)
		{
			assert(pos >= _start);
			assert(pos < _finish);
			iterator start = pos + 1;
			// 将pos位置之后的数据统一向前挪动一位，以覆盖pos位置的数据
			while (start != _finish)
			{
				*(start - 1) = *start;
				++start;
			}
			--_finish;
		}
		void swap(vector<T>& v)
		{
			std::swap(_start, v._start);
			std::swap(_finish, v._finish);
			std::swap(_endofstorage, v._endofstorage);
		}

		// 5、访问容器相关函数
		T& operator[](size_t i)
		{
			assert(i < size());
			return _start[i];
		}
		const T& operator[](size_t i) const
		{
			assert(i < size());
			return _start[i];
		}
	private:
		iterator _start;        //指向容器的头
		iterator _finish;       //指向有效数据的尾
		iterator _endofstorage; //指向容器的尾
	};
	void test_vector1()
	{
		vector<int> v;
		v.push_back(1);
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);
		v.pop_back();
		//v.pop_back();
		for (size_t i = 0; i < v.size(); ++i)
		{
			std::cout << v[i] << " ";
		}
		std::cout << std::endl;

		for (auto e : v)
		{
			std::cout << e << " ";
		}
		std::cout << std::endl;

		vector<int>::iterator it = v.begin();
		while (it != v.end())
		{
			std::cout << *it << " ";
			it++;
		}
		std::cout << std::endl;

		v.resize(2);
		std::cout << v.capacity() << std::endl;
		std::cout << v.size() << std::endl;
	}
}
