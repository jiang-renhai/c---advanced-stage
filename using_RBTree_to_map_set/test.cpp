

#include"MyRBTree.h"
#include"Map.h"
#include"Set.h"

int main()
{
	//JRH::MySet<int, int> m;
	//m.insert(2);
	//m.insert(3);
	//m.insert(3);
	//m.insert(5);
	//m.insert(6);
	//JRH::MySet<int, int>::iterator it = m.begin();
	//while (it != m.end())
	//{
	//	cout << *it << " ";
	//	++it;
	//}
	//cout << endl;

	JRH::MyMap<int, int> m;
	m.insert(make_pair(2, 2));
	m.insert(make_pair(3, 3));
	m.insert(make_pair(4, 4));
	m.insert(make_pair(1, 1));
	JRH::MySet<int, int>::iterator it = m.begin();
	while (it != m.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;

	return 0;
}

