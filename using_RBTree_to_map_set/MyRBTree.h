#pragma once

#include<algorithm>

#include<iostream>
using namespace std;

enum Col
{
	BLACK,
	RED
};

//红黑树结点的定义
template<class T>
struct RBTreeNode
{
	//构造函数
	RBTreeNode(const T& data)
		:_left(nullptr)
		, _right(nullptr)
		, _parent(nullptr)
		, _data(data)
		, _col(RED)
	{}

	//三叉链
	RBTreeNode<T>* _left;
	RBTreeNode<T>* _right;
	RBTreeNode<T>* _parent;

	//存储的数据
	T _data;

	//结点的颜色
	int _col; //红/黑
};

// 正向迭代器
template<class T, class Ref, class Ptr>
struct __TreeIterator
{
	typedef RBTreeNode<T> Node; //结点类型
	typedef __TreeIterator<T, Ref, Ptr> Self; //正向迭代器类型

	// 为了让下面反向迭代器拿到
	typedef typename Ref reference;
	typedef typename Ptr pointer;
	// 构造函数
	__TreeIterator(Node* node)
		:_node(node)
	{}

	// 正向迭代器解引用操作
	Ref operator*()
	{
		return _node->_data;
	}

	// 正向迭代器指向操作
	Ptr operator->()
	{
		return &_node->_data;
	}

	// 判断两个正向迭代器是否不同
	bool operator!=(const Self& s) const
	{
		return _node != s._node;
	}

	// 判断两个正向迭代器是否相同
	bool operator==(const Self& s) const
	{
		return _node == s._node;
	}

	// 正向迭代器++操作
	Self operator++()
	{
		// 结点的右子树不为空，找右子树的最左结点
		if (_node->_right != nullptr)
		{
			Node* R_left = _node->_right;
			while (R_left->_left != nullptr)
			{
				R_left = R_left->_left;
			}
			_node = R_left;
		}
		// 结点的右子树为空，找祖先结点不为右的结点
		else
		{
			Node* cur = _node;
			Node* parent = cur->_parent;
			while (parent && cur == parent->_right)
			{
				cur = parent;
				parent = parent->_parent;
			}
			_node = parent;
		}
		return *this;
	}

	// 正向迭代器--操作
	Self operator--()
	{
		// 结点的左子树不为空，找左子树中的最右节点
		if (_node->_left != nullptr)
		{
			Node* L_right = _node->_left;
			while (L_right->_right != nullptr)
			{
				L_right = L_right->_right;
			}
			_node = L_right;
		}
		// 节点的左子树为空，找祖先结点中，找到孩子不在父亲左的祖先
		else
		{
			Node* cur = _node;
			Node* parent = cur->_parent;
			while (parent && cur == parent->_left)
			{
				cur = parent;
				parent = parent->_parent;
			}
			_node = parent;
		}
		return *this;
	}

	Node* _node;
};


//反向迭代器 -- 根据正向迭代器封装
template<class Iterator>
struct ReverseIterator
{
	typedef ReverseIterator<Iterator> Self; //反向迭代器的类型

	// 这里为了能够让反向迭代器能够拿到正向迭代器的解引用和指针
	typedef typename Iterator::reference Ref; //结点指针的解引用*
	typedef typename Iterator::pointer Ptr; //结点指针->

	//构造函数
	ReverseIterator(Iterator rit)
		:_rit(rit)
	{}

	// 和正向迭代器一样
	Ref operator*()
	{
		return *_rit;
	}

	// 和正向迭代器一样
	Ptr operator->()
	{
		return _rit.operator->();
	}

	// ++操作就是正向迭代器的--操作
	Self& operator++()
	{
		--_rit;
		return *this;
	}

	// --操作就是正向迭代器的++操作
	Self& operator--()
	{
		++_rit;
		return *this;
	}

	// 不等号一样
	bool operator!=(const Self& s) const
	{
		return _rit != s._rit;
	}

	// 等号也一样
	bool operator==(const Self& s) const
	{
		return _rit == s._rit;
	}

	Iterator _rit;
};


template<class K, class T, class KeyOfT>
class RBTree
{
	typedef RBTreeNode<T> Node;

public:
	typedef __TreeIterator<T, T&,T*> iterator;
	typedef ReverseIterator<iterator> reverse_iterator;

	// 构造函数
	RBTree()
		:_root(nullptr)
	{}

	//赋值运算符重载
	RBTree<K, T, KeyOfT>& operator=(RBTree<K, T, KeyOfT> t)
	{
		swap(_root, t._root);
		return *this;
	}

	//析构函数
	~RBTree()
	{
		_Destroy(_root);
		_root = nullptr;
	}

	// 最左节点
	iterator begin()
	{
		Node* left = _root;
		while (left && left->_left != nullptr)
		{
			left = left->_left;
		}
		// 返回最左结点的迭代器
		return iterator(left);
	}

	// end()是整个树的最末尾结点的后一个位置
	iterator end()
	{
		return iterator(nullptr);
	}

	// 最右结点
	reverse_iterator rbegin()
	{
		Node* right = _root;
		while (right && right->_left != nullptr)
		{
			right = right->_right;
		}
		// 返回最右结点的迭代器
		return reverse_iterator(iterator(right));
	}

	// end()是整个树的最末尾结点的后一个位置
	reverse_iterator rend()
	{
		return reverse_iterator(iterator(nullptr));
	}

	// 红黑树的查找
	Node* Find(const K& key)
	{
		KeyOfT kot;
		Node* cur = _root;
		while (cur)
		{
			// 当前结点的值大于寻找的结点的值
			if (key < kot(cur->_data))
			{
				cur = cur->_left;
			}
			else if (key > kot(cur->_data))
			{
				cur = cur->_right;
			}
			else
			{
				// 找到了
				return cur;
			}
		}
		return nullptr;
	}

	// 插入
	pair<iterator, bool> Insert(const T& data)
	{
		KeyOfT kot;

		// 一棵空树
		if (_root == nullptr)
		{
			// 创建新结点 + 颜色初始化为黑色
			_root = new Node(data);
			_root->_col = BLACK; // 根节点得是黑的
			return make_pair(iterator(_root), true);
		}

		// 先找到 -- 利用二叉搜索树的方法进行查找
		Node* cur = _root;
		Node* parent = nullptr;
		// 左小右大
		while (cur)
		{
			// 当前结点值大于待插入结点值，往左子树走
			if (kot(cur->_data) > kot(data))
			{
				parent = cur;
				cur = cur->_left;
			}
			// 当前结点值小于待插入结点值，往右子树走
			else if (kot(cur->_data) < kot(data))
			{
				parent = cur;
				cur = cur->_right;
			}
			// 待插入结点的值和当前结点的值相等，插入失败
			else
			{
				return make_pair(iterator(cur), false);
			}
		}

		// 将当前结点的值插入进去
		cur = new Node(data); // new一个新的结点
		cur->_col = RED;
		Node* newnode = cur; // 记录新插入的结点
		// 新插入的节点值小于父节点的节点值，插入到parent的左边
		if (kot(data) < kot(parent->_data))
		{
			parent->_left = cur;
			cur->_parent = parent;
		}
		// 新插入的节点值小于父节点的节点值，插入到parent的左边
		else
		{
			parent->_right = cur;
			cur->_parent = parent;
		}

		// 新插入结点的父节点是红色的，需要做出调整
		while (parent && parent->_col == RED)
		{
			Node* grandfather = parent->_parent; // parent是红色，则其父结点一定存在
			// 以grandparent左右孩子为分界线，分成if和else
			if (parent == grandfather->_left) // 左孩子
			{
				Node* uncle = grandfather->_right;
				if (uncle && uncle->_col == RED)// 情况一：uncle存在且为红色
				{
					// 颜色调整
					grandfather->_col = RED;
					uncle->_col = parent->_col = BLACK;

					// 继续往上处理
					cur = grandfather;
					parent = cur->_parent;
				}
				else// 情况二：uncle存在且为黑色 / 情况三：uncle不存在
				{
					// 用左右孩子分为两半，一半是if用来表示在左孩子，一半是else用来表示在右孩子
					if (cur == parent->_left)
					{
						//   g
						//  p
						// c
						// 右单旋
						RoateR(grandfather);

						// 颜色调整
						parent->_col = BLACK;
						grandfather->_col = RED;
					}
					else
					{
						//    g
						//  p
						//      c
						// 左右双旋
						RoateLR(grandfather);

						// 颜色调整
						cur->_col = BLACK;
						grandfather->_col = RED;
					}
					break;
				}
			}
			else // 右孩子
			{
				Node* uncle = grandfather->_left;
				if (uncle && uncle->_col == RED)// 情况一：uncle存在且为红色
				{
					// 颜色调整
					grandfather->_col = RED;
					uncle->_col = parent->_col = BLACK;

					// 继续往上处理
					cur = grandfather;
					parent = cur->_parent;
				}
				else// 情况二：uncle存在且为黑色 / 情况三：uncle不存在
				{
					// 用左右孩子分为两半，一半是if用来表示在左孩子，一半是else用来表示在右孩子
					if (cur == parent->_right)
					{
						//   g
						//     p
						//      c
						// 左单旋
						RoateL(grandfather);

						// 颜色调整
						parent->_col = BLACK;
						grandfather->_col = RED;
					}
					else
					{
						//    g
						//      p
						//    c
						// 右左双旋
						RoateRL(grandfather);

						// 颜色调整
						cur->_col = BLACK;
						grandfather->_col = RED;
					}
					break;
				}
			}
		}
		_root->_col = BLACK;
		return make_pair(iterator(newnode), true);
	}

	// 左单旋
	void RoateL(Node* parent)
	{
		// 三叉链
		Node* subr = parent->_right;
		Node* subrl = subr->_left;
		Node* ppnode = parent->_parent;

		// subrl与parent的关系
		parent->_right = subrl;
		if (subrl)
			subrl->_parent = parent;

		// subl和parent的关系
		subr->_left = parent;
		parent->_parent = subr;

		// ppnode和subr的关系
		if (ppnode == nullptr)
		{
			_root = subr;
			subr->_parent = nullptr;
		}
		else
		{
			if (ppnode->_left == parent)
			{
				ppnode->_left = subr;
			}
			else
			{
				ppnode->_right = subr;
			}
			subr->_parent = ppnode;
		}
	}

	// 右单旋
	void RoateR(Node* parent)
	{
		// 三叉链
		Node* subl = parent->_left;
		Node* sublr = subl->_right;
		Node* ppnode = parent->_parent;


		//sublr和parent之间的关系
		parent->_left = sublr;
		if (sublr)
			sublr->_parent = parent;

		//subl和parent的关系
		subl->_right = parent;
		parent->_parent = subl;


		//ppnode 和 subl的关系
		if (ppnode == nullptr)
		{
			_root = subl;
			subl->_parent = nullptr;
		}
		else
		{
			if (ppnode->_left == parent)
			{
				ppnode->_left = subl;
			}
			else
			{
				ppnode->_right = subl;
			}
			subl->_parent = ppnode;
		}
	}

	// 左右双旋
	void RoateLR(Node* parent)
	{
		RoateL(parent->_left);
		RoateR(parent);
	}

	// 右左双旋
	void RoateRL(Node* parent)
	{
		RoateR(parent->_right);
		RoateL(parent);
	}


private:
	//析构函数子函数
	void _Destroy(Node* root)
	{
		if (root == nullptr)
		{
			return;
		}
		_Destroy(root->_left);
		_Destroy(root->_right);
		delete root;
	}

	Node* _root;
};
