#pragma once

#include"MyRBTree.h"

namespace JRH
{
	template<class K, class T>
	class MySet
	{
		// 仿函数
		struct SetKeyOfT
		{
			const K& operator()(const K& key)
			{
				return key;
			}
		};

	public:
		typedef typename RBTree<K, K, SetKeyOfT>::iterator iterator; //正向迭代器
		typedef typename RBTree<K, K, SetKeyOfT>::reverse_iterator reverse_iterator; //反向迭代器

		// begin()
		iterator begin()
		{
			return _t.begin();
		}

		// end()
		iterator end()
		{
			return _t.end();
		}

		// rbegin()
		reverse_iterator rbegin()
		{
			return _t.rbegin();
		}

		// rend()
		reverse_iterator rend()
		{
			return _t.rend();
		}

		// 插入
		pair<iterator, bool> insert(const K& key)
		{
			return _t.Insert(key);
		}

		// 删除
		void erase(const K& key)
		{
			return _t.Erase(key);
		}

		// 查找
		iterator find(const K& key)
		{
			return _t.Find(key);
		}

	private:
		RBTree<K, K, SetKeyOfT> _t;
	};
}


