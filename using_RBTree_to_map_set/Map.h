#pragma once

#include"MyRBTree.h"

namespace JRH
{
	template<class K, class V>
	class MyMap

	{	//仿函数
		struct MapKeyOfT
		{
			const K& operator()(const pair<const K, V>& kv) //返回键值对当中的键值Key
			{
				return kv.first;
			}
		};
	public:
		typedef typename RBTree<K, K, MapKeyOfT>::iterator iterator; //正向迭代器
		typedef typename RBTree<K, K, MapKeyOfT>::reverse_iterator reverse_iterator; //反向迭代器

		// begin()
		iterator begin()
		{
			return _t.begin();
		}

		// end()
		iterator end()
		{
			return _t.end();
		}

		// rbegin()
		reverse_iterator rbegin()
		{
			return _t.rbegin();
		}

		// rend()
		reverse_iterator rend()
		{
			return _t.rend();
		}

		// 插入
		pair<iterator, bool> insert(const pair<const K, V>& kv)
		{
			return _t.Insert(kv);
		}

		// 删除
		void erase(const K& key)
		{
			return _t.Erase(key);
		}

		// 查找
		iterator find(const K& key)
		{
			return _t.Find(key);
		}

		//[]运算符重载函数
		V& operator[](const K& key)
		{
			pair<iterator, bool> ret = insert(make_pair(key, V()));
			iterator it = ret.first;
			return it->second;
		}

	private:
		RBTree<K, pair<K, V>, MapKeyOfT> _t;
	};
}


